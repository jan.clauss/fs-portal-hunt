<?php
//Ü
	// ####################################################
	// MySQL
	// ####################################################

	$database_host='localhost';
	$database_username='FS';
	$database_userpass='volldasgeheime';

	$database_name='FS';

	// Der Name der User-Datenbank, für jeden Bot eine extra Datenbank
	$database_table_main="$database_name.PortalHuntTest";
	// Aufgrund dieses Namens werden noch die Datenbanken
	// $database_table_main.'_chatadmin' - Enthält die Rechte der Nutzer an den Chats, wird durch /ops aktualisiert
	// $database_table_main.'_Portale' - Enthält die gemeldeten Portale

	function connectpdo() { // Liefert eine pdo instanz
		global
			$database_host,$database_username,$database_userpass;
		$pdo=new PDO('mysql:host='.$database_host, $database_username, $database_userpass,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8MB4\''));
		if($pdo->errorInfo()[0]<>'00000') error_log("$botname ".print_r($pdo->errorInfo(),true));
		return $pdo;
	}

	// ####################################################
	// Composer
	// ####################################################
	
	// Pfad zur Composer-Installation vendor/autoload.php
	$composer_vendorautoload='/var/www/vendor/autoload.php';
	
	// ####################################################
	// Google Sheets
	//
	// https://developers.google.com/sheets/api/quickstart/php
	//
	// ####################################################
	
	$sheets_authConfig='/var/www/credentials.json';
	$sheets_tokenPath='/var/www/token.json';
	$sheets_usermail='username@domain'; // Nur als Info, für wen freigegeben werden muss

	// ####################################################
	// ImageWebsite von Crytix
	//
	// sind Token oder url == '' wird nichts gesendet
	//
	// ####################################################
	$cry_mainurl='https://webportals.myingress.net/';
	$cry_apiurl=$cry_mainurl.'api/fsdata.php';
	$cry_apitoken='token';
	$cry_uploadurl=$cry_mainurl.'upload';
	$cry_webportal=$cry_mainurl;

	// ####################################################
	// BOT
	// ####################################################
	
	// Daten vom @botfather
	$bot_name="PortalHuntTest";
	$bot_benutzername="PortalHuntTestBot";
	$bot_token="xxxxxxxxxx:yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";

	// Die Domain des Servers
	$bot_server="janc70.rocks";
	// Nur eine Vereinfachung und Gewohnheit
	$bot_db=$database_table_main;
	// Bot verantwortlicher // Nutzung beim WEB-Aufruf Telegram-Bot-Test
	$bot_owner=123456789; // 
	// Server verantwortlicher // noch keine Nutzung, wird für die Sprachen die Rechte vergeben, da diese für alle Bots die gleichen sind
	$bot_server_owner=123456789; // 
	// Bei true dürfen nur die Aufgelisteten Admins Rechte der User ändern, sonst jeder MOD
//	$bot_admins_use=true;
	// Bot Admins // Setzen der Nutzerrechte (mod, user)
//	$bot_admins[123456789]='janc70';
	// Gruppen-ID für eine übergreifende Logginggruppe für den Admin, kann auch eine User TG-ID sein
	$bot_adminlog=-12345678;
	// Diese Texte werden in Log-Ausgabe (an TG) ersetzt
	$logmess_replace=array(
		array('search' => 'geheimer Text', 'replace' => 'xxx'),
	);
	// Log-Verzeichnis, wenn auch im Dateisystem ein Log gespeichert werden soll sonst ''
	$bot_errorlog='/home/user/logs';
	// Das Root-Verzeichnis des Bots. Dieses muss von aussen erreichbar aber "geheim" sein
	// Es wird via Browser zum Initalisieren und Verbinden/Trennen des Bots benötigt
	// Ansonsten sollte es nur den Telegram Server bekannt sein
	$bot_root='/var/www/FSPortalHunt/eingeheimesverzeichnis/';
	// hier liegen die Sprachdateien json und csv
	$bot_languagedir=$bot_root.'lang/';
	// Defaultwerte für Datenbanken - werden beim Anlegen der Datensätze geschrieben
	// User-Datenbank
	$bot_default_user_timezone='Europe/Berlin';
	$bot_default_user_language='ger';
	// FS-Datenbank
	$bot_fs_timezone='Europe/Berlin';
	// Kann via Browser abgerufen werden (geheimes Verzeichnis...)webhook.php?info
	$bot_info="IFS Portal Hunt Testbot";
	// Konfiguration, wer alles Nachrichten an aktive Agents des FS senden darf
	// Möglichkeiten sind 'mod', 'admin' oder ein Array mit user_id s: array(123456789,864892,1638436)
//	$bot_sendmsg='mod';
	
	//Server
	$server_http='https://FSPortalHunt/';
	$fsroot='/var/www/FSPortalHunt/';
	$fslangfilenameextension='fsph';
	
	// ####################################################
	// QR
	// ####################################################
	
	function setdefaultconfig() {
	    return array();
	}
	
?>