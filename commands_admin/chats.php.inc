<?php
	if(!IsSet($bot_admins[$tg->user_id])) {
		$tg->sendmessage($tg->user_id, 'You are not a admin');
		exit;
	}
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		if($tgres['args'][0] == 'back') {  //Beenden
			$tg->deletemessage($tg->user_id, $tgres['message_id']);
			unset($tgres['args'][0]);
			exit;
		}else{
			$chat_language=$tg->getchatlanguage($tgres['args'][0]);
			if(IsSet($tgres['args'][1])) { // Aktion ausführen
				switch($tgres['args'][1]) {
					case 'setuser':
						$tg->setuserrights($tgres['args'][0], 'user');
						$tg->sendmessage($tgres['args'][0],$tl->getlangstr($chat_language,'chatAddChatOK'));
					break;
					case 'setignore':
						$tg->setuserrights($tgres['args'][0], 'ignore');
						$tg->sendmessage($tgres['args'][0],$tl->getlangstr($chat_language,'chatRevokeChatOK'));
					break;
					case 'setblock':
						$tg->setuserrights($tgres['args'][0], 'block');
						$tg->sendmessage($tgres['args'][0],$tl->getlangstr($chat_language,'chatRevokeChatOK'));
					break;
					case 'del':
						$answer='';
						$result=$tg->leaveChat($tgres['args'][0]);
						if($result['ok'] == true) $answer.='✔'; else $answer.='❌'; $answer.=" leaveChat\n";
						$result=$chatadm->deleteChat($tgres['args'][0]);
						if($result == true) $answer.='✔'; else $answer.='❌'; $answer.=" sql-chatadm\n";
						$phsheet=new portalhuntsheet($pdo, $database_table_main, null, 0); // Ohne ph object und Chat-ID, wird hier nicht benötigt
						$result=$phsheet->deleteChat($tgres['args'][0]);
						if($result == true) $answer.='✔'; else $answer.='❌'; $answer.=" sql-sheet\n";
						unset($phsheet);
						$result=$ph->deletechat($tgres['args'][0]);
						if($result == true) $answer.='✔'; else $answer.='❌'; $answer.=" sql-portale\n";
						$result=$tg->deleteUser($tgres['args'][0]);
						if($result['ok'] == true) $answer.='✔'; else $answer.='❌'; $answer.=" sql-user\n";
						$tg->sendmessage($tg->user_id, $answer);
						exit;
					break;
					case 'admins':
						$result=$tg->getchatAdministrators($tgres['args'][0]);
						$answer='';
						if($result['ok'] <> true) {
							$answer='Fail';
						}else{
							$chatadm->setadmins($tgres['args'][0],$result);
							$answer=$tl->getstr('chatsAdminsAktualisiert')."\n";
							if(count($result['result']) == 0) {
								$answer=$tl->getstr('chatsNoAdmins'); // Keine Admins gefunden, aus dem Chat gekickt?
							}else foreach($result['result'] AS $user) {
								$username='';
								if(IsSet($user['user']['username'])) $username.='@'.$user['user']['username'];
								$first_name='';
								if(IsSet($user['user']['first_name'])) $first_name.=$user['user']['first_name'];
								$last_name='';
								if(IsSet($user['user']['last_name'])) $last_name.=$user['user']['last_name'];
								$answer.=$tg->escapestr($username.', '.$first_name.' '.$last_name)."\n";
							}
						}
//						$answer.=print_r($result,true);
					break;
				}
			}
			if(!IsSet($answer)) $answer=$tl->getstr('chatsOptions');
			$chat=$chatadm->getchat($tgres['args'][0]);
			if($chat <> false) {
				$chatrights='🔴';
				if($chat['rights'] == 'waiting') $chatrights='🟡';
				if($chat['rights'] == 'user') $chatrights='🟢';
				if(!$tg->testechat($chat['user_id'])) {
					$chatrights.='⛔';
				}
				$configarray[]=array(array('text' => $chatrights.$chat['first_name'], 'callback_data' => 'chats '.$chat['user_id']));
				if($chat['rights'] <> 'user') $configarray[]=array(array('text' => $tl->getstr('chatsToActiv'), 'callback_data' => 'chats '.$chat['user_id'].' setuser'));
				if($chat['rights'] <> 'waiting' and $chat['rights'] <> 'ignore') $configarray[]=array(array('text' => $tl->getstr('chatsToIgnore'), 'callback_data' => 'chats '.$chat['user_id'].' setignore'));
				if($chat['rights'] <> 'block') $configarray[]=array(array('text' => $tl->getstr('chatsToBlock'), 'callback_data' => 'chats '.$chat['user_id'].' setblock'));
				if($chat['rights'] <> 'user') $configarray[]=array(array('text' => $tl->getstr('chatsDelete'), 'callback_data' => 'chats '.$chat['user_id'].' del'));
				$configarray[]=array(array('text' => $tl->getstr('chatsAdmins'), 'callback_data' => 'chats '.$chat['user_id'].' admins'));

				$configarray[]=array(array('text' => $tl->getstr('zurueck'), 'callback_data' => 'chats'));
			}else{
				$tg->sendmessage($tg->user_id, $tl->getstr('chatNotFound'));
				exit;
			}
		}
	}
	if(!IsSet($tgres['args'][0])) {
		$chats=$chatadm->getallchats();
		foreach($chats AS $chat) {
			$chatrights='🔴';
			if($chat['rights'] == 'waiting') $chatrights='🟡';
			if($chat['rights'] == 'user') $chatrights='🟢';
			if(!$tg->testechat($chat['user_id'])) {
				$chatrights.='⛔';
			}
			$configarray[]=array(array('text' => $chatrights.$chat['first_name'].' '.date($tl->getstr('datetime_date'),$chat['lastkontakt']), 'callback_data' => 'chats '.$chat['user_id']));
		}
		$configarray[]=array(array('text' => $tl->getstr('abbruch'), 'callback_data' => 'chats back'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);
		if(!IsSet($answer)) $answer=$tl->getstr('chatsWaehle');
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>