<?php
	if($tg->rightslevel < 10 and !IsSet($bot_admins[$tg->user_id])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
		exit; // Nicht die nötigen Rechte
	}
	$vclass=new codeversion();
	
	// Version
	$gitcodearr=$vclass->git_codeversion();
	$gitoutputarray[]=$gitcodearr['commit'];
	$gitoutputarray[]=$gitcodearr['timestr'];
	$answer=$tl->getstr('versionGitCode',$gitoutputarray);
	
	// branches
	$branchesarray=$vclass->git_branches();
	if(count($branchesarray) > 1) { // Nur wenn mehrere Branches existieren
		$answer.="\n\n";
		foreach ($branchesarray as $branche) {
			$answer.=$branche['name'];
			if($branche['active'] == true) $answer.=' ✅';
			$answer.="\n";
		} 
	}
	$tg->sendmessage($tg->user_id,$answer);
	
	exit;
?>