<?php
if(!IsSet($bot_admins[$tg->user_id])) {
	error_log(__FILE__.' '.__LINE__.' Without right in this script!!!');
	exit;
}
$sql="UPDATE ".$database_table_main." SET rights='ignore' WHERE user_id=:chat_id"; // Rechte vom Chat auf user setzen
$stm_all=$pdo->prepare($sql);
$stm_all->bindParam(':chat_id',$tg->chat_id, PDO::PARAM_INT);
$stm_all->execute();
if($stm_all->errorInfo()[0]<>'00000') {
	error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
	if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
	exit;
}
$tg->sendmessage($tg->chat_id,$tl->getlangstr($tg->chat_language,'chatRevokeChatOK'));
exit;
?>