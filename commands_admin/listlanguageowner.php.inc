<?php
//Ü
$answer='';
if($tg->rightslevel < 10 and !IsSet($bot_admins[$tg->user_id])) { // Kein Mod
	$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
	exit;
}
$sql="SELECT user_name, first_name, last_name, botconfjson FROM ".$tg->database_table_telegram." WHERE botconfjson LIKE '%languageowner%'";
$stm_all=$pdo->prepare($sql);
//$stm_all->bindParam(':user_id',$arr[1], PDO::PARAM_INT);
$stm_all->execute();
if($stm_all->errorInfo()[0]<>'00000') {
	error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
	if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
	$res=json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
	$answer='Error';
}
if($stm_all->rowCount() > 0) {
	while($resarr=$stm_all->fetch(PDO::FETCH_ASSOC)) {
//		error_log(print_r($resarr,true));
		$configarray=json_decode($resarr['botconfjson'],true);
		if(IsSet($configarray['languageowner']) and count($configarray['languageowner'])>0) {
			$answer.=$tg->escapestr($resarr['user_name']).": ".$tg->escapestr($resarr['first_name']).", ".$tg->escapestr($resarr['last_name'])."\n";
//			$answer.=count($configarray['languageowner']);
			foreach($configarray['languageowner'] AS $langname => $langstatus) {
				if(preg_match('/^([a-z0-9]+)$/i', $langname)) {
					$answer.=" - $langname\n";
				}
			}
		}
	}
}
if($answer == '') $answer="Nichts gefunden";
$tg->sendmessage($tg->user_id,$answer);

?>