<?php

	// Dieses Script dient zum Testen von neuen Funktionen ...
	
	if(!IsSet($bot_admins[$tg->user_id])) {
		$tg->sendmessage($tg->user_id, 'You are not a Admin');
		exit; // Nicht die nötigen Rechte
	}
	$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
	$sheetlist=$sheets->getSheetsTitles($ph->portalhuntsheet);
	$logmess->output('test','text');
	exit;
?>