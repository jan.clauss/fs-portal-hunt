<?php
	if($tg->user_id <> $bot_server_owner) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinServerOwner'));
		exit;
	}
//	$tg->sendmessage($tg->user_id,json_encode($tgres['args']));
	if(!IsSet($tgres['args'][0])) {
		// Falsch aufgerufen
		$tg->sendmessage($tg->user_id, $tl->getstr('langSetLangFalschAufruf'));
		exit;
	}
	if(!preg_match('/^[0-9]+$/',$tgres['args'][0])) {
		// Keine Nutzer ID -> Nutzer suchen
		$searchstring="%".strtolower($tgres['args'][0])."%";
		$sql="SELECT user_id, user_name, first_name, last_name, lastkontakt FROM ".$tg->database_table_telegram." WHERE lower(user_name) LIKE :searchstring or lower(first_name) LIKE :searchstring or lower(last_name) LIKE :searchstring";
		$stm_all=$pdo->prepare($sql);
		$stm_all->bindParam(':searchstring',$searchstring, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			exit();
//			$res=json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount()==0) {
			$tg->sendmessage($tg->user_id,$tl->getstr('searchNoUserFound'));
			exit;
		}
		while($resarr=$stm_all->fetch(PDO::FETCH_ASSOC)) {
			$configarray[]=array(array(
				'text' => $resarr['user_name'].": ".$resarr['first_name'].", ".$resarr['last_name']." - ".date($tl->getstr('datetime_datetime'),$resarr['lastkontakt']),
				'callback_data' => 'setlanguageowner '.$resarr['user_id']
			));
		}
		$answer=$tl->getstr('langSelectUser');
	}else{ // Nutzer-ID übergeben, direkt in die Sprachwahl
		$select_user=$tg->getuserds($tgres['args'][0]);
		if($select_user['ok'] <> true) { // Nicht gefunden
			$tg->sendmessage($tg->user_id,$tl->getstr('searchNoUserFound'));
			exit;
		}
		$outputtyp='csv';
		if(IsSet($select_user['botconf']['languageexport']['output'])) $outputtyp=$select_user['botconf']['languageexport']['output'];
		if(IsSet($tgres['args'][1])) {
			// Funktion aufgerufen
			switch($tgres['args'][1]) {
				case 'outputselect':
					switch($tgres['args'][2]) {
						case 'csv':
							$outputtyp='sheet';
						break;
//						case 'sheet':
//							$outputtyp='po';
//						break;
						default: // alles andere zu CSV:
							$outputtyp='csv';
						break;
					}
					$select_user['botconf']['languageexport']['output']=$outputtyp;
					$tg->writebotconf($select_user['user_id'], $select_user['botconf']);
					unset($tgres['args'][1]);
				break;
				case 'sheetlink':
					if($tgres['message_typ'] == 'callback_query') {
						$answer=$tl->getstr('langGoogleSheetConfLinkGet',array($sheets_usermail));
						$tg->setcommand($tg->user_id, "setlanguageowner",array($select_user['user_id'],"sheetlink"));
					}else{
						if(trim($tgres['text']) == '-') {
							$select_user['botconf']['languageexport']['googlesheet']='';
							$tg->writebotconf($select_user['user_id'], $select_user['botconf']);
							unset($tgres['args'][1]);
							$tg->setcommand($tg->user_id,'');
						}elseif(preg_match('#/spreadsheets/d/([a-zA-Z0-9-_]+)#',$tgres['text'],$arr)) {
							// Gültiger Sheet Link
							$select_user['botconf']['languageexport']['googlesheet']=$arr[1];
							$tg->writebotconf($select_user['user_id'], $select_user['botconf']);
							unset($tgres['args'][1]);
							$tg->setcommand($tg->user_id,'');
						}else{
							$answer=$tl->getstr('langGoogleSheetConfLinkGetRepeat');
						}
					}
				break;
				case 'language':
					if(preg_match('/^[a-z]{2,100}$/i', $tgres['args'][2])) { // Sprache prüfen (Sprachkürzel)
						if(IsSet($select_user['botconf']['languageowner'][$tgres['args'][2]])) {
							unset($select_user['botconf']['languageowner'][$tgres['args'][2]]);
						}else{
							$select_user['botconf']['languageowner'][$tgres['args'][2]]=1;
						}
						$tg->writebotconf($select_user['user_id'], $select_user['botconf']);
						unset($tgres['args'][1]);
					}
				break;
				case 'newlanguage':
					if($tgres['message_typ'] == 'callback_query') {
						$answer=$tl->getstr('langNewLanguageGet');
						$tg->setcommand($tg->user_id, "setlanguageowner",array($select_user['user_id'],"newlanguage"));
					}else{
						if(preg_match('/^[a-z]{2,100}$/i',$tgres['text'],$arr)) {
							// Gültiges Sprachkürzel
							$select_user['botconf']['languageowner'][strtolower($tgres['text'])]=1;
							$tg->writebotconf($select_user['user_id'], $select_user['botconf']);
							unset($tgres['args'][1]);
							$tg->setcommand($tg->user_id,'');
						}else{
							$answer=$tl->getstr('langNewLanguageGetRepeat');
						}
					}
				break;
				default:
					$tg->sendmessage($tg->user_id,'Uppps');
					unset($tgres['args'][1]);
			}
		} // Funktionsaufruf beendet
		if(!IsSet($tgres['args'][1])) { // Hautemnü aufbauen
			switch($outputtyp) {
				case 'sheet':
					$outputstr='Google-Sheet';
				break;
				case 'po':
					$outputstr='po-File';
				break;
				default:
					$outputtyp='csv';
					$outputstr='CSV-File';
				break;
			}
			// Output-Typ
			$configarray[]=array(array('text' => $tl->getstr('langOutputTypSelect',array($outputstr)), 'callback_data' => "setlanguageowner ".$select_user['user_id']." outputselect $outputtyp"));
			// Google Sheet
			$configarray[]=array(array('text' => $tl->getstr('googleSheetConfLink'), 'callback_data' => "setlanguageowner ".$select_user['user_id']." sheetlink"));
			// Sprachen
			if(IsSet($select_user['botconf']['languageowner'])) { // Aus Config des Users, dadurch tauchen auch neue Sprachen, die zugeordnet aber noch nicht im Bot sind auf
				foreach($select_user['botconf']['languageowner'] AS $lang => $langactive) {
					if(preg_match('/^[a-z]{2,100}$/i', $lang)) { // Sprachen ausfiltern, nur Buchstaben
						$langarr[$lang]['active']=$langactive==1;
						$langarr[$lang]['desc']='';
					}
				}
			}
			$existlang=$tl->listlanguages(); // Existierende Sprachen
			foreach($existlang AS $langopt) {
				if(!IsSet($langopt['default']) or $langopt['default']<>true) {
					if(!IsSet($langarr[$langopt['name']]['active'])) $langarr[$langopt['name']]['active']=false;
					$langarr[$langopt['name']]['desc']=$langopt['desc'];
				}
			}
			foreach($langarr AS $lang => $langopt) { // Ausgeben
				$langsmile='❌';
				if($langopt['active'] == true) $langsmile='✔';
				$configarray[]=array(array('text' => "$lang - ".$langopt['desc']." $langsmile", 'callback_data' => "setlanguageowner ".$select_user['user_id']." language $lang"));
			}
			// Neue Sprache
			$configarray[]=array(array('text' => $tl->getstr('langNewLanguage'), 'callback_data' => "setlanguageowner ".$select_user['user_id']." newlanguage"));
		}
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			$googlesheet='-';
			if(IsSet($select_user['botconf']['languageexport']['googlesheet'])) $googlesheet=$select_user['botconf']['languageexport']['googlesheet'];
			$autputtyp='csv';
			if(IsSet($select_user['botconf']['languageexport']['output'])) $outputtyp=$select_user['botconf']['languageexport']['output'];
			switch($outputtyp) {
				case 'sheet':
					$outputstr='Google-Sheet';
				break;
				case 'po':
					$outputstr='po-File';
				break;
				default:
					$outputtyp='csv';
					$outputstr='CSV-File';
				break;
			}
			$googlesheettitle=$googlesheet;
			if($googlesheet <> '-') {
				$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
				if($title=$sheets->getTitle($googlesheet)) {
					$googlesheettitle=$title;
				}else{
					$sheetlink=$tl->getstr('googleSheetException'.$sheets->lasterr->error->code,array(),$tl->getstr('googleSheetExceptionDefault'));
				}
			}
			$answer=$tl->getstr('langOwnerConfKopf',array(
				$select_user['user_name'].": ".$tg->escapestr($select_user['first_name']).", ".$tg->escapestr($select_user['last_name']),
				$googlesheet,
				$googlesheettitle,
				$outputstr
			));
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			$answer['message_id']=$tgres['message_id'];
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>