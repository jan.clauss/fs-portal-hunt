<?php
	if($bot_admins_use == true) {
		// Nur Bot-Admins dürfen Rechte ändern, siehe "hiddenvariables.*.php.inc"
		if(!IsSet($bot_admins[$tg->user_id])) {
			$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinAdmin'));
			exit;
		}
	}else{
		// Alle Mods dürfen Rechte ändern
		if($tg->rightslevel < 10) { // Kein Mod
			$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
			exit;
		}
	}
//	$tg->sendmessage($tg->user_id,json_encode($tgres['args']));
	if(!IsSet($tgres['args'][0])) {
		// Falsch aufgerufen, es muss der Teil eines Namens übergeben werden
		$tg->sendmessage($tg->user_id, $tl->getstr('setuserstatusFalschAufruf')); // Token anpassen
		exit;
	}
	if(!preg_match('/^[0-9]+$/',$tgres['args'][0])) {
		// Keine Nutzer ID -> Nutzer suchen
		$searchstring="%".strtolower($tgres['args'][0])."%";
		$sql="SELECT user_id, user_name, first_name, last_name, rights, lastkontakt FROM ".$tg->database_table_telegram." WHERE lower(user_name) LIKE :searchstring or lower(first_name) LIKE :searchstring or lower(last_name) LIKE :searchstring";
		$stm_all=$pdo->prepare($sql);
		$stm_all->bindParam(':searchstring',$searchstring, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			exit;
		}
		if($stm_all->rowCount()==0) {
			$tg->sendmessage($tg->user_id,$tl->getstr('searchNoUserFound'));
			exit;
		}
		while($resarr=$stm_all->fetch(PDO::FETCH_ASSOC)) {
			$configarray[]=array(array(
				'text' => $resarr['user_name'].": ".$resarr['first_name'].", ".$resarr['last_name']." - ".date($tl->getstr('datetime_datetime'),$resarr['lastkontakt'])." - ".$resarr['rights'],
				'callback_data' => 'setuserstatus '.$resarr['user_id']
			));
		}
		$answer=$tl->getstr('setuserstatusSelectUser');
	}else{
		// Nutzer-ID übergeben
		$select_user=$tg->getuserds($tgres['args'][0]); // Userdaten laden
		if(IsSet($tgres['args'][1])) {
			// Argument übergeben
			switch($tgres['args'][1]) {
				case 'cancel':
					// Abbruch
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					$tg->sendmessage($tg->user_id,$tl->getstr('abgebrochen'));
					exit;
				break;
				case 'mod':
				case 'user':
					$ret=$tg->setuserrights($tgres['args'][0],$tgres['args'][1],$bot_admins_use);
					if($ret['ok'] == false) {
						$answer=$tl->getstr('rightsErrorBySet')."\n";
						$answer.=$ret['description'];
					}else{
						$answer=$tl->getstr('rightsOKMessage',array($tg->escapestr($select_user['user_name']), $tg->escapestr($select_user['first_name']), $tg->escapestr($select_user['last_name']), $tgres['args'][1]));
					}
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					$tg->sendmessage($tg->user_id,$answer);
					exit;
				break;
			}
		}
		if($select_user['ok'] <> true) { // Nicht gefunden
			$tg->sendmessage($tg->user_id,$tl->getstr('searchNoUserFound'));
			exit;
		}
		$newrights='user';
		$rightsbutton=$tl->getstr('setuserstatusDegrade');
		if($select_user['rights']=='user') {
			$newrights='mod';
			$rightsbutton=$tl->getstr('setuserstatusUpgrade');
		}
		$configarray[]=array(array('text' => $rightsbutton, 'callback_data' => "setuserstatus ".$select_user['user_id']." ".$newrights));
		$configarray[]=array(array('text' => $tl->getstr('abbruch'), 'callback_data' => "setuserstatus ".$select_user['user_id']." cancel"));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);
		if(!IsSet($answer)) {
			$answer=$tl->getstr('setuserstatusKopf',array(
				$tg->escapestr($select_user['user_name']).": ".$tg->escapestr($select_user['first_name']).", ".$tg->escapestr($select_user['last_name']),
				$select_user['rights']
			));
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			$answer['message_id']=$tgres['message_id'];
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>