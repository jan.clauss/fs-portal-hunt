<?php
		$answer='';
		$pregarr['col']=strtoupper($pregarr['col']);
		if($tgres['username'] <> '') {
			$answer.=$tg->escapestr($tgres['username']);
			$agentname=$tgres['username'];
		}else{
			$answer.=$tgres['user_id'];
			$agentname=$tgres['user_id'];
		}
		$answer.="\n";
		$answer.=$pregarr['col'].'-'.$pregarr['row']."\n";
		$answer.="https://intel.ingress.com/?pll=$pregarr[lat],$pregarr[lng]\n";
		$answer.=$tg->escapestr($pregarr['name'])."\n";
		// Zeit prüfen
		if($ph->starttime > time()) {
			$answer.="\n⚠ ".$tl->getlangstr($tg->chat_language,'PHSheetFalseTime',[date($tl->getlangstr($tg->chat_language,'datetime_datetime'),$ph->starttime)]);
			$tg->sendmessage($tg->chat_id, $answer);
			exit;
		}
		// Sheet Klasse, wegen Zeichenumwandlungen, später zum lesen
		$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
		// Range prüfen
		$spaltennummer=$sheets->columnDez($pregarr['col']);
		if(!IsSet($ph->passcodesheetdata[$spaltennummer]) or $ph->passcodesheetdata[$spaltennummer]['portals'] < $pregarr['row']) {
			$answer.="\n⚠ ".$tl->getlangstr($tg->chat_language,'PHSheetOutOfRange');
			$tg->sendmessage($tg->chat_id, $answer);
			exit;
		}
		$portalstatus='waiting'; // Wartet auf Bearbeitung
		if($ph->isInQueue($tg->chat_id, $pregarr['col'], $pregarr['row'], $pregarr['lat'], $pregarr['lng'])) {
			$answer.="✔ ".$tl->getlangstr($tg->chat_language,'PHSheetAlreadyInQueue')."\n";
			$portalstatus='ok'; // Bereits eingetragen
		}else{
			if($ph->portalhuntsheet == '') {
				// Kein Sheet verknüpft
				$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'NoPortalHuntSheet'));
				$answer.="\n⏳";
			}else{
				// Daten aus Sheet auslesen
				$range=$sheetsites[$pregarr['col']].'!'.$ph->portaldatarange;
//				$answer.="$range\n"; // !Debug
				$rows=$sheets->getRange($ph->portalhuntsheet,$range);
				if(!IsSet($rows['values']) or $rows['values'] == null) {
					$answer.="⚠ ".$tl->getlangstr($tg->chat_language,'PHSheetNoRowsSet')."\n\n⏳";
				}else{
//					$answer.=$rows['values'][$pregarr['row']-1][3]."\n"; // !Debug
					if(!IsSet($rows['values'][$pregarr['row']-1][3])) {
						//Ist leer
						if($ph->autoinsertifempty) { // Direkt einfügen
							$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->chat_id);
							$link="https://intel.ingress.com/?pll=".$pregarr['lat'].",".$pregarr['lng'];
							$range=$sheets->decoderange($ph->portaldatarange); // Normale Datenrange zerlegen
							$writerange=$sheets->dezColumn($sheets->columnDez($range['column'][0])+1).($range['row'][0]+$pregarr['row']-1).':'.$range['column'][1].($range['row'][0]+$pregarr['row']-1);
							$range=$sheetsites[$pregarr['col']].'!'.$writerange;
							$values=[[$pregarr['name'], $agentname, $link]];
							$res=$sheets->update($ph->portalhuntsheet,$range,$values);
							if(is_object($res) and IsSet($res->updatedCells) and $res->updatedCells == 3) { // Erfolgreich
								$phsheet->writeportal($pregarr['col'], $pregarr['row'], $link, $agentname);
								$puimage->sendportalstatus($pregarr['col'], $pregarr['row'],true, $ph->fevgamesid);
								$portalstatus='ok';
								$answer.="✔ ".$tl->getlangstr($tg->chat_language,'PHSheetInsert')."\n";
							}else{ // Fehler beim schreiben ins Sheet
								$answer.="\n⏳";
							}
						}else{
							$answer.="\n⏳";
						}
					}else{
						if(preg_match('/pll=([-0-9.]+),([-0-9.]+)/',$rows['values'][$pregarr['row']-1][3],$arr)){
							if($arr[1] == $pregarr['lat'] and $arr[2] == $pregarr['lng']) {
								$answer.="✔❗ ".$tl->getlangstr($tg->chat_language,'PHSheetAlreadySet')."\n";
								$portalstatus='ok'; // Bereits eingetragen
							}else{
								$answer.="\n❗⏳ ".$tl->getlangstr($tg->chat_language,'PHSheetOtherPortalLink')."\n";
							}
						}else{
							$answer.="\n❗⏳ ".$tl->getlangstr($tg->chat_language,'PHSheetNoPortalLink')."\n";
						}
					}
				}
			}
		}
		// Prüfen ob Usernachricht gelöscht werden soll
		if($ph->deleteoriginalmessage == true) {
			$result=$tg->deletemessage($tg->chat_id, $tgres['message_id']);
			if($result['ok'] <> true) {
				$logmess->output('chat_rights',$tl->getlangstr($tg->chat_language,'BotNeedsDeleteRights',[$tg->chat_title]));
//				$tg->sendmessage($tg->chat_id,$tl->getlangstr($tg->chat_language,'BotNeedsDeleteRights'));
			}
		}
		$return=$tg->sendmessage($tg->chat_id, $answer);
		if($return['ok'] == true)
		{
			if($portalstatus == 'waiting')
			{
//				$tg->sendmessage($tg->chat_id, time() - $ph->getoldesttimeportal($tg->chat_id)); // !Debug
				if($ph->getoldesttimeportal($tg->chat_id) < time()-($ph->nextoldtime))
				{ // Nachricht nur, wenn kein Portal mehr in der Queue oder älter als 5min
					// MODs informieren
					$admins=$chatadm->getadmins($tg->chat_id);
					foreach($admins AS $admin)
					{
						$adminuserds=$tg->getuserds($admin);
						$tg->sendmessage($admin, $tl->getlangstr($adminuserds['language'],'PHAdminInfoNewPortals',[$tg->chat_title]));
					}
				}
				// Portaldaten, message id... in PortalHuntClass speichern
				$ph->addportal($agentname, $pregarr['col'], $pregarr['row'], $pregarr['lat'], $pregarr['lng'], $pregarr['name'],$tg->chat_id, $return['result']['message_id']);
			}
		}else{
			error_log(__FILE__.' '.__LINE__.' Chat eintrag lieferte kein [ok] == true');
			$tg->sendmessage($tg->chat_id, '❗');
		}
		
		exit;

?>