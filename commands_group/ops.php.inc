<?php
	$member=$tg->getChatMember($tg->chat_id, $tg->user_id);
	if($member['result']['status'] == 'creator' or $member['result']['status'] == 'administrator') {
		// Administrator
		$admins=$tg->getChatAdministrators($tg->chat_id);
		$ret=$chatadm->setadmins($tg->chat_id, $admins);
		if($ret['ok'] == true) $return='✔'; else $return='❌';
		$tg->sendmessage($tg->chat_id, $return);
	}else{
		$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'group_onlyAdmins')); // User-Helptext
	}
	exit;
?>