<?php
	$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'group_helptext'));
	if($tg->chat_rightslevel < 8) { // Weniger als userrights
		$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'new_chat_participantNeedRights'));
	}
	if(IsSet($bot_admins[$tg->user_id])) {
		$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'group_adminhelptext'));
	}
	exit;
?>