<?php
	$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'new_chat_participant'));
	if($tg->chat_rightslevel < 8) { // Weniger als userrights
		$tg->sendmessage($tg->chat_id, $tl->getlangstr($tg->chat_language,'new_chat_participantNeedRights'));
	}
	exit;
?>