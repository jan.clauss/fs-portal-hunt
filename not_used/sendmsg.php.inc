<?php

// Leider nicht nutzbar, da die 'normalen' Nutzer nicht am Bot angemeldet sind. Der Bot hat nicht deren ID und darf ihnen nichts senden
	if(!IsSet($bot_sendmsg)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgNotAktivated'));
		exit;
	}
	
	$sendOK=false; // Nur wenn true, werden Nachrichten gesendet
	
	if(is_array($bot_sendmsg)) { // $bot_sendmsg ist ein Array mit User_id s
		if(in_array($tg->user_id,$bot_sendmsg)) {
			$sendOK=true;
		}
	}elseif($bot_sendmsg == 'admin') {
		// Nur Bot-Admins dürfen Nachrichten senden "hiddenvariables.*.php.inc"
		if(!IsSet($bot_admins[$tg->user_id])) {
			$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgOnlyAdmins'));
			exit;
		}
		$sendOK=true;
	}elseif($bot_sendmsg == 'mod') {
		// Nur Chat-Admins dürfen nachrichten senden
		$admins=$chatadm->getadmins($tg->botconf['activchat_id']);
		error_log(json_encode($admins));
		if(!in_array($tg->user_id,$admins)) { // Kein Mod
			$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgOnlyMods'));
			exit;
		}
		$sendOK=true;
	}
	if($sendOK == false) {
			$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgFalseParameters'));
			exit;
	}
	if(trim(substr($tgres['text'],strlen($tgres['command']))) == '' and ($tgres['message_typ']<>'command' or $tgres['command'] <> 'sendmsg')) { //nix übergeben und keine Antwort
		$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgTextInput'));
		$tg->setcommand($tg->user_id, 'sendmsg');
		exit;
	}
	$tg->setcommand($tg->user_id, '');
	if($tgres['message_typ'] == 'query') { // Eingabe hinter Befehl
		$sendmsgtext=trim(substr($tgres['text'],strlen($tgres['command'])));
	}else{ // Eingabe über commando / Abgfrage
		$sendmsgtext=trim($tgres['text']);
	}
	
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);
	
	$agentnames=$phsheet->getaktiveagents(); // durch true werden auch Daten ohne Endupload geladen
	if(!is_array($agentnames) or count($agentnames) == 0) {
		error_log('Agents konnten nicht abgerufen werden');
		$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgNoAgents'));
		exit;
	}
	foreach ($agentnames as $agent) {
		$agent=$tg->getuseridfromname($agent);
		if($agent['ok'] == true) {
			$agents['agents']=$agent['user_id'];
		}
	}
	if(!IsSet($agents) or count($agents['agents']) == 0) {
		$tg->sendmessage($tg->user_id, $tl->getstr('sendmsgNoAgents'));
		exit;
	}
	
	$sendmsgtext=$tg->escapestr($sendmsgtext);
	$sendmsgtext = preg_replace('/\b([a-z]{3}\d{2}[a-z]+\d{3}[a-z]{2})\b/im', '<code>$1</code>', $sendmsgtext); // Idee von @DhrMekmek, wenn ein FS-Passcode im Text ist, diesen in <code> einschließen
	$sendmsgtext = '@'.$tg->username.': '.$sendmsgtext;
	
	$sendagents='';
	$sendok=0;
	$sendfail=0;
	if(IsSet($agents['agents'])) {
		$agents=$agents['agents'];
		$maxerrcount=3;
		foreach($agents AS $agent) {
			$result=array('ok' => false);
			$errcount=0;
			while($result['ok'] == false and $errcount < $maxerrcount) {
				$errcount++;
				$result=$tg->sendmessage($agent['user_id'],$sendmsgtext);
				if($result['ok'] == true) {
					$sendagents.='✔'.$agent['name']."\n";;
					$sendok++;
				}else{
					$sendagents.='❌';
						// /telegramclass.php.inc 288 Array\n(\n    [ok] => \n    [error_code] => 403\n    [description] => Forbidden: bot was blocked by the user\n)\n
					error_log(__FILE__.' '.__LINE__.' '.json_encode($result)); // Ist zwar doppelt im Log, da die Meldung auch schon aus der telegramclass kommt, aber so ist nachvollziehbar, wo sie her kommt.
					switch($result['error_code']) {
						case 403: // Agent hat Bot gelöscht und blockiert
							$errtext='blocked';
							$wait=0;
							$errcount = $maxerrcount; // Nicht nochmal senden
						break;
						case 420: // https://core.telegram.org/api/errors#420-flood
							$wait=5; // Fünf Sekunden by default
							if(preg_match('/FLOOD_WAIT_(\n+):/', $result['description'],$arr)) $wait=$arr[1]; // Von Telegramserver gemeldete Sekunden
							$errtext='wait';
						break;
						case 429: // https://core.telegram.org/bots/faq#how-can-i-message-all-of-my-bots-subscribers-at-once
							$wait=5;
							$errtext='wait';
						break;
						default:
							$wait=5;
							$errtext='undef-err';
						break;
					}
					$sendagents.="$errtext ".$agent['name']."\n";
					if($wait > 0) {
						sleep($wait);
						$answer=$tl->getstr('sendmsgAgents',array($sendagents));
						if(IsSet($admmessage_id)) $answer=array('text' => $answer, 'message_id' => $admmessage_id);
						$admresult=$tg->sendmessage($tg->user_id,$answer);
						if($admresult['ok'] == true) {
							$admmessage_id=$admresult['result']['message_id'];
						}
					}
					if($errcount == $maxerrcount) $sendfail++;
				}
			}
		}
		$answer=$tl->getstr('sendmsgAgents',array($sendagents, $sendok, $sendfail));
//		error_log(__FILE__.' '.__LINE__." $sendagents, $sendok, $sendfail");
		if(IsSet($admmessage_id)) $answer=array('text' => $answer, 'message_id' => $admmessage_id);
		$admresult=$tg->sendmessage($tg->user_id,$answer);
	}
	exit;
?>