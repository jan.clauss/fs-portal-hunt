<?php

	function tgmigratechatinfo($old, $new) { // Chat-ID wechselt, group -> supergroup
		global
			$pdo, $database_table_main;
		// db_chatadm Chat-IDs korrigieren
		$sql="UPDATE ".$database_table_main."_chatadm SET chat_id=:chat_id WHERE chat_id=:from_chat_id";
		$stm_all=$pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$new, PDO::PARAM_INT);
		$stm_all->bindParam(':from_chat_id',$old, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
		}
		// db_Portale Chat-IDs korrigieren
		$sql="UPDATE ".$database_table_main."_Portale SET chat_id=:chat_id WHERE chat_id=:from_chat_id";
		$stm_all=$pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$new, PDO::PARAM_INT);
		$stm_all->bindParam(':from_chat_id',$old, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
		}
		// db_Sheet Chat-IDs korrigieren
		$sql="UPDATE ".$database_table_main."_Sheet SET chat_id=:chat_id WHERE chat_id=:from_chat_id";
		$stm_all=$pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$new, PDO::PARAM_INT);
		$stm_all->bindParam(':from_chat_id',$old, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
		}
	}
?>