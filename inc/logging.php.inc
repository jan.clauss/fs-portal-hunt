<?php

	function logthis($file, $line, $level, $text, $logfile='main')
	{
		// Logmessaging-Konfiguration
		$logmess=logmessaging::getInstance();
		$oldlogdir=$logmess->logdir; // gesetztes Verzeichnis zwischenspeichern
		
		if(IsSet($bot_errorlog)) { // Logverzeichnis konfiguriert
			$logmess->setlogdir($bot_errorlog);
		}else{ // Logverzeichnis selber bestimmen
			$logmess->setlogdir((empty($DOCUMENT_ROOT) ? dirname(dirname(__FILE__)).'/logs' : str_replace("/", DIRECTORY_SEPARATOR, dirname($DOCUMENT_ROOT))) . DIRECTORY_SEPARATOR).'/logs';
		}
		if(IsSet($logmess_replace)) {  // Zu ersetzende Texte konfiguriert
			$logmess->replacestrings=$logmess_replace; // Strings aus der Hiddenvariables, die ersetzt werden sollen
		}
		$logmess->codeoutput($file, $line, $level, 'logthis', $text, $logfile='main');
		
		if($oldlogdir <> $logmess->logdir) {
			$logmess->setlogdir($oldlogdir);
		}
	}
	
?>