<?php
//Ü
$languageger['language_name']                     =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Der Name der Sprache', 'text' => 'Deutsch'); // Dieser Token sollte immer vorhanden sein
$languageger['language_author']                   =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Bearbeiter der Übersetzung, bei Bedarf', 'text' => 'janc70'); // Dieser Token sollte immer vorhanden sein

// Sicherheitsabfrage
$languageger['safetyquery_ja']                    =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Savetyquery (Ja, Yes, да)', 'text' => 'Ja');
$languageger['safetyquery_nein']                  =array('date' => '2021-01-10', 'dest' => 'all',  'desc' => 'Savetyquery (Nein, No, Нет)', 'text' => 'Nein');

// Datumzeit
$languageger['datetime_date']                     =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für das Datum', 'text' => 'd.m.Y');
$languageger['datetime_time']                     =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für die Zeit', 'text' => 'H:i:s');
$languageger['datetime_HourMin']                  =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer nur Stunden,Minuten', 'text' => 'H:i');
$languageger['datetime_timeWOSecound']            =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für die Zeit', 'text' => 'H:i');
$languageger['datetime_datetimeWOSecound']        =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für die Datum,Zeit ohne Sekunden', 'text' => 'd.m.Y H:i');
$languageger['datetime_datetime']                 =array('date' => '2020-12-30', 'dest' => 'all',  'desc' => 'Formatierer für Datum, Zeit', 'text' => 'd.m.Y H:i:s');
$languageger['datetime_monthday']                 =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für Tag, Monat', 'text' => 'd.m.');
$languageger['datetime_datetimeWOYear']           =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Formatierer für Datum, Zeit ohne Jahr und Sekunden', 'text' => 'd.m. H:i');
$languageger['datetime_StrSecunden']              =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Text Sekunden', 'text' => 'Sekunden');
$languageger['datetime_StrMinuten']               =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Text Minuten', 'text' => 'Minuten');
$languageger['datetime_StrStunden']               =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Text Stunden', 'text' => 'Stunden');

$languageger['webcmdhelp']                        =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Undefinierter Webaufruf', 'text' => "Falsch aufgerufen, Optionen:
 ?createdb -> Erstellt die Datenbanken
 ?instwebhook -> Setzt den Telegram Webhook
 ?uninstwebhook -> Löscht den Telegram Webhook
 ?sendmsg -> Sendet eine Testnachricht an den bot_owner");

$languageger['Zeit']                              =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Zeit (time)', 'text' => 'Zeit');
$languageger['Zeitzone']                          =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Zeitzone (time zone)', 'text' => 'Zeitzone');
$languageger['Status']                            =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Status (Rechte)', 'text' => 'Status');

$languageger['abgebrochen']                       =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Allgemein Abgebrochen', 'text' => 'Abgebrochen');
$languageger['dsgvotext']                         =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Der Text zur DSGVO Ausgabe', 'text' => "Deine Daten:\n[#1#]");
$languageger['gibstartein']                       =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Mit Start verbinden', 'text' => 'Gib /start ein um den Bot zu nutzen'); //!
$languageger['willkommen']                        =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Begrüßung bei /start', 'text' => 'Willkommen');
$languageger['zurueck']                           =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Zurück', 'text' => 'Zurück');
$languageger['verlassen']                         =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'verlassen', 'text' => 'Verlassen');
$languageger['abbruch']                           =array('date' => '2020-12-09', 'dest' => 'all',  'desc' => 'Abbruch', 'text' => 'Abbruch');

$languageger['botadmins']                         =array('date' => '2020-12-23', 'dest' => 'all',  'desc' => '/botadmins Liste der Botadmins', 'text' => "Botadmins:\n[#1#]");


// Chat:
$languageger['BotNeedsDeleteRights']              =array('date' => '2020-12-15', 'dest' => 'chat', 'desc' => 'Chat: löschen von Originalnachricht', 'text' => '[#1#]: Der Bot benötigt Rechte zum Löschen von Nachrichten');
$languageger['NoPortalHuntSheet']                 =array('date' => '2021-02-12', 'dest' => 'chat', 'desc' => 'Chat: Sheet, Kein Sheet verknüpft', 'text' => '@MODs: Kein Sheet verknüpft!');
$languageger['PHSheetNoRowsSet']                  =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: Sheet, Portalanzahl nicht gesetzt', 'text' => '@MODs: Prüfe das Sheet, ist die Anzahl der Portale gesetzt?');
$languageger['PHSheetFalseTime']                  =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: vor der Startzeit', 'text' => 'Erst ab [#1#]');
$languageger['PHSheetOutOfRange']                 =array('date' => '2021-02-12', 'dest' => 'chat', 'desc' => 'Chat: Kein im Sheet erwartetes Portal', 'text' => 'Diese Koordinate ist ausserhalb des erwarteten Bereichs');
$languageger['PHSheetInsert']                     =array('date' => '2021-02-08', 'dest' => 'chat', 'desc' => 'Chat: Portal (direkt) eingetragen', 'text' => 'Portal eingetragen');
$languageger['PHSheetAlreadySet']                 =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: Sheet, Portal schon eingetragen', 'text' => 'Portal bereits eingetragen');
$languageger['PHSheetAlreadyInQueue']             =array('date' => '2021-01-11', 'dest' => 'chat', 'desc' => 'Chat: Sheet, Portal schon gemeldet (in Queue)', 'text' => 'Portal schon gemeldet');
$languageger['PHSheetOtherPortalLink']            =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: Sheet, Anderes Portal im Sheet', 'text' => 'Anderes Portal eingetragen');
$languageger['PHSheetNoPortalLink']               =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: Sheet, irgendwas eingetragen', 'text' => 'Etwas eingetragen, aber kein Portallink');
$languageger['PHAdminInfoNewPortals']             =array('date' => '2020-12-18', 'dest' => 'chat', 'desc' => 'Chat: Neue Portale, Admins infomieren', 'text' => 'Neue Portale... [#1#] - /next');
$languageger['PHMapAdress']                       =array('date' => '2021-02-07', 'dest' => 'chat', 'desc' => 'Chat: Meldung, dass es nur eine Mapadresse ist', 'text' => "⚠ Mapadresse ⚠, enthält keinen Portallink\nhttps://janc70.rocks/HowTo/PortallinkExport");
$languageger['new_chat_participant']              =array('date' => '2020-12-23', 'dest' => 'chat', 'desc' => 'Chat: Meldung nach Hinzufügen zum Chat', 'text' => "Hi, Danke das ich Euch beim IFS Portal Hunt unterstützen darf.\nMit /help erfahrt Ihr mehr über meine Funktionsweise");
$languageger['new_chat_participantNeedRights']    =array('date' => '2020-12-23', 'dest' => 'chat', 'desc' => 'Chat: Chat muss freigeschalten werden', 'text' => 'Leider ist es nicht möglich, dass ich unbegrenzt Chats bediene. Verbinde Dich privat mit dem Bot und benutze das Kommando /botadmins um einen Admin anzuschreiben.');
$languageger['chatAddChatOK']                     =array('date' => '2020-12-23', 'dest' => 'chat', 'desc' => 'Chat: Chat Freigeben', 'text' => 'Ich darf Euch jetzt beim IFS Portal Hunt unterstützen :-)');
$languageger['chatRevokeChatOK']                  =array('date' => '2020-12-23', 'dest' => 'chat', 'desc' => 'Chat: Chat deaktivieren', 'text' => 'Ich wurde für diesen Chat deaktiviert :-(');
$languageger['group_onlyAdmins']                  =array('date' => '2020-12-27', 'dest' => 'chat', 'desc' => 'Chat: Nur mit Admin Rechten', 'text' => 'Das dürfen nur Admins');


$languageger['group_helptext']                    =array('date' => '2020-12-15', 'dest' => 'chat', 'desc' => 'Hilfstext für die Gruppe', 
	'text' => "Sende eine Nachricht mit den Koordinaten aus dem Bild, dem IntelLink des Portals und dem Portalnamen an diese Gruppe. Der Bot wird dies prüfen und an die Admins weiterreichen.

Zusätzliche Befehle:
 /ops - Aktualisiert die Admins der Gruppe im Bot
 /help - dieser Hilfetext"
);
$languageger['group_adminhelptext']               =array('date' => '2020-12-23', 'dest' => 'mod',  'desc' => 'Hilfstext für Bot_Admins in der Gruppe',
'text' => "/addchat - Schaltet den Chat für den Bot frei
/revokechat - Deaktiviert den Bot für diesen Chat
/blockchat - Der Chat reagiert ausschließlich auf addchat, revokechat und blockchat von Bot_Admins");

// Private:
$languageger['confKopf']                          =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: Config Kopf', 'text' => "Chat: [#1#]\nZeitzone: [#2#]\nSprache: [#3#]\n\nWähle eine Option");
$languageger['conftimezone']                      =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: löschen von Originalnachricht', 'text' => 'Zeitzone wählen');
	$languageger['TZselectContinent']               =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Priv: Zeitzone Kopf Kontinentwahl', 'text' => 'Wähle einen Kontinent');
	$languageger['TZselectLand']                    =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Priv: Zeitzone Kopf Stadtwahl', 'text' => 'Wähle eine Stadt');
	$languageger['TZneueZeitzone']                  =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Zeitzone Info', 'text' => 'Neue Zeitzone: [#1#]');
	$languageger['TZzoneInfo']                      =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Zeitzone Info', 'text' => "\nDeine Zeit: [#1#]\nServer Zeit: [#2#]\nUTC: [#3#]");
$languageger['conflanguage']                      =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Sprache wählen', 'text' => 'Sprache wählen');
	$languageger['conflangKannNichtLaden']          =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Kann Sprache nicht laden', 'text' => 'Konnte Sprache nicht laden');
	$languageger['conflangGewechselt']              =array('date' => '2020-12-16', 'dest' => 'all',  'desc' => 'Sprache gewechselt', 'text' => 'Sprache gewechselt');
$languageger['confselchat']                       =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: Config Chat auswählen', 'text' => 'Chat wählen');
	$languageger['confselchatNoChat']               =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: Config keine Chats gefunden', 'text' => "Keine Chats für Dich gefunden\nDu musst in den Chats Admin sein, dass Du sie hier siehst");
	$languageger['confselchatGewechselt']           =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: Config Chat gewechselt', 'text' => 'Chat ausgewählt');
	$languageger['confselchatNotFound']             =array('date' => '2020-12-15', 'dest' => 'all',  'desc' => 'Priv: Config Chat nicht gefunden', 'text' => 'Chat nicht gefunden');
$languageger['confsheet']                         =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Config PortalHuntSheet', 'text' => 'PortalHuntSheet');
	$languageger['confSheetKopf']                   =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Kopf des Sheet-Config-Menüs', 'text' => "[#1#]\nFreigabeuser: [#2#]");
	$languageger['confSheetLink']                   =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Sheets Sheetlink', 'text' => 'Link des Sheets');
	$languageger['confSheetLinkGet']                =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Sheets Sheetlink abfragen', 'text' => 'Gib den Link zum Sheet ein oder "-" zum löschen');
	$languageger['confSheetLinkGetRepeat']          =array('date' => '2020-11-12', 'dest' => 'mod',  'desc' => 'Sheets Sheetlink nochmal abfragen', 'text' => "Der Link war nicht gültig!\nNochmal oder Abbruch mit /cancel");
	$languageger['confSheetNames']                  =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Sheets Sheetnamen', 'text' => "Sheet-Namen");
	$languageger['confSheetRename']                 =array('date' => '2022-04-28', 'dest' => 'mod',  'desc' => 'Spreadsheets umbenennen', 'text' => "Spreadsheets umbenennen");
	$languageger['confSheetRange']                  =array('date' => '2020-12-19', 'dest' => 'mod',  'desc' => 'Sheets Data Range', 'text' => "Datenbereich (range)");
	$languageger['confSheetRangeGet']               =array('date' => '2020-12-19', 'dest' => 'mod',  'desc' => 'Sheets Data Range Abfrage', 'text' => "Gib den Bereich mit den Portaldaten an, nur Datenzeilen, beginnend bei der Portalnummer, Portalname, Discoverer bis zum Portallink (4 Zellen).\nDefault: A8:D17\nAbbruch mit /cancel");
	$languageger['confSheetRangeGetOK']             =array('date' => '2020-12-19', 'dest' => 'mod',  'desc' => 'Sheets Data Range Abfrage OK', 'text' => "Range OK");
	$languageger['confSheetRangeGetFail']           =array('date' => '2020-12-19', 'dest' => 'mod',  'desc' => 'Sheets Data Range Abfrage Fehler', 'text' => "Fehlerhafte Range, nicht gespeichert");
$languageger['confFS']                            =array('date' => '2021-02-21', 'dest' => 'mod',  'desc' => 'Priv: Config FS-Daten', 'text' => 'FS-/MyIngress-Daten');
	$languageger['confFSKopf']                      =array('date' => '2021-02-21', 'dest' => 'mod',  'desc' => 'Kopf des FS-Config-Menüs', 'text' => "FS-Daten:\n<a href='".FEVGAMESPATH."[#4#]'>Fevgames-ID [#4#]</a>\nTitel: [#1#]\nDatum: [#2#]\nOrt: [#3#]");
	$languageger['confFevId']                       =array('date' => '2021-02-21', 'dest' => 'mod',  'desc' => 'Button Fevgames abruf', 'text' => "von Fevgames abrufen");
	$languageger['confFevReadsheet']                =array('date' => '2022-04-28', 'dest' => 'mod',  'desc' => '/readsheet', 'text' => "Sheet einlesen");
	$languageger['confFevImgUpload']                =array('date' => '2021-02-26', 'dest' => 'mod',  'desc' => 'Link zum Image Upload', 'text' => "Image Upload");
	$languageger['confFevImgPortalSite']            =array('date' => '2021-04-03', 'dest' => 'mod',  'desc' => 'Link zum Image Web Portal', 'text' => "Image Web Portal");
	$languageger['confFSIdGet']                     =array('date' => '2021-03-05', 'dest' => 'mod',  'desc' => 'Gib ID der Fevgamesseite ein', 'text' => "Gib die ID der Fevgames-Seite ein");
	$languageger['confFSFevError']                  =array('date' => '2021-03-05', 'dest' => 'mod',  'desc' => 'Fehler beim Fevgames-Abruf', 'text' => "Fehler beim abrufen von Fevgames.\nStimmt die ID?");
	$languageger['confFSFevSave']                   =array('date' => '2021-03-05', 'dest' => 'mod',  'desc' => 'Fevgamesdaten gespeichert', 'text' => "Fevgames-Daten gespeichert");
$languageger['confchatKopf']                      =array('date' => '2020-12-30', 'dest' => 'mod',  'desc' => 'Priv: Config Chat Kopf', 'text' => 'Chat: [#1#]');
	$languageger['confchatDelUserMsg']              =array('date' => '2020-12-30', 'dest' => 'mod',  'desc' => 'Priv: Config Usernachricht löschen, wenn erkannt', 'text' => 'Usernachricht löschen');
	$languageger['confchatStartTime']               =array('date' => '2020-12-30', 'dest' => 'mod',  'desc' => 'Priv: Config Startzeit', 'text' => 'Startzeit:');
	$languageger['confchatAutoInsertIfEmpty']       =array('date' => '2021-02-08', 'dest' => 'mod',  'desc' => 'Priv: Automatisch einfügen, wenn leer', 'text' => 'Automatisch eintragen');
	$languageger['confchatNextOldTime']             =array('date' => '2021-02-08', 'dest' => 'mod',  'desc' => 'Priv: Ab der Zeit kommt bei jedem Portal eine /next-Meldung', 'text' => 'Maximales Portalalter bis Nerv: [#1#]min');
	$languageger['confchatQueueWarn']               =array('date' => '2021-03-11', 'dest' => 'mod',  'desc' => 'Priv: Warnung Portale vor Startzeit in Queue -> löschen', 'text' => '⚠ Alte Portale in Queue ⚠');
	$languageger['confchatQueueDel']                =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Queue löschen', 'text' => '⚠ Queue löschen');
		$languageger['confchatQueueDelQuest']         =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Queue löschen abfrage', 'text' => "Wirklich die Queue und im Bot gespeicherte Portale löschen? (Ja)\nAbbruch mit /cancel");
		$languageger['confchatQueueDelQuestDeleted']  =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Queue gelöscht', 'text' => 'Queue gelöscht');
		$languageger['confchatQueueDelQuestFail']     =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Queue löschen fehler', 'text' => 'Fehler beim löschen der Queue');
		$languageger['confchatQueueDelQuestCanceled'] =array('date' => '2020-12-17', 'dest' => 'mod',  'desc' => 'Priv: Queue löschen abgebrochen', 'text' => 'Queue löschen abgebrochen');

$languageger['deletealldataQuery']                =array('date' => '2020-12-18', 'dest' => 'all',  'desc' => 'Priv: Config Usernachricht löschen', 'text' => "Löscht alle Deine Daten und entfernt Dich aus dem Bot\nSicher? (Ja)");

$languageger['nextPortalInfoOffen']               =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Daten', 'text' => "Offen: [#1#]");
$languageger['nextPortalInfo']                    =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Daten', 'text' => "[#1#]\nLink: [#3#]\nName: [#4#]\nvon: [#5#]");
$languageger['nextPortalInfoReplace']             =array('date' => '2021-02-28', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Daten', 'text' => "[#1#]\nLink: [#3#]\nOld: [#4#]\nName: [#5#]\nvon: [#6#]");
$languageger['nextIgnoreEntry']                   =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Portal ignorieren', 'text' => "Verwerfen");
$languageger['nextInsert']                        =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Automatisch eingetragen', 'text' => "Eingetragen von");
$languageger['nextUpdate']                        =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Automatisch aktualisiert', 'text' => "Aktualisiert von");
$languageger['nextIgnore']                        =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal ignoriert', 'text' => "Verworfen von");
$languageger['nextNoSheet']                       =array('date' => '2020-01-10', 'dest' => 'mod',  'desc' => 'Priv: Kein Sheet verknüpft', 'text' => "⛔ Kein Sheet!");
$languageger['nextAutoInsert']                    =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Manuell eingetragen', 'text' => "Eintragen");
$languageger['nextAlreadySet']                    =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Manuell eingetragen', 'text' => "Bereits gesetzt");
$languageger['nextReplace']                       =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Eintrag ersetzen', 'text' => "Ersetzen");
$languageger['nextNoPortalsToSearch']             =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Nextportal Keine offenen Portale', 'text' => "Keine offenen Portale");
$languageger['nextWriteError']                    =array('date' => '2020-12-18', 'dest' => 'mod',  'desc' => 'Priv: Meldung bei Schreibfehler ins Sheet', 'text' => "⚠ Konnte nicht ins Sheet schreiben!");
$languageger['nextAlreadyProcessed']              =array('date' => '2020-12-27', 'dest' => 'mod',  'desc' => 'Priv: Hat schon wer anders bearbeitet', 'text' => "Bereits abgearbeitet!");
$languageger['nextToOldPortal']                   =array('date' => '2021-03-11', 'dest' => 'mod',  'desc' => 'Priv: Portal ist zu alt für diesen FS, Zeitausgabe', 'text' => "⚠ von [#1#]");

$languageger['readsheetNoSheet']                  =array('date' => '2020-12-30', 'dest' => 'mod',  'desc' => 'Priv: Kein Sheet verknüpft', 'text' => "Kein Sheet verknüpft!");
$languageger['readsheetWait']                     =array('date' => '2020-12-27', 'dest' => 'mod',  'desc' => 'Priv: Wartemeldung zum Einlesen der Sheetdaten', 'text' => "Bitte warte!");
$languageger['readsheetError001']                 =array('date' => '2021-01-11', 'dest' => 'mod',  'dest' => 'mod',  'desc' => 'Priv: Readsheet-Fehlermeldung Kein Sheet', 'text' => "Kein Sheet übergeben!");
$languageger['readsheetError002']                 =array('date' => '2021-01-11', 'dest' => 'mod',  'dest' => 'mod',  'desc' => 'Priv: Readsheet-Fehlermeldung Lesefehler', 'text' => "Lesefehler Sheet!");
$languageger['readsheetError003']                 =array('date' => '2021-01-11', 'dest' => 'mod',  'dest' => 'mod',  'desc' => 'Priv: Readsheet-Fehlermeldung Keine Daten', 'text' => "Keine Daten");
$languageger['readsheetErrorDefault']             =array('date' => '2021-01-11', 'dest' => 'mod',  'dest' => 'mod',  'desc' => 'Priv: Readsheet-Fehlermeldung Allgemein', 'text' => "Fehler bei Auswerten des Sheets!");
$languageger['readsheetTransfer']                 =array('date' => '2021-01-11', 'dest' => 'mod',  'dest' => 'mod',  'desc' => 'An myingress gesendet', 'text' => "An webportals.MyIngress.net gesendet");

$languageger['drawAll']                           =array('date' => '2020-12-29', 'dest' => 'mod',  'desc' => 'Priv: /draw Alle Draws', 'text' => "Alle Draws");
$languageger['drawWaehle']                        =array('date' => '2020-12-29', 'dest' => 'mod',  'desc' => 'Priv: /draw Wähle die Ausgabe', 'text' => "Wähle einen Draw");
$languageger['drawNoDrawData']                    =array('date' => '2020-12-30', 'dest' => 'mod',  'desc' => 'Priv: /draw Keine Daten für Draw', 'text' => "Keine Daten für ein Draw");

$languageger['chatNotFound']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Chat nicht gefunden', 'text' => "⚠ Chat nicht gefunden");
$languageger['chatsWaehle']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Wähle einen Chat', 'text' => "Wähle einen Chat");
$languageger['chatsOptions']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Wähle eine Aktion', 'text' => "Wähle eine Aktion");
$languageger['chatsToActiv']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Setze Chat aktiv', 'text' => "Setze Chat aktiv");
$languageger['chatsToIgnore']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Deaktiviere Chat', 'text' => "Deaktiviere Chat");
$languageger['chatsToBlock']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Blockiere Chat', 'text' => "Blockiere Chat");
$languageger['chatsDelete']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Chat löschen', 'text' => "❗ Chat löschen");
$languageger['chatsAdmins']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Admins ermitteln', 'text' => "Admins");
$languageger['chatsAdminsAktualisiert']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Admins aktualisiert', 'text' => "Admins aktualisiert");
$languageger['chatsNoAdmins']=array('date' => '2020-12-29', 'desc' => 'Priv: /chats Keine Admins', 'text' => "Keine Admins gefunden");

$languageger['rightsDuKeinMod']=array('date' => '2020-12-19', 'desc' => 'Priv: Keine MOD-Rechte', 'text' => "Du bist kein MOD");
// Setrights
$languageger['rightsOKMessage']=array('date' => '2020-12-19', 'desc' => 'Rechtemeldung', 'text' => "Neue Rechte von [#1#], [#2#] [#3#]:\n[#4#]");
$languageger['searchNoUserFound']=array('date' => '2020-12-19', 'desc' => 'Searchuser keine Nutzer gefunden', 'text' => 'Keine passenden Nutzer gefunden');
$languageger['setuserstatusFalschAufruf']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner falsch aufgerufen',  'text' => 'Übergib einen Teil des Namens oder die ID des Nutzers');
$languageger['setuserstatusSelectUser']=array('date' => '2020-12-19', 'desc' => 'setuserstatus Nutzer wählen', 'text' => 'Wähle einen Nutzer');
$languageger['setuserstatusUpgrade']=array('date' => '2020-12-19', 'desc' => 'setuserstatus Button für Rights', 'text' => 'Upgrade zum MOD');
$languageger['setuserstatusDegrade']=array('date' => '2020-12-19', 'desc' => 'setuserstatus Button für Rights', 'text' => 'Degradiere zum User');
$languageger['setuserstatusKopf']=array('date' => '2020-12-19', 'desc' => 'Setuserstatus Kopf Rechte-Menüs', 'text' => "[#1#]\nAktuell [#2#]");

$languageger['renamesheetsUmbenennen']            =array('date' => '2021-05-01', 'desc' => 'renamesheet Text Umbenennen', 'text' => 'Umbenennen');
$languageger['renamesheetNoSheet']                =array('date' => '2021-05-01', 'desc' => 'renamesheet Kein Sheet eingestellt', 'text' => 'Es ist kein Sheet eingestellt');
$languageger['renamesheetOriginalName']           =array('date' => '2021-05-01', 'desc' => 'renamesheet Text Umbenennen', 'text' => 'Die Original Sheetbezeichnung ist eingestellt. Die Umwandlung geht nur von Original nach einer anderen Einstellung');

$languageger['langSetLangFalschAufruf']=array('date' => '2020-12-30', 'desc' => 'SetLanguageOwner falsch aufgerufen',  'text' => "⚠ Falsch aufgerufen. Übergib einen Teil des Namens oder die Telegram-Id des Nutzers\nDie Telegram-Id liefert /searchuser\n\n<code>/setlanguageowner [name]</code> oder\n<code>/setlanguageowner [TgID] [lang][0 | 1]</code>");
$languageger['langSelectUser']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Nutzer wählen', 'text' => 'Wähle einen Nutzer');
$languageger['langOwnerConfKopf']=array('date' => '2020-12-19', 'desc' => 'SetLanguage Kopf des Config-Menüs', 'text' => "User: [#1#]\n<a href='https://docs.google.com/spreadsheets/d/[#2#]'>[#3#]</a>\nAusgabeformat: [#4#]");
$languageger['langOutputTypSelect']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Ausgabetyp wahl', 'text' => 'Ausgabetyp: [#1#]');
$languageger['langNewLanguage']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Neue Sprache anlegen', 'text' => "Neue Sprache anlegen");
$languageger['langNewLanguageGet']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Neue Sprache abfragen', 'text' => "Gib das Kürzel für die Sprache ein, nur Buchstaben!");
$languageger['googleSheetConfLink']=array('date' => '2020-12-19', 'desc' => 'Sheets Sheetlink', 'text' => 'Link des Sheets');
$languageger['googleSheetUpdatedRows']=array('date' => '2021-01-01', 'desc' => 'exportlanguage Sheets Zeilen aktualisiert', 'text' => 'Es wurden [#1#] Zeilen im Sheet aktualisiert');
$languageger['langGoogleSheetConfLinkGet']=array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Sheets Sheetlink abfragen', 'text' => "Gib den Link zu einem leeren Sheet ein oder \"-\" zum löschen\nDas Sheet muss für [#1#] freigegeben werden.");
$languageger['langGoogleSheetConfLinkGetRepeat']  =array('date' => '2020-12-19', 'desc' => 'SetLanguageOwner Sheets Sheetlink nochmal abfragen', 'text' => "Der Link war nicht gültig!\nNochmal oder Abbruch mit /cancel");
$languageger['googleSheetExceptionDefault']       =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code unbekannt', 'text' => '⚠ GoogleSheet Fehler');
$languageger['googleSheetException400']           =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code 400 Unable to parse range: Data!A1:Z', 'text' => '🚫 Bereich zum Schreiben nicht gefunden');
$languageger['googleSheetException403']           =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code 403 The caller does not have permission', 'text' => '🚫 Keine Zugriffs-/Schreibrechte auf das ScoringSheet');
$languageger['googleSheetException404']           =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code 404 Requested entity was not found', 'text' => '⚠ GoogleSheet nicht gefunden');
$languageger['googleSheetException429']           =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code 429 Quota exeeded', 'text' => "⚠ Zu viele Operationen in 100 Sekunden\nWarte kurz und probier es nochmal");
$languageger['googleSheetException503']           =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'Sheet Exception Code 429  The service is currently unavailable. ', 'text' => "⚠ Google-Sheet-Service ist zur Zeit nicht erreichbar");

$languageger['checkWait']                         =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => 'Wartemeldung', 'text' => "Bitte warte!");
$languageger['checkFevError']                     =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => 'Fehler beim Abruf von Fevgames', 'text' => "Fevgames-Fehler!");
$languageger['checkFevTitle']                     =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => 'Title bei Fevgames', 'text' => "Titel:");
$languageger['checkFevTime']                      =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => 'Startzeit bei Fevgames', 'text' => "Startzeit:");
$languageger['checkFSTime']                       =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => 'Startzeit des FS', 'text' => "Startzeit:");
$languageger['checkTimesDiff']                    =array('date' => '2021-08-10', 'dest' => 'mod',  'desc' => '⚠ Differenz bei Zeiten', 'text' => "⚠ Differenz bei Zeiten");
$languageger['checkNoSheet']                      =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Kein Sheet eingestellt', 'text' => 'Kein Sheet eingestellt');
$languageger['checkNoPasscodeSheet']              =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Sheet PASSCODE nicht gefunden', 'text' => 'Konnte das Sheet PASSCODE nicht finden');
$languageger['checkNoColumns']                    =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Noch keine Anzahl Zeichen eingestellt', 'text' => 'Noch keine Anzahl Zeichen eingestellt');
$languageger['checkIsFilled']                     =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Es ist schon was eingetragen', 'text' => 'Es ist schon was eingetragen');
$languageger['checkOutAnz']                       =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Eingestellte Zeichen', 'text' => 'Zeichen: [#1#]');
$languageger['checkResult']                       =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'check - Ergebnis des Checks', 'text' => 'Erfolgreich: [#1#], Fehlerhaft: [#2#]');

$languageger['missingNoConfigured']               =array('date' => '2021-02-11', 'dest' => 'mod',  'desc' => 'missing - Keine Einträge gefunden', 'text' => "Keine Einträge gefunden\n Nutze /readsheet");

$languageger['PrivInfo']=array('date' => '2020-12-24', 'desc' => 'Priv: Chatinfo, wenn kein gültiger Befehl', 'text' => "Aktiver Chat: [#1#]\nOffene Portale: [#2#]");

$languageger['YouAreNoMod']=array('date' => '2020-12-18', 'desc' => 'Priv: Keine Rechte', 'text' => "Du bist kein MOD");
$languageger['NoChatSelected']=array('date' => '2020-12-28', 'desc' => 'Priv: Kein Chat ausgewählt', 'text' => "Benutze /config um einen Chat auszuwählen");

$languageger['InputNoUndestand']=array('date' => '2020-12-18', 'desc' => 'Priv: Befehl nicht gefunden', 'text' => "Sorry, habe die Eingabe / den Befehl nicht verstanden");

// codeversion
$languageger['versionGitCode']                    =array('date' => '2021-04-23', 'dest' => 'mod',  'desc' => 'Ausgabe der Programmversion', 'text' => "Commit: <code>[#1#]</code> vom <code>[#2#]</code>");

$languageger['private_helptext']=array('date' => '2021-03-30', 'desc' => 'Hilfstext für den Private Chat', 
	'text' => "/config - Einstellungen für Dein Profil und den Bot
/next - Liefert Dir das nächste zu prüfende Portal.
/newsheet - Liefert Links zu den Sheet-Templates zum Erstellen neuer Sheets
/names - Zeigt die Sheetnamen an
/renamesheets - Benennt die Originalnamen (COL_#) in die eingestellten Namen um
/readsheet - Liest den aktuellen Status des Sheets in die DB
  Vor dem Start und nach manuellen Änderungen aufrufen
/check - Prüft, ob der Bot in das Sheet, jede Seite schreiben kann
/missing - Listet alle fehlenden Portale auf
/bookmarks - Bookmark der Portale für IITC
/draw - Draw für IITC
/botadmins - listet die Admins des Bots auf
"
);

$languageger['helpadmin']=array('date' => '2020-12-22', 'desc' => 'Der Hilfetext für Admins', 'text' => '<b>Admin-Befehle</b>
/chats - Verwaltung der Chats des Bot (Bot_Admins)
/setlanguageowner [name | TgId] [lang] [1 | 0] - Setzt einen Sprachverantwortlichen

');

$languageger['helplang']=array('date' => '2020-12-22', 'desc' => 'Der Hilfetext für Sprachen', 'text' => '<b>Sprachbefehle</b>

/exportlanguage - Exportiert die Sprachen je nach Einstellung als CSV oder GoogleSheet
/importlanguage - Importiert je nach Einstellung die Sprachdaten aus dem eingestellten Google-Sheet.
	Ist die Einstellung CSV, dann sende einfach die CSV an den Bot
	Achte dabei auf die Trennung durch Semikolon und den Zeichensatz UTF8

');

$languageger['cbm_monate']=array('date' => '2021-01-02', 'desc' => 'callbackmenuclass: Monate', 'text' => "Jan\nFeb\nMär\nApr\nMai\nJun\nJul\nAug\nSep\nOkt\nNov\nDez");
$languageger['cbm_jetzt']=array('date' => '2021-01-02', 'desc' => 'callbackmenuclass: Jetzt', 'text' => "Jetzt:");
$languageger['cbm_waehle']=array('date' => '2021-01-02', 'desc' => 'callbackmenuclass: Wähle', 'text' => "Wähle:");


?>