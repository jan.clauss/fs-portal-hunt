<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	if($ph->portalhuntsheet == '') {
		$tg->sendmessage($tg->user_id, $tl->getstr('readsheetNoSheet'));
		exit;
	}
	$result=$tg->sendmessage($tg->user_id, $tl->getstr('readsheetWait'));
	
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);
	
	$sheetdata=$phsheet->readsheet();
	if($puimage->ok == false) $tg->sendmessage($tg->user_id,'No transfer to imagewebsite');
	if($sheetdata['ok'] == true) {
		$tg->editMessageText($tg->user_id, $result, print_r($sheetdata['data'], true));
		$puimage->sendfoundportals(array('ok' => true, 'portals' => $sheetdata['portals']), $ph->fstitle, $ph->fsdate, $ph->fslocation, $ph->fevgamesid, $ph->starttime);
		$tg->editMessageText($tg->user_id, $result, print_r($sheetdata['data']."\n".$tl->getstr('readsheetTransfer'), true));
	}else{
		$tg->sendmessage($tg->user_id, $tl->getstr('readsheetError'.$sheetdata['code'],[],'readsheetErrorDefault'));
		$puimage->sendfoundportals($sheetdata, $ph->fstitle, $ph->fsdate, $ph->fslocation, $ph->fevgamesid, $ph->starttime);
	}
	
	exit;
?>