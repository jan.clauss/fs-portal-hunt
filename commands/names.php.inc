<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	$answer='';
	foreach($sheetsites AS $col => $name) {
		$answer.="$col => $name\n";
	}
	$tg->sendmessage($tg->user_id, "<code>$answer</code>");
	
	exit;
?>