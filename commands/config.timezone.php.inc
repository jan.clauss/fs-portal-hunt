<?php
//Ü
	$timezone_identifiers = DateTimeZone::listIdentifiers();
	foreach($timezone_identifiers AS $tz) {
		$tz=explode('/',$tz);
		if(IsSet($tz[1])) {
			if(IsSet($tz[2])) {
				$tz[1]=$tz[1].'/'.$tz[2];
			}
			$timezones[$tz[0]][]=$tz[1];
		}
	}
//	$tg->sendmessage($tg->user_id,print_r($tgres['args'],true));
	if(!IsSet($tgres['args'][0])) { // Nichts übergeben, also als erstes den Kontinent wählen
		$tzarray[]=array(array('text' => 'UTC', 'callback_data' => "conftimezone land UTC UTC")); // Sonderfall UTC
		foreach($timezones AS $continent => $areaarr) {
			$tzarray[]=array(array('text' => $continent, 'callback_data' => "conftimezone continent $continent"));
		}
		$text=$tl->getstr('TZselectContinent');
	}elseif($tgres['args'][0] == 'continent') { // Kontinent übergeben, Land wählen
		$continent=$tgres['args'][1];
		$tzarray[]=array(array('text' => '<<<', 'callback_data' => "conftimezone"));
		foreach($timezones[$continent] AS $landid => $land) {
			$tzarray[]=array(array('text' => $land, 'callback_data' => "conftimezone land $continent $land"));
		}
		$text=$tl->getstr('TZselectLand');
	}elseif($tgres['args'][0] == 'land') { // Kontinent und Land übergeben
		if($tgres['args'][1] == 'UTC') { // Sonderfall UTC
			$timezone='UTC';
		}else{
			$timezone=$tgres['args'][1].'/'.$tgres['args'][2];
		}
		if(IsSet($iscallbackquery)) {
			$tg->deleteMessage($tg->user_id,$tgres['message_id']);
			Unset($iscallbackquery);
		}
		$tg->deletemessage($tg->user_id, $tgres['message_id']);
		$answer=$tl->getstr('TZneueZeitzone',array($timezone));
		$ret=$tg->settimezone($tg->user_id, $timezone);
		if($ret['ok'] <> true) {
			$answer=mb_convert_encoding('&#x2757;', 'UTF-8', 'HTML-ENTITIES').' '.$tl->getstr('TZerrorNeueZone');
		}else{
			$now=time();
			$your=date($tl->getstr('datetime_datetime'),$now);
			date_default_timezone_set("Europe/Berlin");
			$server=date($tl->getstr('datetime_datetime'),$now);
			date_default_timezone_set('UTC');
			$utc=date($tl->getstr('datetime_datetime'),$now);
			date_default_timezone_set($timezone);
			$answer.=$tl->getstr('TZzoneInfo',array($your, $server, $utc));
		}
	}else{
		$answer="Fehler: $args";
	}
	if(IsSet($tzarray)) {
		$tzarray=array('inline_keyboard'=>$tzarray);
		$answer=array(
			'text'=>$text,
			'reply_markup'=>json_encode($tzarray)
		);
		if($tgres['message_typ']=='callback_query') {
			$answer['message_id']=$tgres['message_id'];
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
?>