<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);
	
	
	$bookmarks=$phsheet->getall();
	
	$bmkarr['maps']['idOthers']['label']='Others';
	$bmkarr['maps']['idOthers']['state']=1;
	$bmkarr['maps']['idOthers']['bkmrk']=[];
	$bmkarr['portals']['idOthers']['label']='Others';
	$bmkarr['portals']['idOthers']['state']=1;
	$bmkarr['portals']['idOthers']['bkmrk']=[];
	
	foreach($bookmarks AS $bmk) {
		$bmkarr['portals']['id'.$bmk['col']]['label']='col_'.$bmk['col'];
		$bmkarr['portals']['id'.$bmk['col']]['state']=1;
		$bmkarr['portals']['id'.$bmk['col']]['bkmrk']['id_'.$bmk['col'].$bmk['row']]['latlng']=$bmk['lat'].','.$bmk['lng'];
		$bmkarr['portals']['id'.$bmk['col']]['bkmrk']['id_'.$bmk['col'].$bmk['row']]['label']=$bmk['col'].$bmk['row'];
	}
	
	$json=json_encode($bmkarr,JSON_OBJECT_AS_ARRAY);
	$json=str_replace('[]','{}',$json);
//	$tg->sendmessage($tg->user_id, $json);
	$tg->sendMsgBigDoc($tg->user_id, $json, 2048, 'bookmark.json');
	exit;
?>