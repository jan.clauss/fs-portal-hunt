<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);

	$answer='';
	$i=1;
	while(($have=$phsheet->getcol($i)) <> false) {
		$i++;
		if(IsSet($have['portals']) and $have['portals']>0) {
//			$answer.=$have['col'].': ';
			$anz=0;
			for($j=1;$j<=$have['portals'];$j++) {
				if(!in_array($have['col'].$j,$have['exist'])) {
					$answer.=$have['col'].$j.' ';
					$anz++;
				}else{
					$answer.='✅ ';
				}
			}
//			if($anz==0) $answer.=$have['col'].' ✅';
			$answer.="\n";
		}
	}
	if($answer == '') $answer=$tl->getstr('missingNoConfigured');
	$tg->sendmessage($tg->user_id,$answer);
	$tg->sendmessage($tg->botconf['activchat_id'], $answer);
	exit;
?>