<?php
//Ü
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
		exit; // Nicht die nötigen Rechte
	}

	if(!IsSet($tgres['args'][0])) {
		$langlist=$tl->listlanguages(true);
//		$tg->sendmessage($tg->user_id, json_encode($langlist));
		foreach($langlist AS $lang) {
			$percent='';
			if($lang['percent'] >= 0) $percent=' ('.$lang['percent'].'%)';
			$configarray[]=array(array('text' => $lang['name']." - ".$lang['desc'].$percent, 'callback_data' => "confchatlanguage ".$lang['name']));
		}
		$configarray=array('inline_keyboard'=>$configarray);

		$answer=array(
				'text'=>"Wähle eine Sprache\nChoose a language\nВыбрать язык",
				'reply_markup'=>json_encode($configarray)
			);
		if($tgres['message_typ']=='callback_query') {
			$answer['message_id']=$tgres['message_id'];
		}

	}elseif(!preg_match('/^([a-z]{2,20})$/',trim($tgres['args'][0]),$arr)) {
		$answer='Fehlerhafter Aufruf :-(';
	}else{
		$lang=$arr[1];
		$answer=$tl->getstr('conflangKannNichtLaden');
		if($lang=='ger' or $tl->iflanguage($lang)) { // Existiert die Sprache
			if($tl->setlanguage($lang)) {
				$tg->setchatlanguage($tg->botconf['activchat_id'],$lang);
				$answer=$tl->getstr('conflangGewechselt');
				$tg->deleteMessage($tg->user_id,$tgres['message_id']);			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>