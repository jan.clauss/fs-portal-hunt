<?php
	if(!IsSet($tg->botconf['activchat_id'])) { // Kein Chat
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) { // Kein Mod
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
		exit;
	}
	if($ph->portalhuntsheet == '') { // Kein Sheet angegeben
		$tg->sendmessage($tg->user_id, $tl->getstr('renamesheetNoSheet'));
		exit;
	}
	if($ph->sheetnames == 'original') { // Auf Originalnamen eingestellt
		$tg->sendmessage($tg->user_id, $tl->getstr('renamesheetOriginalName'));
		exit;		
	}
	$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
	$sheetlist=$sheets->getSheetsTitles($ph->portalhuntsheet);
	$answer=$tl->getstr('renamesheetsUmbenennen')."\n";
	$result=$tg->sendmessage($tg->user_id, $answer);
	foreach($sheetlist AS $sheetident) {
		if(preg_match('/^col_(\d+)$/i',$sheetident['title'],$arr)) {
			$letter=$ph->dezColumn($arr[1]);
			switch($ph->sheetnames) {
				case 'number':
					$name=$arr[1];
				break;
				case 'letter':
					$name=$letter;
				break;
				case 'letterandnumber':
					$name=$letter.'_'.$arr[1];
				break;
				default:
					error_log('False SheetNamesTyp');
					exit;
				break;
			}
			$answer.=$sheetident['title']." - $name";
//		if($arr[1] == 3) $sheetident['id']++; // Debug eingebauter Fehler
			if($sheets->renameSheet($ph->portalhuntsheet,$sheetident['id'],$name,true)) $answer.='✅'; else $answer.='❌';
			$answer.="\n";
			$tg->editMessageText($tg->user_id, $result, $answer);
		}
	}
	$tg->editMessageText($tg->user_id, $result, $answer.'✅✅');
	exit;
?>