<?php
//Ü

//	$tg->sendmessage($tg->user_id,json_encode($tgres));
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		switch($tgres['args'][0]) {
			case 'none':
				exit;
			break;
			case 'exit':
				$tg->deletemessage($tg->user_id, $tgres['message_id']);
				unset($tgres['message_id']);
				exit;
			break;
			case 'selectchat':
				$chats=$chatadm->getchats($tg->user_id);
				if(count($chats['values']) == 0) {
					$answer=$tl->getstr('confselchatNoChat');
				}else{
					if(IsSet($tgres['args'][1])) {
						// Ein Chat übergeben
						$chat_id=0;
						foreach($chats['values'] AS $chat) {
							if(abs($chat['chat_id']) == $tgres['args'][1]) {
								$chat_id=$chat['chat_id'];
								$chat_title=$chat['chat_title'];
							}
						}
						if($chat_id == 0) { // Gewählter Chat nicht gefunden
							$tg->sendmessage($tg->user_id,$tl->getstr('confselchatNotFound'));
							unset($tgres['args'][0]);
						}else{
							$tg->botconf['activchat_id']=$chat_id;
							$tg->botconf['activchat_title']=$chat_title;
							$tg->writebotconf();
							unset($tgres['args'][0]);
							$tg->sendmessage($tg->user_id,$tl->getstr('confselchatGewechselt'));
						}
					}else{
						foreach($chats['values'] AS $chat) {
							$configarray[]=array(array('text' => $chat['chat_title'], 'callback_data' => 'config selectchat '.abs($chat['chat_id'])));
						}
					}
				}
			break;
			case 'delusermsg':
				$ph->deleteoriginalmessage=$ph->deleteoriginalmessage == false;
				$ph->savechatdata($tg->botconf['activchat_id']);
				unset($tgres['args'][0]);
			break;
			default:
				$answer='Upps';
			break;
		}
	}
	if(!IsSet($tgres['args'][0])) {
		// Hauptmenü
		// Zeitzohne
		$configarray[]=array(array('text' => $tl->getstr('conftimezone'), 'callback_data' => 'conftimezone'));
		// Sprache
		$configarray[]=array(array('text' => $tl->getstr('conflanguage'), 'callback_data' => 'conflanguage'));
		// Chatwahl
		$configarray[]=array(array('text' => $tl->getstr('confselchat'), 'callback_data' => 'config selectchat'));
		if(IsSet($tg->botconf['activchat_id']) and $tg->botconf['activchat_id'] <> 0 and $chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
			$configarray[]=array(array('text' => $chatadm->getchat($tg->botconf['activchat_id'])['first_name'].' ...', 'callback_data' => 'confchat'));
		}
		// Zurück
		$configarray[]=array(array('text' => $tl->getstr('verlassen'), 'callback_data' => 'config exit'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			$chat_title='--';
			if(IsSet($tg->botconf['activchat_id']) and $chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id) and $tchat_title=$chatadm->getchat($tg->botconf['activchat_id'])['first_name']) $chat_title=$tchat_title;
			$answer=$tl->getstr('confKopf',array(
				$chat_title,
				$tg->timezone,
				$tl->getstr('language_name')
			));
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;


?>