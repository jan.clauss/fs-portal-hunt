<?php
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
		exit; // Nicht die nötigen Rechte
	}

	$cbm=new callbackmenu($tg, $tl);

	$starttime=0;
	if($ph->starttime > 0) $starttime=$ph->starttime;
	
//	$tg->sendmessage($tg->user_id,json_encode($tgres));
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		switch($tgres['args'][0]) {
			case 'none':
				exit;
			break;
			case 'delusermsg':
				$ph->deleteoriginalmessage=$ph->deleteoriginalmessage == false;
				$ph->savechatdata($tg->botconf['activchat_id']);
				unset($tgres['args'][0]);
			break;
			case 'time':
				if(($zeit=$cbm->datumzeit('confchat time', $tgres, $starttime)) == false) exit;
				$starttime = $zeit;
				$ph->starttime = $starttime;
				unset($tgres['args'][0]);
				$ph->savechatdata($tg->botconf['activchat_id']);
			break;
			case 'nextoldtime':
				if(($min=$cbm->AuswahlListe('confchat nextoldtime', $tgres, $ph->nextoldtime/60, [[2,5],[10,15],[30,60]])) == false) exit;
				$ph->nextoldtime=$min*60;
				unset($tgres['args'][0]);
				$ph->savechatdata($tg->botconf['activchat_id']);
			break;
			case 'queuedel':
				if($tgres['message_typ'] == 'callback_query') {
					$answer=$tl->getstr('confchatQueueDelQuest');
					$tg->setcommand($tg->user_id, 'confchat queuedel');
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					unset($tgres['message_id']);
				}else{
					if($tgres['text'] == $tl->getstr('safetyquery_ja')) {
						if($ph->deletequeue($tg->botconf['activchat_id'])) {
							$answer=$tl->getstr('confchatQueueDelQuestDeleted');
						}else{
							$answer=$tl->getstr('confchatQueueDelQuestFail');
						}
					}else{
						$answer=$tl->getstr('confchatQueueDelQuestCanceled');
					}
					$tg->setcommand($tg->user_id, '');
					$tg->sendmessage($tg->user_id, $answer);
					unset($answer);
					unset($tgres['args'][0]);
				}
			break;
			case 'autoifempty':
				$ph->autoinsertifempty=$ph->autoinsertifempty == false;
				$ph->savechatdata($tg->botconf['activchat_id']);
				unset($tgres['args'][0]);
			break;
			default:
				$answer='Upps';
			break;
		}
	}
	if(!IsSet($tgres['args'][0])) {
		// Hauptmenü
		// Sprache für chat
		$configarray[]=array(array('text' => $tl->getstr('conflanguage'), 'callback_data' => 'confchatlanguage'));
		// Formulardaten
		$configarray[]=array(array('text' => $tl->getstr('confsheet'), 'callback_data' => 'confsheet'));
		// FS-Daten
		$configarray[]=array(array('text' => $tl->getstr('confFS'), 'callback_data' => 'confFS'));
		// Usernachrichten löschen
		$delusersmile='❌';
		if($ph->deleteoriginalmessage) $delusersmile='✔';
		$configarray[]=array(array('text' => "$delusersmile ".$tl->getstr('confchatDelUserMsg'), 'callback_data' => 'confchat delusermsg'));
		// Startzeitpunkt festlegen
		$vstarttime='--';
		if($starttime > 0) $vstarttime=date($tl->getstr('datetime_datetimeWOSecound'),$starttime);
		$starttimeaktiv='🟢'; // grün
		if($starttime>time()) $starttimeaktiv='🔴'; // rot
		$configarray[]=array(array('text' => $starttimeaktiv.' '.$tl->getstr('confchatStartTime').' '.$vstarttime, 'callback_data' => 'confchat time'));
		// Direkt eintragen, wenn frei
		if($ph->autoinsertifempty) $autoifempty='✅'; else $autoifempty='❌';
		$configarray[]=array(array('text' => "$autoifempty ".$tl->getstr('confchatAutoInsertIfEmpty'), 'callback_data' => 'confchat autoifempty'));
		// Zeitraum, ab dem bei jeder Portalmeldung eine /next-Meldung kommt
		$configarray[]=array(array('text' => $tl->getstr('confchatNextOldTime',[$ph->nextoldtime/60]), 'callback_data' => 'confchat nextoldtime'));
		// Warnung, wenn Portale von vor Startzeit
		if($ph->testOlderQueue($ph->chat_id,$starttime)) {
			$configarray[]=array(array('text' => $tl->getstr('confchatQueueWarn'), 'callback_data' => 'confchat none'));
		}
		// Queue löschen
		$configarray[]=array(array('text' => $tl->getstr('confchatQueueDel'), 'callback_data' => 'confchat queuedel'));
		// Zurück
		$configarray[]=array(array('text' => $tl->getstr('zurueck'), 'callback_data' => 'config'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			$answer=$tl->getstr('confchatKopf',[$chatadm->getchat($ph->chat_id)['first_name']]);
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);

	exit;

?>