<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	
	if($ph->portalhuntsheet == '') {
		$tg->sendmessage($tg->user_id, $tl->getstr('checkNoSheet'));
		exit;
	}
	
	$waitstr=$tl->getstr('checkWait');
	// Fevgames
	$answer="<b>Fevgames</b>\n";
	$answer.='ID: '.$ph->fevgamesid."\n";
	$result=$tg->sendmessage($tg->user_id, $answer."\n\n$waitstr");
	$message_id=$result['result']['message_id'];
	$fev=new fevgames($ph->fevgamesid);
	$FSData=$fev->getfsdata();
	if($FSData == false) { // Fehler beim Abrufen der Fevgames-Daten
		$answer.=$tl->getstr('checkFevError');
		$tg->editMessageText($tg->user_id, $message_id, $answer);
	}else{
		$answer.=$tl->getstr('checkFevTitle')." ".$tg->escapestr($fev->fsdata['title'])."\n";
		$fsdate=strtotime($fev->fsdata['date'].' '.$fev->fsdata['time']);
		$fsdateerr='';
		if($fsdate < time()) $fsdateerr='⚠ ';
		$answer.=$tl->getstr('checkFevTime')." $fsdateerr".$fev->fsdata['date'].' '.$fev->fsdata['time']."\n";
		$tg->editMessageText($tg->user_id, $message_id, $answer);
	}
	$answer.="\n<b>FS:</b>";
	// Startzeit
	$fsdateerr='';
	if($ph->starttime < time()) $fsdateerr='⚠ '; // Startzeitpunkt schon vorbei
	$answer.="\n".$tl->getstr('confchatStartTime')." $fsdateerr".date('Y-m-d H:i:s',$ph->starttime)."\n";
	if(IsSet($fsdate) and $fsdate <> $ph->starttime) $answer.=$tl->getstr('checkTimesDiff')."\n";
	$tg->editMessageText($tg->user_id, $message_id, $answer);	
	
	$answer.="\n<b>Sheet:</b>";
	$tg->editMessageText($tg->user_id, $message_id, $answer."\n\n$waitstr");
	$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
		
	// *** Passcode Sheet lesen ***
	// Daten auslesen
	$range=SHEETNAME_PASSCODE.'!'.SHEETRANGE_PASSCODE;
	$rows=$sheets->getRange($ph->portalhuntsheet,$range);
	if($rows === false) {
		$answer.=$tl->getstr('googleSheetException'.$sheets->lasterr->error->code,array(),$tl->getstr('googleSheetExceptionDefault'))."\n";
		$answer.=$tl->getstr('checkNoPasscodeSheet')."\n";
		$tg->editMessageText($tg->user_id, $message_id, $answer);
		exit;
	}
	if(!IsSet($rows['values']) or $rows['values'] == null) {
		$answer.=$tl->getstr('checkNoColumns');
		$tg->editMessageText($tg->user_id, $message_id, $answer);
		exit;
	}
	$filled=false; // Ist schon was eingetragen
	foreach($rows['values'] AS $value) {
		$pcs[$value[0]]=['portals' =>intval($value[3]), 'character' => $value[5]];
		if($value[4] > 0) $filled=true;
	}
	if($filled) { // Schon Portale eingetragen
		$answer.=$tl->getstr('checkIsFilled');
		$tg->editMessageText($tg->user_id, $message_id, $answer);
		exit;
	}
	$answer.=$tl->getstr('checkOutAnz', [count($pcs)]);
	$tg->editMessageText($tg->user_id, $message_id, $answer);
	$col='A';
	$ok=0;
	$fail=0;
	foreach($pcs AS $nr => $data) {
		$answer.="\n".$sheetsites[$col].': ';
		// Daten schreiben
		$range=$sheets->decoderange($ph->portaldatarange);
		$writerange=$sheets->dezColumn($sheets->columnDez($range['column'][0])+1).($range['row'][0]).':'.$range['column'][1].($range['row'][0]);
		$range=$sheetsites[$col].'!'.$writerange;
		// Testwerte schreiben
		$values=[['Name_'.$col, 'Agent', 'pll=12.3,45.6']];
		$res=$sheets->update($ph->portalhuntsheet,$range,$values);
		if(is_object($res) and IsSet($res->updatedCells) and $res->updatedCells == 3) { // Erfolgreich
			$answer.='✅';
			$ok++;
			// wieder löschen
			$values=[['', '', '']];
			$res=$sheets->update($ph->portalhuntsheet,$range,$values);
			// Warnung bei 0 Portalen
			if(intval($data['portals']) == 0) $answer.=' ❗ no portalcount set';
		}else{
			$fail++;
			if($res === false) {
				$answer.=$tl->getstr('googleSheetException'.$sheets->lasterr->error->code,array(),$tl->getstr('googleSheetExceptionDefault'));
			}else{
				$answer.='❌';
			}
		}
		$tg->editMessageText($tg->user_id, $message_id, $answer);
		$col++;
	}
	$answer.="\n\n".$tl->getstr('checkResult',[$ok,$fail]);
	$tg->editMessageText($tg->user_id, $message_id, $answer);
	
	exit;
?>