<?php
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
		exit; // Nicht die nötigen Rechte
	}
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		switch($tgres['args'][0]) {
			case 'none':
				exit;
			break;
			case 'FevId':
				if($tgres['message_typ'] == 'callback_query') {
					$answer=$tl->getstr('confFSIdGet');
					$tg->setcommand($tg->user_id, 'confFS FevId');
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					unset($tgres['message_id']);
				}else{
					if(trim($tgres['text']) == '-') {
						$ph->Fevgamesid=0;
						$ph->savechatdata($tg->botconf['activchat_id']);
						unset($tgres['args'][0]);
						$tg->setcommand($tg->user_id,'');
					}else{
						$Fev=new fevgames($tgres['text']);
						$FSData=$Fev->getfsdata();
						if($FSData == false) { // Fehler beim Abrufen der Fevgames-Daten
							$answer=$tl->getstr('confFSFevError');
							unset($tgres['args'][0]);
							$tg->setcommand($tg->user_id,'');
						}else{
							// Gültiger Fevgames-Eintrag
							$ph->fevgamesid=intval($tgres['text']);
//							error_log(json_encode($Fev->fsdata));
							$ph->fstitle=$Fev->fsdata['title'];
							$ph->fsdate=$Fev->fsdata['date'];
							$ph->fslocation=$Fev->fsdata['title'];
							$ph->savechatdata($tg->botconf['activchat_id']);
							unset($tgres['args'][0]);
							$tg->setcommand($tg->user_id,'');
							$tg->sendmessage($tg->user_id, $tl->getstr('confFSFevSave',[$ph->fstitle, $ph->fsdate, $ph->fslocation, $ph->fevgamesid]));
						}
					}
				}
			break;
			default:
				$answer='Upps';
			break;
		}
	}
	if(!IsSet($tgres['args'][0])) {
		// Hauptmenü
		// Fevgames-Abruf
		$configarray[]=array(array('text' => $tl->getstr('confFevId'), 'callback_data' => 'confFS FevId'));
		// Titel
		// Datum
		// Ort
		// Readsheet ausführen
		$configarray[]=array(array('text' => $tl->getstr('confFevReadsheet'), 'callback_data' => 'readsheet'));
		// ImageUpload-Link
		$configarray[]=array(array('text' => $tl->getstr('confFevImgUpload'), 'url' => $puimage->genconfigsitelink($ph->fevgamesid)));
		// Seiten-Link
		$configarray[]=array(array('text' => $tl->getstr('confFevImgPortalSite'), 'url' => $puimage->WebportalLink($ph->fevgamesid)));
		// Zurück
		$configarray[]=array(array('text' => $tl->getstr('zurueck'), 'callback_data' => 'confchat'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			$answer=$tl->getstr('confFSKopf',[$tg->escapestr($ph->fstitle), $tg->escapestr($ph->fsdate), $tg->escapestr($ph->fslocation), $ph->fevgamesid]);
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);

	exit;

?>