<?php
	//Ü
	
// Übergeben der Testfunktion an die Classe
$tl->testfunction='tgstringtest';

if(!IsSet($tg->botconf['languageowner']) or count($tg->botconf['languageowner']) == 0) {
	$answer=$tl->getstring('nolanguageowner');
	$tg->sendmessage($tg->user_id,$answer);
	exit;
}
foreach($tg->botconf['languageowner'] AS $langrights => $lstat) {
	if($lstat == 1) $langrightsarray[]=$langrights;
}

switch($tgres['message_typ']) {
	case 'document': // Übersenden einer Datei
		$filename=$bot_root."lang/".$tg->user_id."_language.csv";
		$res=$tg->copyDocument($tgres['fileid'],$filename);
		// error_log(print_r($res,true));
		if($res['ok'] == false) {
			$answer="Transfer error";
			exit;
		}
		$res=$tl->importfromfile($filename,$langrightsarray);
	break;
	case 'query': // /importlanguage aufgerufen
		if(IsSet($tg->botconf['languageexport']['googlesheet']) and $tg->botconf['languageexport']['googlesheet']<>'') {
			$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
			$range='A1:Z';
			if(!$result=$sheets->getRangeArr($tg->botconf['languageexport']['googlesheet'],$range)) {
				$tg->sendmessage($tg->user_id, 'Fehler beim lesen des Sheets');
				exit;
			}
			$res=$tl->importfromarray($result, $langrightsarray);
		}
	break;
}
if($res['ok'] <> true) {
	if(!IsSet($res['errorInfo'])) {
		$tg->sendmessage($tg->user_id, 'Import Failed');
		exit;
	}
	$tg->sendmessage($tg->user_id,$res['errorInfo']);
	exit;
}
if($res['description'] == '') $res['description']='Nothing Changed';
$tg->sendmessage($tg->user_id,$res['description']);
if(IsSet($res['errors'])) {
	$tg->sendmessage($tg->user_id, $res['errors']);
	foreach($res['errorlist'] AS $lang => $list) {
		foreach($list AS $errors) {
			$answer=$errors['token']."\n".$errors['desc'];
			$tg->sendmessage($tg->user_id, $answer);
		}
	}
}
exit;

function tgstringtest($text) {
	global
		$tg;
	return($tg->testsendstr($tg->user_id, $text));
}
?>