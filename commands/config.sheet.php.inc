<?php
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('rightsDuKeinMod'));
		exit; // Nicht die nötigen Rechte
	}
//	$tg->sendmessage($tg->user_id,json_encode($tgres));
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		switch($tgres['args'][0]) {
			case 'none':
				exit;
			break;
			case 'link':
				if($tgres['message_typ'] == 'callback_query') {
					$answer=$tl->getstr('confSheetLinkGet');
					$tg->setcommand($tg->user_id, 'confsheet link');
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					unset($tgres['message_id']);
				}else{
					if(trim($tgres['text']) == '-') {
						$ph->portalhuntsheet='';
						$ph->savechatdata($tg->botconf['activchat_id']);
						unset($tgres['args'][0]);
						$tg->setcommand($tg->user_id,'');
					}elseif(preg_match('#/spreadsheets/d/([-a-zA-Z0-9_]+)#',$tgres['text'],$arr)) {
						// Gültiger Sheet Link
						$ph->portalhuntsheet=$arr[1];
						$ph->savechatdata($tg->botconf['activchat_id']);
						unset($tgres['args'][0]);
						$tg->setcommand($tg->user_id,'');
					}else{
						$answer=$tl->getstr('confSheetLinkGetRepeat');
					}
				}
			break;
			case 'names':
				switch($ph->sheetnames) {
					case 'letterandnumber':
						$ph->sheetnames='letter';
					break;
					case 'letter':
						$ph->sheetnames='number';
					break;
					case 'number':
						$ph->sheetnames='original';
					break;
					default:
						$ph->sheetnames='letterandnumber';
					break;
				}
				$ph->savechatdata($tg->botconf['activchat_id']);
				$ph->loadchatdata($tg->botconf['activchat_id']);
				unset($tgres['args'][0]);
			break;
			case 'range':
				if($tgres['message_typ'] == 'callback_query') {
					$answer=$tl->getstr('confSheetRangeGet');
					$tg->setcommand($tg->user_id, 'confsheet range');
					$tg->deletemessage($tg->user_id, $tgres['message_id']);
					unset($tgres['message_id']);
				}else{
					$rangefail=true;
					if(preg_match('/([a-z]+)([0-9]+):([a-z]+)([0-9]+)/i',$tgres['text'],$arr)) {
						if($arr[1] < $arr[3] and $arr[2] < $arr[4]) {
							$rangefail=false;
							$ph->portaldatarange=$arr[1].$arr[2].':'.$arr[3].$arr[4];
							$ph->savechatdata($tg->botconf['activchat_id']);
							$tg->sendmessage($tg->user_id, $tl->getstr('confSheetRangeGetOK'));
							unset($tgres['args'][0]);
							$tg->setcommand($tg->user_id,'');
						}
					}
					if($rangefail) {
							$tg->sendmessage($tg->user_id, $tl->getstr('confSheetRangeGetFail'));
							unset($tgres['args'][0]);
							$tg->setcommand($tg->user_id,'');
					}
				}
			break;
		}
	}
	if(!IsSet($tgres['args'][0])) {
		// Hauptmenü
		// SPreadSheet-ID
		$configarray[]=array(array('text' => $tl->getstr('confSheetLink'), 'callback_data' => 'confsheet link'));
		
		// Sheet-Namen
		$configarray[]=array(array('text' => $tl->getstr('confSheetNames').': '.$sheetsites['A'].', '.$sheetsites['B'].'...', 'callback_data' => 'confsheet names'));
		
		// Rename-Sheet - Wie Befehl /renamesheet
		$configarray[]=array(array('text' => $tl->getstr('confSheetRename'), 'callback_data' => 'renamesheets'));
		
		// Range
		$configarray[]=array(array('text' => $tl->getstr('confSheetRange').': '.$ph->portaldatarange, 'callback_data' => 'confsheet range'));
		
		// Zurück
		$configarray[]=array(array('text' => $tl->getstr('zurueck'), 'callback_data' => 'confchat'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			if($ph->portalhuntsheet <> '') {
				// Sheet-Klasse
				$sheets=new googlesheets($sheets_authConfig, $sheets_tokenPath);
				if($title=$sheets->getTitle($ph->portalhuntsheet)) {
					$sheetlink=$sheets->getSpreadSheetShareLink($ph->portalhuntsheet);
					$sheetlink="<a href='$sheetlink'>$title</a>";
				}else{
					$sheetlink=$tl->getstr('googleSheetException'.$sheets->lasterr->error->code,array(),$tl->getstr('googleSheetExceptionDefault'))." <a href='https://docs.google.com/spreadsheets/d/".$ph->portalhuntsheet."'>Link</a>";
				}
			}else{
				$sheetlink="---";
			}
			$answer=$tl->getstr('confSheetKopf',array(
				$sheetlink,
				$sheets_usermail
				));
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);

	exit;
?>