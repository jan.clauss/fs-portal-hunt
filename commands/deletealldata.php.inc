<?php
	if($tgres['message_typ'] == 'query') { // Aufrauf mit /deletealldata
		$answer=$tl->getstr('deletealldataQuery');
		$tg->setcommand($tg->user_id, 'deletealldata');
		$tg->sendmessage($tg->user_id, $answer);
	}else{
		if(strtoupper(trim($tgres['text'])) == strtoupper($tl->getstr('safetyquery_ja'))) { // Aufruf über internes kommando deletealldata
			$tg->deleteuser($tg->user_id);
			$answer=$tl->getstr('deletealldataok');
			$tg->sendmessage($tg->user_id, $answer);
			if($fs->ifuplwindow()) {
				include $bot_root.'commands/sheet/refreshfssheet.php.inc'; // Fevgames Sheet aktualisieren
			}
		}else{
			$answer=$tl->getstr('abgebrochen');
			$tg->sendmessage($tg->user_id, $answer);
		}
		$tg->setcommand($tg->user_id,'');
	}
	exit;
?>