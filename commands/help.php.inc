<?php
	$answer=$tl->getstr('private_helptext');
	if(IsSet($bot_admins[$tg->user_id])) {
		$answer.="\n\n".$tl->getstr('helpadmin');
	}
	if(IsSet($tg->botconf['languageowner'])) {
		$languageowner=false;
		foreach($tg->botconf['languageowner'] AS $langon) {
			if($langon) $languageowner=true;
		}
		if($languageowner) $answer.="\n\n".$tl->getstr('helplang');
	}
	$tg->sendmessage($tg->user_id, $answer);
	exit;
?>