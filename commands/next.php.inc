<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
		exit;
	}
	if($ph->portalhuntsheet == '') {
		$tg->sendmessage($tg->user_id, $tl->getstr('nextNoSheet'));
		exit;
	}
	$chat_language=$tg->getchatlanguage($tg->botconf['activchat_id']);
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);
	if($tgres['message_typ'] == 'callback_query' and IsSet($tgres['args'][0]) and IsSet($tgres['args'][1])) {
		$id=intval($tgres['args'][1]);
		switch($tgres['args'][0]) {
			case 'none': // Keine Aktion
				exit;
			break;
			case 'autoinsert':
			case 'autoupdate':
				switch($tgres['args'][0]) {
					case 'autoinsert':
						$answertyp="\n✔ ".$tl->getstr('nextInsert');
						$chatanswertyp="\n✔ ".$tl->getlangstr($chat_language, 'nextInsert');
					break;
					case 'autoupdate':
						$answertyp="\n✔ ".$tl->getstr('nextUpdate');
						$chatanswertyp="\n✔ ".$tl->getlangstr($chat_language, 'nextUpdate');
					break;
				}
				$portal=$ph->getportalbyid($id);
				if($portal == false) {
					$tg->sendmessage($tg->user_id,$tl->getstr('nextAlreadyProcessed'));
				}else{
					$link="https://intel.ingress.com/?pll=".$portal['lat'].",".$portal['lng'];
					$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
					// Daten schreiben
					$range=$sheets->decoderange($ph->portaldatarange);
					$writerange=$sheets->dezColumn($sheets->columnDez($range['column'][0])+1).($range['row'][0]+$portal['row']-1).':'.$range['column'][1].($range['row'][0]+$portal['row']-1);
					$range=$sheetsites[$portal['col']].'!'.$writerange;
					$values=[[$portal['name'], $portal['agent'], $link]];
					$res=$sheets->update($ph->portalhuntsheet,$range,$values);
					if(is_object($res) and IsSet($res->updatedCells) and $res->updatedCells == 3) { // Erfolgreich
						$phsheet->writeportal($portal['col'], $portal['row'], $link, $portal['agent']);
						$puimage->sendportalstatus($portal['col'], $portal['row'],true, $ph->fevgamesid);
						$answer=nextportaloutputstr($portal, $ph->starttime);
						$answer.=$answertyp.' '. $tg->username;
						$answer=array(
							'text' => $answer,
							'message_id' => $tgres['message_id']
						);
						$ph->deleteportalbyid($id);
						$tg->sendmessage($tg->user_id, $answer); // An priv-Chat senden
						$answer=chatportaloutputstr($portal);
						$answer.=$chatanswertyp.' '. $tg->username;
						$answer=array(
							'text' => $answer,
							'message_id' => $portal['message_id']
						);
						$tg->sendmessage($tg->botconf['activchat_id'], $answer); // An Gruppe senden
					}else{
						$tg->sendmessage($tg->user_id, $tl->getstr('nextWriteError')."\n$range - ".print_r($values,true));
						exit;
					}
				}
			break;
			case 'ignore':
				$portal=$ph->getportalbyid($id);
				if($portal == false) {
					$tg->sendmessage($tg->user_id,$tl->getstr('nextAlreadyProcessed'));
				}else{
					$answer=nextportaloutputstr($portal, $ph->starttime);
					$answer.="\n❌ ".$tl->getstr('nextIgnore');
					$answer=array(
						'text' => $answer,
						'message_id' => $tgres['message_id']
					);
					$ph->deleteportalbyid($id);
					$tg->sendmessage($tg->user_id, $answer); // An priv-Chat senden
					$answer=chatportaloutputstr($portal);
					$answer.="\n❌ ".$tl->getlangstr($chat_language, 'nextIgnore').' '. $tg->username;
					$answer=array(
						'text' => $answer,
						'message_id' => $portal['message_id']
					);
					$tg->sendmessage($tg->botconf['activchat_id'], $answer); // An Gruppe senden
				}
			break;
		}
	}
	// Sucht das nächste zu prüfende Portal
	$portalanz=$ph->getanzportal($tg->botconf['activchat_id']);
	if($portalanz == 0) {
		$tg->sendmessage($tg->user_id, $tl->getstr('nextNoPortalsToSearch'));
		exit;
	}
	$nextportal=$ph->getnextportal($tg->botconf['activchat_id']);
//	$tg->sendmessage($tg->user_id, print_r($nextportal, true));
	$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
	// Prüfen, ob vorgesehener Schreibbereich
	$spaltennummer=$sheets->columnDez($nextportal['col']);
	if(!IsSet($ph->passcodesheetdata[$spaltennummer]) or $ph->passcodesheetdata[$spaltennummer]['portals'] < $nextportal['row']) {
		$answer.="\n⚠ ".$tl->getstr('PHSheetOutOfRange');
	}
	// Daten auslesen
	$range=$sheetsites[$nextportal['col']].'!'.$ph->portaldatarange;
	$rows=$sheets->getRange($ph->portalhuntsheet,$range);
	if(!IsSet($rows['values']) or $rows['values'] == null) {  // Keine Rückgabewerte, es sind keine Zeilen gesetzt
		$answer.="⚠ ".$tl->getstr('PHSheetNoRowsSet')."\n";
		$configarray[]=array(array('text' => $tl->getstr('nextAutoInsert'), 'callback_data' => 'next autoinsert '.$nextportal['id']));
	}else{
//			$answer.=$rows['values'][$nextportal['row']-1][3]."\n";
		if(!IsSet($rows['values'][$nextportal['row']-1][3])) {
			//Ist leer
			$answer=nextportaloutputstr($nextportal, $ph->starttime, $portalanz);
			$configarray[]=array(array('text' => $tl->getstr('nextAutoInsert'), 'callback_data' => 'next autoinsert '.$nextportal['id']));
		}else{
			$answer=nextportaloutputstr($nextportal, $ph->starttime, $portalanz,$rows['values'][$nextportal['row']-1][3]);
			$configarray[]=array(array('text' => "⚠️ ".$tl->getstr('nextReplace'), 'callback_data' => 'next autoupdate '.$nextportal['id']));
		}
//		$answer.=json_encode($rows);
	}
	$configarray[]=array(array('text' => $tl->getstr('nextIgnoreEntry'), 'callback_data' => 'next ignore '.$nextportal['id']));
	

	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
	}
	$tg->sendmessage($tg->user_id, $answer);
	
	exit;
	
	function nextportaloutputstr($nextportal, $starttime, $portalanz=-1, $oldportal='')
	{
		global
			$tl, $tg, $sheetsites;
		$answer='';
		$link="https://intel.ingress.com/?pll=".$nextportal['lat'].",".$nextportal['lng'];
		if($portalanz <> -1 ) $answer.=$tl->getstr('nextPortalInfoOffen',[$portalanz])."\n";
		if($nextportal['erstellt'] < $starttime) { // Portal ist warscheinlich aus anderem Hunt, zu alt
			$answer.=$tl->getstr('nextToOldPortal',[date($tl->getstr('datetime_datetimeWOSecound'),$nextportal['erstellt'])])."\n"; // Zeitpunkt des einreichens
		}
		if($oldportal <> '') {
			$answer.=$tl->getstr('nextPortalInfoReplace',
			[
				$nextportal['col'].'-'.$nextportal['row'],
				$sheetsites[$nextportal['col']],
				$link,
				$oldportal,
				$tg->escapestr($nextportal['name']),
				$tg->escapestr($nextportal['agent'])
			]);
		}else{
			$answer.=$tl->getstr('nextPortalInfo',
			[
				$nextportal['col'].'-'.$nextportal['row'],
				$sheetsites[$nextportal['col']],
				$link,
				$tg->escapestr($nextportal['name']),
				$tg->escapestr($nextportal['agent'])
			]);
		}
		return($answer);
	}

	function chatportaloutputstr($nextportal)
	{
		global
			$tl, $tg, $sheetsites;
		$link="https://intel.ingress.com/?pll=".$nextportal['lat'].",".$nextportal['lng'];
		$answer=$tg->escapestr($nextportal['agent'])."\n";
		$answer.=$nextportal['col'].'-'.$nextportal['row']."\n";
		$answer.="$link\n";
		$answer.=$tg->escapestr($nextportal['name']);
		return($answer);
	}
?>