<?php
//Ü
if(!IsSet($tg->botconf['languageowner']) or count($tg->botconf['languageowner']) == 0) {
	$answer=$tl->getstr('nolanguageowner');
	$tg->sendmessage($tg->user_id,$answer);
	exit;
}
foreach($tg->botconf['languageowner'] AS $langtoken => $langactive) {
	$languagearr[]=$langtoken;
}
// Ermitteln des Ausgabeformats
$outputtyp='csv';
if(IsSet($tg->botconf['languageexport']['output'])) $outputtyp=$tg->botconf['languageexport']['output'];
switch($outputtyp) {
	case 'sheet':
		$outputstr='Google-Sheet';
	break;
	case 'po':
		$outputstr='po-File';
	break;
	default:
		$outputstr='CSV-File';
	break;
}
switch($outputtyp) {
	case 'csv':
		$filename=$bot_root."lang/".$tg->user_id."_language.csv";
		$tl->exporttofile($filename,$languagearr);
		$tg->sendDocument($tg->user_id, $filename, 'Language Export');
	break;
	case 'sheet':
		$langarr=$tl->exporttoarray($languagearr);
		$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
		$range='A1:Z';
		$sheets->batchClear($tg->botconf['languageexport']['googlesheet'], $range);
		$params = [
   		'valueInputOption' => 'USER_ENTERED'
		];
		$result=$sheets->update($tg->botconf['languageexport']['googlesheet'], $range, $langarr);
		if($result == false) {
			$tg->sendmessage($tg->user_id, $tl->getstr('googleSheetException'.($sheets->lasterr->error->code),array(),$tl->getstr('googleSheetExceptionDefault')));
		}else{
			$tg->sendmessage($tg->user_id, "✔ ".$tl->getstr('googleSheetUpdatedRows',array($result['updatedRows'])));
			$sheettitle=$sheets->getTitle($tg->botconf['languageexport']['googlesheet']);
			$tg->sendmessage($tg->user_id,"<a href='https://docs.google.com/spreadsheets/d/".$tg->botconf['languageexport']['googlesheet']."'>".$sheettitle."</a>");
		}
	break;
}
exit;
?>