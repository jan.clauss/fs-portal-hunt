<?php
	if(!IsSet($tg->botconf['activchat_id'])) {
		$tg->sendmessage($tg->user_id, $tl->getstr('NoChatSelected'));
		exit;
	}
	if(!$chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
		$tg->sendmessage($tg->user_id, $tl->getstr('YouAreNoMod'));
	}
	$phsheet=new portalhuntsheet($pdo, $database_table_main, $ph, $tg->botconf['activchat_id']);

	$bookmarks=$phsheet->getall();
	
/*	$answer='';
	foreach ($bookmarks AS $portal) {
	 	 $answer.=$portal['col'].$portal['row']."\n";
	} 
	$tg->sendmessage($tg->user_id, $answer);*/

	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		if($tgres['args'][0] == 'all') { // Alle Draws
			
			$col='';
			foreach($bookmarks AS $bmk) {
				if($col <> $bmk['col']) {
					if(IsSet($linearr) and count($linearr) > 1) $drawarr[]=array("type" => "polyline", "latLngs" =>$linearr, "color" => "#a24ac3");
					$linearr=array();
					$col=$bmk['col'];
				}
				$linearr[]=array('lat' => $bmk['lat'], 'lng' => $bmk['lng']);
			}
			if(IsSet($linearr) and count($linearr) > 1) $drawarr[]=array("type" => "polyline", "latLngs" =>$linearr, "color" => "#a24ac3");
		}else{ // Einzelner Draw
			$linearr=array();
			$col=$tgres['args'][0];
			foreach($bookmarks AS $bmk) {
				if($col == $bmk['col']) {
					$linearr[]=array('lat' => $bmk['lat'], 'lng' => $bmk['lng']);
				}
			}
			if(count($linearr) > 1) $drawarr[]=array("type" => "polyline", "latLngs" =>$linearr, "color" => "#a24ac3");
		}
		if(!IsSet($drawarr)) {
			$tg->sendmessage($tg->user_id, $tl->getstr('drawNoDrawData'));
			$tg->deletemessage($tg->user_id, $tgres['message_id']);
			exit;
		}
		$drawjson=json_encode($drawarr);
		$tg->deletemessage($tg->user_id, $tgres['message_id']);
		$tg->sendMsgBigDoc($tg->user_id, $drawjson, 2048, 'draw.json');
		exit;
	}
	if(!IsSet($tgres['args'][0])) {
		// Hauptmenü
		// Alle Draws
		$configarray[]=array(array('text' => $tl->getstr('drawAll'), 'callback_data' => 'draw all'));
		$col=0;
		$maxcol=4;
		$oldcol='';
		foreach($bookmarks AS $bmk) {
			if($oldcol <> $bmk['col']) {
				$oldcol=$bmk['col'];
				if($col==0) $configarray1=array();
				$configarray1[]=array('text' => $bmk['col'], 'callback_data' => 'draw '.$bmk['col']);
				$col++;
				if($col == $maxcol) {
					$col=0;
					$configarray[]=$configarray1;
				}
			}
		}
		if(count($configarray1) > 0) {
			while(count($configarray1) < $maxcol) {
				$configarray1[]=array('text' => '-', 'callback_data' => 'non');
			}
			$configarray[]=$configarray1;
		}
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);
		$answer=$tl->getstr('drawWaehle');
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			if(IsSet($tgres['message_id'])) {
				$answer['message_id']=$tgres['message_id'];
			}
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;



?>