<?php
//Ü	

if (php_sapi_name() != 'cli') { // Im Kommandozeilenmodus braucht es kein flush ;-)
	header( 'Content-type: text/html; charset=utf-8' ); // https://www.php.net/manual/de/function.ob-flush.php
	header("HTTP/1.0 200 OK"); // Wenn das zu lange dauert, sendet Telegram sonst die Anfrage erneut.
	flush();
	ob_flush();
}

	include_once "class/Autoloaderclass.php.inc";

	if($composer_vendorautoload<>'') {
		require $composer_vendorautoload;
	}
	
	include "inc/constvar.php.inc";
	include "inc/languageger.php.inc";
	include "inc/functionen.php.inc";
	include "inc/logging.php.inc";
	
	date_default_timezone_set($bot_default_user_timezone);
	
	// Datenbankklasse Standard PDO-Objekt
	$pdo=connectpdo();
	if(!is_object($pdo)) {
		error_log(__FILE__." PDO-Error 1");
		die ("PDO-Error 1");
	}
	
	// Telegram-Klasse telegramdbclass - telegramclass
	$tg=new telegramdb($bot_token,$pdo,$database_table_main);
	if(!is_object($tg)) {
		error_log(__FILE__." Telegram-Error 2");
		die ("Telegram-Error 2");
	}
	$tg->chat_usedb=true; // Datensatz für den Chat anlegen und laden
	$tg->default_language=$bot_default_user_language; // Wird für neue Nutzer genutzt
	$tg->default_timezone=$bot_default_user_timezone; // Wird für neue Nutzer genutzt
	$tg->updateuserdata=true; // Nutzerdaten ständig aktuell halten
	$tg->disablewebpagepreview=true;
	$tg->migrate_chat_info_fkt='tgmigratechatinfo';
	
	// Logmessaging-Konfiguration
	$logmess=logmessaging::getInstance();
	$logmess->tg=$tg;
	$logmess->modlist=array(); // Erst mal mods ausschalten, nach laden der TG-User-Daten kann die Adminlist aus der Chat-Konfig verwendet werden
//	$logmess->normalchat=$fs->logginggroup_id; // Noch keine Konfigurationsmöglichkeit
	$logmess->globalchat=$bot_adminlog;
	$logmess->setlogdir($bot_errorlog); // Pfad zum Log-Verzeichnis
	$logmess->bot_owner = $bot_owner; // Benutze für Fehlermeldungen $bot_owner, $bot_owner bekommt Fileinfos in seine Log-Meldungen
	$logmess->sendtgusername=true; // Usernamen in die Nachricht aufnehmen
	if(IsSet($logmess_replace)) $logmess->replacestrings=$logmess_replace; // Strings aus der Hiddenvariables, die ersetzt werden sollen

	$chatadm=new chatadmin($pdo, $database_table_main);
	if(!is_object($chatadm)) {
		error_log(__FILE__." ChatAdmin-Error 3");
		die ("Chatadmin->Error 3");
	}
	
	$ph=new portalhunt($pdo, $database_table_main, $tg);
	if(!is_object($ph)) {
		error_log(__FILE__." Portalhunt-Error 4");
		die ("Portalhunt-Error 4");
	}
	
	// Translate-Klasse
	$tl=new translate($languageger,'ger',$bot_languagedir);
	if(!is_object($tl)) {
		error_log(__FILE__." Translate-Error 5");
		die ("Translate-Error 5");
	}
	$tl->setlanguage($bot_default_user_language); // Wird vor dem Laden der User Config als Bot Default Sprache verwendet
	$tl->tokencounterfile=$bot_languagedir.'tokencounter.json';
	
	$puimage=new puzzleimage($cry_apitoken, $cry_apiurl, $cry_uploadurl, $cry_webportal);
	if(!is_object($puimage)) {
		error_log(__FILE__." PuzzleImage-Error 6");
		die ("PuzzleImage-Error 6");
	}

if(IsSet($_GET['info'])) {
		echo $bot_info;
	}elseif(IsSet($_GET['instwebhook'])) {
		$res=$tg->installwebhook();
		echo $res;
		exit;
	}elseif(IsSet($_GET['uninstwebhook'])) {
		$res=$tg->uninstallwebhook();
		echo $res;
		exit;
	}elseif(IsSet($_GET['sendmsg'])) { // Zum Testen
		$res=$tg->sendmessage($bot_owner,"Telegram Bot Test");
		echo print_r($res,true);
		exit;
	}elseif(IsSet($_GET['createdb'])) { // Anlegend der Datenbank
		include 'webhook.createdb.php.inc';
		exit;
	}else{
		// *******
		// Ab hier der eigentliche BOT
		// *******
		$msg=file_get_contents("php://input");
		$msgarr=json_decode($msg,true);
		$isprimedata=false;
		$tgres=$tg->decodemessage($msg,$isprimedata);
//				$tg->sendmessage($tg->user_id,print_r($msgarr,true));
		if(!IsSet($tgres['message_typ'])) { // War das überhaupt was von Telegram?
			echo str_replace("\n","<br>",$tl->getstr('webcmdhelp'));
			// Fehler bei der Dekodierung
		}else{
			if($tg->chat_type == 'private' and !IsSet($tgres['db']['rightslevel'])) { // Nicht am Bot angemeldet (DB)
				$tg->sendmessage($tg->user_id,$tl->getstr('gibstartein'));
			}elseif($tg->chat_type == 'private' and $tgres['db']['rightslevel'] <= 4) { // Ignorieren oder blockieren
				exit;
			}else{
				// Sprache laden
				$tl->setlanguage($tg->language);
				// Zeitzone anpassen
				date_default_timezone_set($tg->timezone);
				// Besondere Aufrufe
				// Gruppennachricht
				if(($tgres['chat']['type'] == 'group'  or $tgres['chat']['type'] == 'supergroup') and $tgres['message_typ'] == 'query') {
					$ph->loadchatdata();
					$logmess->modlist=$chatadm->getadmins($tg->chat_id); // Admins für den Chat in die LogMessaginggruppe eintragen
					if(IsSet($tgres['text'])) {
						if(IsSet($bot_admins[$tg->user_id])) {
							if($tgres['command'] == '/addchat') {
								include 'commands_admin/addchat.php.inc';
							}elseif($tgres['command'] == '/revokechat') {
								include 'commands_admin/revokechat.php.inc';
							}elseif($tgres['command'] == '/blockchat') {
								include 'commands_admin/blockchat.php.inc';
							}
						}
						if($tg->chat_rightslevel < 4) exit(); // Chat blockiert
						if($tgres['command'] == '/help') { // Hilfe für die Gruppe
							include 'commands_group/help.php.inc';
						// Portal für das Sheet
						}
						if($tgres['message_typ'] == 'callback_query' and $tgres['command'] == 'logmessage') {
							$logmess->outputpause($tgres);
							exit;
						}
						if($tgres['command'] == '/ops') { // Hilfe für die Gruppe
							include 'commands_group/ops.php.inc';
						// Portal für das Sheet
						}
						if(IsSet($tgres['new_chat_participant']) and $tgres['new_chat_participant']['id'] == substr($bot_token,0,strlen($tgres['new_chat_participant']['id']))) {
							// Zu einem Chat hinzugefügt
							include 'commands_group/new_chat_participant.php.inc';
						}
						if($tg->chat_rightslevel < 8) exit; // Chat nicht aktiv
						// URL-Codierung entfernen
						$tgres['text']=urldecode($tgres['text']);
						// In einheitliche Links wandeln
						$tgres['text']=str_replace('pll/', 'pll=', $tgres['text']);
						// Dekodierungsarray aufbauen
						$portalpreg[]=array('name' => 'K L N', 'preg' => '/(?<col>[a-z]{1,2})[-_]*(?<row>[0-9]{1,2})\s+(?<link>.*?(?<mtyp>pll)=(?<lat>[-0-9.]+),(?<lng>[-0-9.]+).*?)\s+(?<name>.*)/i');
						$portalpreg[]=array('name' => 'L K N', 'preg' => '/(?<link>.*?(?<mtyp>pll)=(?<lat>[-0-9.]+),(?<lng>[-0-9.]+)[^\s]*)\s*(?<col>[a-z]{1,2})[-_]*(?<row>[0-9]{1,2})\s*(?<name>.*)/i');
						$portalpreg[]=array('name' => 'K N L', 'preg' => '/(?<col>[a-z]{1,2})[-_]*(?<row>[0-9]{1,2})\s+(?<name>.*)\s+(?<link>.*?(?<mtyp>pll)=(?<lat>[-0-9.]+),(?<lng>[-0-9.]+).*?)/i');
						$portalpreg[]=array('name' => 'Portal Summary', 'preg' => '/(?<col>[a-z]{1,2})[-_]*(?<row>[0-9]{1,2})\s+.*?Portal name: (?<name>[^\n]*).*(?<link>.*?(?<mtyp>pll)=(?<lat>[-0-9.]+),(?<lng>[-0-9.]+).*?)/is');
						$i=0;
						$found=false;
						while(IsSet($portalpreg[$i]) and $found == false) {
							if(preg_match($portalpreg[$i]['preg'],$tgres['text'],$arr)) {
								$found=true;
								$pregarr=$arr;
							}
							$i++;
						}
						if($found) include 'commands_group/sendportalpreg.php.inc';
						$failpreg[]=array('name' => 'Map Link', 'preg' => '/(?<col>[a-z]{1,2})[-_]*(?<row>[0-9]{1,2})\s+(?<link>.*?(?<mtyp>ll)=(?<lat>[-0-9.]+),(?<lng>[-0-9.]+).*?)\s+(?<name>.*)/i', 'tltoken' => 'PHMapAdress');
						$i=0;
						$found=false;
						foreach($failpreg AS $failset) {
							if(preg_match($failset['preg'],$tgres['text'])) {
								$tg->sendmessage($tg->chat_id, $tl->getstr($failset['tltoken']));
							}
						}
					}
				}
				if($tgres['chat']['type'] <> 'private') exit; // Damit Userbefehle nicht in der Gruppe aufgerufen werden können
				
				if(IsSet($tg->botconf['activchat_id'])) {
					$logmess->modlist=$chatadm->getadmins($tg->botconf['activchat_id']); // Admins für den Chat in die LogMessaginggruppe eintragen
					$ph->loadchatdata($tg->botconf['activchat_id']);
				}
				switch($tgres['message_typ']) {
					// ******************** Query *******************************
					case 'query':
						switch($tgres['command']) {
							case '/cancel':
								exit; // Befehl wird in der Telegramdb-Klasse verarbeitet
							break;
							case '/start':
								$tg->sendmessage($tg->user_id,$tl->getstr('willkommen'));
								exit;
							break;
							case '/help':
								include "commands/help.php.inc";
							break;
							case '/config': // Einstellen der Userdaten (Sprache, Zeitzone...)
								include "commands/config.php.inc";
							break;
							case '/next':
								include "commands/next.php.inc";
							break;
							case '/names':
								include "commands/names.php.inc";
							break;
							case '/readsheet';
								include "commands/readsheet.php.inc";
							break;
							case '/bookmark';
							case '/bookmarks';
								include "commands/bookmark.php.inc";
							break;
							case '/missing';
								include "commands/missing.php.inc";
							break;
							case '/draw';
								include "commands/draw.php.inc";
							break;
							case '/chats';
								include "commands_admin/chats.php.inc";
							break;
							case '/exportlanguage':
								include "commands/exportlanguage.php.inc";
							break;
							case '/importlanguage':
								include "commands/importlanguage.php.inc";
							break;
							case '/listlanguageowner':
								include_once "commands_admin/listlanguageowner.php.inc";
							break;
							case '/searchuser':
								include_once "commands_admin/searchuser.php.inc";
							break;
							case '/setlanguageowner':
								include_once "commands_admin/setlanguageowner.php.inc";
							break;
							case '/mods':
								include_once "commands_admin/mods.php.inc";
							break;
							case '/dsgvo':
								include_once "commands/dsgvo.php.inc";
							break;
							case '/deletealldata':
								include_once "commands/deletealldata.php.inc";
							break;
							case '/setuserstatus':
								include_once "commands_admin/setuserstatus.php.inc";
							break;
							case '/botadmins':
								include "commands/botadmins.php.inc";
							break;
							case '/check':
								include "commands/check.php.inc";
							break;
							case '/newsheet':
								include "commands/newsheet.php.inc";
							break;
							case '/renamesheets':
								include "commands/renamesheets.php.inc";
							break;
							case '/version':
								include_once "commands_admin/version.php.inc";
							break;
							case '/request':
								include_once "commands_admin/request.php.inc";
							break;
						}
					break;
					// ******************* Callback *****************************
					case 'callback_query':
						switch($tgres['command']) {
							case 'logmessage':
								$logmess->outputpause($tgres);
							break;
							case 'config':
								include "commands/config.php.inc";
							break;
							case 'conflanguage':
								include "commands/config.language.php.inc";
							break;
							case 'conftimezone':
								include "commands/config.timezone.php.inc";
							break;
							case 'confsheet':
								include "commands/config.sheet.php.inc";
							break;
							case 'confchat':
								include_once "commands/config.chat.php.inc";
							break;
							case 'confFS':
								include_once "commands/config.fs.php.inc";
							break;
							case 'next':
								include "commands/next.php.inc";
							break;
							case 'draw';
								include "commands/draw.php.inc";
							break;
							case 'chats';
								include "commands_admin/chats.php.inc";
							break;
							case 'setlanguageowner':
								include_once "commands_admin/setlanguageowner.php.inc";
							break;
							case 'setuserstatus':
								include_once "commands_admin/setuserstatus.php.inc";
							break;
							case 'confchatlanguage':
								include "commands/config.chatlanguage.php.inc";
							break;
							case 'renamesheets':
								include "commands/renamesheets.php.inc";
							break;
							case 'readsheet';
								include "commands/readsheet.php.inc";
							break;
						}
					break;
					// *************** Command ****************************
					case 'command': // Kommandos (DB) die auf Antwort warten
						switch($tgres['command']) {
							case 'confsheet':
								include "commands/config.sheet.php.inc";
							break;
							case 'confchat':
								include_once "commands/config.chat.php.inc";
							break;
							case 'confFS':
								include_once "commands/config.fs.php.inc";
							break;
							case 'setlanguageowner':
								include_once "commands_admin/setlanguageowner.php.inc";
							break;
						}
					break;
					// ************** Location ****************************
					case 'location': // Übersenden der Position
//						include "commands_admin/fsconfig.php.inc";
					break;
					// ************** Document ****************************
					case 'document': // Übersenden einer Datei
						if(preg_match('/^'.$tg->user_id.'_language( \(\d+\))?\.csv$/',$tgres['filename'])) {
							include "commands/importlanguage.php.inc";
						}
					break;
				}
			}
			$chat_title='--';
			$portalanz=0;
			if(IsSet($tg->botconf['activchat_id']) and $chatadm->isadmin($tg->botconf['activchat_id'], $tg->user_id)) {
//		if(IsSet($tg->botconf['activchat_id'])) {
				$chat_title=$chatadm->getchat($tg->botconf['activchat_id'])['first_name'];
				$portalanz=$ph->getanzportal($tg->botconf['activchat_id']);
			}
			$tg->sendmessage($tg->user_id,$tl->getstr('PrivInfo',[$chat_title, $portalanz]));
/*			if($tgres['user_id'] <> $bot_owner) {
				$tg->sendmessage($tg->user_id,$tl->getstr('InputNoUndestand'));
			}else{
				$tg->sendmessage($bot_owner,json_encode($tgres));
				if(IsSet($tgres['args'])) $tg->sendmessage($bot_owner,print_r($tgres['args'],true));
			}*/
		}
	}


?>