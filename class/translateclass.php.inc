<?php
/**** Klasse für alternative Sprachen in einer Anwendung
*
*	@Author: Jan Clauß
* @Version: 1.22 (2021-04-18)
* 
*/

class translate
{
	public $defaultlanguage=array();
	public $defaultlanguage_name=''; // ger,eng, rus...
	public $defaultlanguage_desc='';
//	public $language=array(); // Die defaultla
	public $otherlanguage_name=''; // Name der gesetzten Sprache
	public $otherlanguage=array(); // Ein Cache für andere, als die default Sprache, wird befüllt, wenn benötigt, Index ist der Sprachname

//********************************
//
//$languageger['language_name']=array('date'=>'2020-01-01', 'desc'=>'Der Name der Sprache', 'text'=>'Deutsch'); // Dieser Token sollte immer vorhanden sein
//$languageger['other Token']=array('date'=>'2020-02-02', 'desc'=>'Beschreibung', 'text'=>'Text');
//$languageger['mit Parametern']=array('date'=>'2020-02-02', 'desc'=>'Parameterübergabe', 'text'=>'Parameter 1: [#1#], Parameter 2: [#2#]'); // Parameter werden getstr() als Array übergenben
//
//********************************
	public $languagedir='';
	
	public $testfunction=''; // Funktion zum Testen der übergebenen Texte
		// Return array('ok' == true, 'error_code' => 'code', 'description' => 'text [,parameters => array('retry_after' => sec(int))])
		
	public $tokencounter=array(); // Zählt die Aufrufe der einzelnen Token
	public $tokencounterfile=''; // ist hier eine Datei eingetragen, dann wird der Tokencounter darin im Destructor gespeichert
	public $tokenfaillog='';
	
	function __construct($language, $languagename, $languagedir)
	{
  	$this->defaultlanguage=$language;
  	$this->defaultlanguage_name=$languagename;
  	$this->defaultlanguage_desc=$languagename;
  	$this->languagedir=$languagedir;
  	$this->tokenfaillog=$languagedir.'tokenfail.log';
  	// Aktuelle Sprache bei initialisierung = default
  	$this->otherlanguage_name=$languagename;
  }
  
  function __destruct()
  {
  	if($this->tokencounterfile <> '') { // Schreiben der Zählung der Token
 			$maintokenjson=file_get_contents($this->tokencounterfile);
 			if($maintokenjson === false) {
 				// Wenn File (json) nicht existiert ein Array mit allen Token erstellen
 				foreach($this->defaultlanguage AS $token => $arr) {
 					$maintokencounter[$token]=0;
 				}
 			}else{
 				// Ansonsten aus json laden
 				$maintokencounter=json_decode($maintokenjson,true);
 			}
 			// gezählte Token addieren
 			foreach($this->tokencounter AS $token => $anz) {
 				if(!IsSet($maintokencounter[$token])) $maintokencounter[$token]=0;
 				$maintokencounter[$token]=$maintokencounter[$token]+$anz;
 			}
 			// in JSON wandeln
 			$maintokenjson=json_encode($maintokencounter);
 			// und zurück Schreiben
 			file_put_contents($this->tokencounterfile, $maintokenjson);
  	}
  }
  
  function setlanguage($languagename)
  // Prüft und Setzt die aktuelle Sprache, lädt sie nach $this->otherlanguage
  {
  	$ret=false;
  	if($languagename == $this->defaultlanguage_name) { // Defaultsprache ausgewählt
  		$this->otherlanguage_name=$languagename;
  		$ret=true;
  	}elseif(is_file($this->languagedir.'language_'.$languagename.'.json')) {
			// Sprachdatei existiert
			if($language=file_get_contents($this->languagedir.'language_'.$languagename.'.json')) {
				$language=json_decode($language,true);
				if(IsSet($language['language_name'])) {
					// Spracharray ist gültig
					$this->otherlanguage[$languagename]=$language;
		  		$this->otherlanguage_name=$languagename;
					return(true);
				}
				error_log('error while loading language');
			}
		}else{
			error_log("Language not found ".$this->languagedir.'language_'.$languagename.'.json');
		}
		return $ret;
  }
  
  function isstr($token)
  // Prüft, ob der Token existiert
  {
  	return(IsSet($this->defaultlanguage[$token]));
  }

	function getstr($token, $args=array(), $defaultstr='')
	{
		if(!is_array($args)) $args=array($args); // Nur ein Wert übergeben, in Array wandeln
		return($this->getlangstr($this->otherlanguage_name, $token, $args, $defaultstr));
	}
	
	function getlangstr($lang, $token, $args=array(), $defaultstr='')
	{
		$retstr='';
		// Prüfen, ob Token existiert
		if(!IsSet($this->defaultlanguage[$token])) {
			if($defaultstr=='') {
				error_log(__FILE__.' '.__LINE__." Unknown token: #$token#");
				$retstr="?$token?";
				if($this->tokenfaillog <> '') error_log(date('Y.m.d H:i:s')." - $token\n",3,$this->tokenfaillog);
			}else{
				$retstr=$defaultstr;
			}
		}else{
			// Token zählen
			if(!IsSet($this->tokencounter[$token])) $this->tokencounter[$token]=0;
			$this->tokencounter[$token]++;
			// default-language-Text laden
			$retstr=$this->defaultlanguage[$token]['text'];
			// Prüfen, ob sprache Existiert, schon geladen, und laden der Sprache
			if($lang <> $this->defaultlanguage_name) { // Nicht die default-Language
				if(!IsSet($this->otherlanguage[$lang])) { // Sprache noch nicht geladen
					if($this->iflanguage($lang)) { // Sprache nach otherlanguage laden
						if($language=file_get_contents($this->languagedir.'language_'.$lang.'.json')) {
							$language=json_decode($language,true);
							if(IsSet($language['language_name'])) { // Prüfen, ob gültiges Sprach-Array
								$this->otherlanguage[$lang]=$language;
							}
						}
					}
				}
			}
			if(IsSet($this->otherlanguage[$lang][$token])) {
				$retstr=$this->otherlanguage[$lang][$token]['text'];
			}
			$counter=0;
			while(preg_match('/\[#(\d+)#\]/', $retstr, $arr) and $counter<20) {
				$counter++;
				$repl='/\[#'.$arr[1].'#\]/';
				if(IsSet($args[$arr[1]-1])) {
					$retstr=preg_replace($repl,$args[$arr[1]-1],$retstr,1);
				}else{
					$retstr=preg_replace($repl,'[# '.$arr[1].' #]',$retstr);
				}
			}
		}
		return($retstr);		
	}
	
	function iflanguage($languagename)
	{
		if($languagename==$this->defaultlanguage_name or is_file($this->languagedir.'language_'.$languagename.'.json')) {
			return(true);
		}
		return(false);
	}
	
	function listlanguages($percent=false)
	{
		$langlist[]=array('name' => $this->defaultlanguage_name, 'desc' => $this->defaultlanguage['language_name']['text'], 'default' => true, 'percent' => 100);
		if(!$handle=opendir($this->languagedir)) {
			error_log("Language Dir ".$this->languagedir." nicht gefunden");
		}else{
			while(($file=readdir($handle)) !== false) {
				if(preg_match('/^(language_([a-z]{2,20})\.json)$/', $file, $arr)) {
					
					$langarr=json_decode(file_get_contents($this->languagedir.$arr[1]),true);
					$langname=$arr[2];
					if(IsSet($langarr['language_name'])) $langname=$langarr['language_name']['text'];
					$langpercent=-1;
					if($percent) $langpercent=$this->getpercent($langarr);
					$langlist[]=array('name' => $arr[2], 'desc' => $langname, 'percent' => $langpercent);
				}
			}
			closedir($handle);
		}
		if(IsSet($langlist)) return($langlist);
		return(false);
	}
	
	function getpercent($langarr) {
		$def=0;
		$test=0;
		foreach($this->defaultlanguage AS $deftoken => $deflang) {
			$def++;
			if(IsSet($langarr[$deftoken])) $test++;
		}
		if($def==0) return(0);
		return(intval($test/$def*100));
	}

  function importfromfile($file, $languages) //$language ist array mit Sprachen die Importiert werden dürfen array('eng', 'rus')
  {
		if(!$csvfile=fopen($file,'r')) {
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Cannot find CSV-File', 'errorInfo' => 'Cannot find CSV-File'));
		}
		while(!feof($csvfile)) {
 			$csvarray[] = fgetcsv  ($csvfile, 0, ';', '"'); // Semikolon als Trenner
		}
		fclose($csvfile);
		if(!IsSet($csvarray[0][2]) or $csvarray[0][2]<>'Description') {  // nicht ; separiert, jetzt mit , probieren
			if(IsSet($csvarray)) unset($csvarray);
			if($csvfile=fopen($file,'r')) {
				while(!feof($csvfile)) {
    			$csvarray[] = fgetcsv($csvfile, 0, ',', '"');
				}
				fclose($csvfile);
			}
		}
		if(!IsSet($csvarray[0][3]) or $csvarray[0][3]<>'Description') {
			return(array('ok' => false, 'error_code' => '3', 'description' => 'Cannot read CSV-File', 'errorInfo' => 'Cannot read CSV-File'));
		}
		return($this->importfromarray($csvarray,$languages));
	}
	
	function importfromarray($importarray, $languages) {
		if(!IsSet($importarray[0][3]) or $importarray[0][3]<>'Description') {
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Cannot understand Data', 'errorInfo' => 'Cannot understand Data'));
		}else{
	  	foreach($languages AS $langname) {
  			$languagesi[$langname]=$langname;
  		}
			for($i=4;IsSet($importarray[0][$i]);$i++) { // schauen welche Sprachen enthalten sind
				if(IsSet($languagesi[$importarray[0][$i]])) { // Prüfen, ob Importiert werden darf
					$csvlang[$importarray[0][$i]]['idx']=$i; // Index (Spalte in der CSV) in $csvlang speichern (idx)
					// aktuelle Sprachdatei laden, damit soll es reichen nur geänderte Zeilen zu aktualisieren
					$filename=$this->languagedir.'language_'.$importarray[0][$i].'.json';
					if(file_exists($filename)) {
						if($file=fopen($filename,'r')) {
							$json='';
							$jsonlength=-1;
							while(!FEOF($file) and $jsonlength<>strlen($json)) {
								$jsonlength=strlen($json); //Um Endlosschleifen vorzubeugen
								$json.=fread($file,100000);
							}
							fclose($file);
							$csvlang[$importarray[0][$i]]['translate']=json_decode($json,true); // Lädt die aktuellen Sprachdaten in die csvlang['translate']
						}
					}else{
						error_log("Can not read language ".$importarray[0][$i]."! Danger, Data loses!");
					}
				}
			}
			if(!IsSet($csvlang)) {
				return(array('ok' => false, 'error_code' => '2', 'description' => 'No matched language included', 'errorInfo' => 'No matched language included'));
			}else{
				for($i=1;IsSet($importarray[$i]); $i++) { // Jedes einzelne Token der importierten Sprachdatei
					if($importarray[$i][0] <> '') {
						if(!IsSet($this->defaultlanguage[$importarray[$i][0]])) { // Gibt es das Token?
							error_log("Token [".$importarray[$i][0]."] not found");
						}else{ // Token wurde gefunden
							foreach($csvlang AS $lang => $larr) { // Für jede Sprache in die csvlang['translate']
								if(IsSet($importarray[$i][$larr['idx']]) and $importarray[$i][$larr['idx']] <> '' and (!IsSet($csvlang[$lang]['translate'][$importarray[$i][0]]['text']) or $importarray[$i][$larr['idx']] <> $csvlang[$lang]['translate'][$importarray[$i][0]]['text'])) {
									if($this->testfunction !=='' and function_exists($this->testfunction)) {
										// Testen, ob Output OK
										$testfunction=$this->testfunction;
										$resarr=$testfunction($importarray[$i][$larr['idx']]);
										if($resarr['ok'] <> true) { // Fehler beim Versenden der Nachricht ins Error-Log
//											error_log(__FILE__.", ".__LINE__.print_r($resarr,true));
											$errmsgs[$lang][]=array('token' => $importarray[$i][0], 'code' => $resarr['error_code'], 'desc' => $resarr['description']);
											if(IsSet($resarr['parameters']['retry_after'])) sleep($resarr['parameters']['retry_after']); // Speziell Telegram, wenn zu viele Nachrichten gesendet wurden
										}else{
											$csvlang[$lang]['changing']=true; // merken, das was geändert wurde
											$csvlang[$lang]['translate'][$importarray[$i][0]]['text']=$importarray[$i][$larr['idx']];
										}
									}else{
										$csvlang[$lang]['changing']=true; // Merken, das was geändert wurde
										$csvlang[$lang]['translate'][$importarray[$i][0]]['text']=$importarray[$i][$larr['idx']];
									}
								}
							}
						}
					}
				}
				$funcerrors='';
				$funcoutput='';
				foreach($csvlang AS $lang => $larr) {
					if(IsSet($errmsgs[$lang])) { // Fehler beim Test der Ausgabe festgestellt
						$funcerrors.="Fail by language $lang:\n";
						$funcerrors.="NOT all of language $lang saved!\n";
					}
					if(IsSet($larr['changing'])) {
						$langjson=json_encode($larr['translate']);
						if(file_put_contents($this->languagedir.'language_'.$lang.'.json',$langjson)) {
							file_put_contents($this->languagedir.'language_'.$lang.'_'.date('Y-m-d His').'.json.bkup',$langjson); // Sofort ein Backup anlegen
							$funcoutput.="Language $lang saved\n";
						}else{
							$funcoutput="Language $lang not saved\n";
						}
					}
				}
			}
		}
		$retarr=array('ok' => true, 'error_code' => '0', 'description' => $funcoutput);
		if($funcerrors <> '') {
			$retarr['errors']=$funcerrors;
			$retarr['errorlist']=$errmsgs;
		}
		if(IsSet($warnmsgs)) $retarr['warnlist']=$warnmsgs;
		return($retarr);
  }
  
	function exporttoarray($languages) // $language ist array mit Sprachen array('eng', 'rus')
	{
 		$englishexport=false;
  	if($this->defaultlanguage_name<>'eng' and $this->iflanguage('eng')) {
  		$exportlanguages[]='eng'; // Wenn möglich englische Übersetzung
  		$englishexport=true;
  	}elseif($this->defaultlanguage_name<>'en' and $this->iflanguage('en')) {
  		$exportlanguages[]='en'; // Wenn möglich englische Übersetzung
  		$englishexport=true;
  	}
  	
  	foreach($languages AS $langname) {
  		if(preg_match('/^([a-z0-9]+)$/i', strtolower($langname), $arr)) {
  			if(($arr[1] <> 'eng' and $arr[1] <> 'en') or $englishexport == false) { // wenn englisch vorhanden ist, wurde es oben schon eingefügt
  				$exportlanguages[]=$arr[1];
  			}
  		}
  	}
		$ret1array[]='Token';
		$ret1array[]='LastChange';
		$ret1array[]='Destination';
		$ret1array[]='Description';
		$ret1array[]=$this->defaultlanguage_name;
		foreach($exportlanguages AS $langtoken) {
			$ret1array[]=$langtoken;
			// Sprachen aus json laden
			$filename=$this->languagedir.'language_'.$langtoken.'.json';
			if(file_exists($filename)) {
				if($file=fopen($filename,'r')) {
					$json='';
					$jsonlength=-1;
					while(!FEOF($file) and $jsonlength<>strlen($json)) {
						$jsonlength=strlen($json); //Um Endlosschleifen vorzubeugen
						$json.=fread($file,100000);
					}
					fclose($file);
					$langdata[$langtoken]=json_decode($json,true);
				}
			}else{
				error_log(__FILE__.' '.__LINE__." Can not read language $langtoken");
				$langdata[$langtoken]['language_name']['text']=$langtoken; // Neue Sprache
			}
		}
		$retarray[]=$ret1array;
		unset($ret1array);
		foreach($this->defaultlanguage AS $token => $tokenarr) { // Jedes Token der Default-Sprache
			$ret1array[]=$token;
			if(IsSet($tokenarr['date'])) {
				$ret1array[]=$tokenarr['date'];
			}else{
				$ret1array[]='2020-01-01';
			}
			if(IsSet($tokenarr['dest'])) {
				$ret1array[]=$tokenarr['dest'];
			}else{
				$ret1array[]='all';
			}
			$ret1array[]=$tokenarr['desc'];
			$ret1array[]=$tokenarr['text'];
			foreach($exportlanguages AS $langtoken) {
				if(IsSet($langdata[$langtoken][$token])) {
					$ret1array[]=$langdata[$langtoken][$token]['text'];
				}else{
					$ret1array[]='';
				}
			}
			$retarray[]=$ret1array;
			unset($ret1array);
		}
		return($retarray);
	}

	function exporttofile($file, $languages) //$language ist array mit Sprachen array('eng', 'rus')
	{
		$csvfile=fopen($file,'w');
		if(!$csvfile) {
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Cannot create CSV-File', 'errorInfo' => 'Cannot create CSV-File'));
		}
		$csvarray=$this->exporttoarray($languages);

		foreach($csvarray AS $csvline) {
			fputcsv($csvfile, $csvline,';'); //Zeile schreiben
		}
		fclose($csvfile);
	}

}

?>