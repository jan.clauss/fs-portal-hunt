<?php

/**** Klasse für Daten von Portalhunt
*
*	@Author: Jan Clauß
* @Version: 1.0
* 
*/


class portalhunt
{
	
	public $pdo = null; // Das PDO-object für den Bot (Constructor)
	public $database_table_telegram = ''; // Die Datenbank für den Bot (Constructor)
	public $database_table_portale = ''; // Die Datenbank für gemeldete Portale (Constructor)
	public $chat_id=0; // ID des Chats, Wird mit loadchatdata gesetzt
	public $portalhuntsheet=''; // Das Sheet zum erfassen der gefundenen Portale
	public $deleteoriginalmessage=true; // Originalmessage der User wird durch den Bot ersetzt
	public $sheetnames='letterandnumber'; // letterandnumber A_1, letter A, number 1, original COL_1 //Legt das Format der Sheetnamen fest
//	public $sheetcount=26; // Anzahl der Sheetseiten // Verlagert in constvar.php.inc global $sheetsitesanz 2021-02-14
	public $portaldatarange=SHEETRRANGE_COL;
	public $starttime=0; // Zeitpunkt, ab wann Portale gesendet werden können
	public $passcodesheetdata=[];
	public $nextoldtime=5*60; // Wenn das ältäste Portal diese Zeit erreicht hat, wird bei jedem Portal eine /next-Meldung ausgegeben
	public $autoinsertifempty=false; // Fügt das Portal direkt ins Sheet ein, wenn noch nichts drin steht
	public $fstitle='';  // Via Fevgames-Abruf für Crytix Seite
	public $fsdate=''; // Via Fevgames-Abruf für Crytix Seite
	public $fslocation=''; // Via Fevgames-Abruf für Crytix Seite
	public $fevgamesid=0; // Via Fevgames-Abruf für Crytix Seite
	public $tg=null; // Telegramclasse
	
	function __construct($pdoobj, $database_table, $tg)
	{
		if(!is_object($pdoobj))
			return(array('ok' => false, 'error_code' => '1', 'description' => '$pdo must be an Database Object'));
		$this->pdo=$pdoobj;
		$this->database_table_telegram=$database_table;
		$this->database_table_portale=$database_table.'_Portale';
		$this->tg=$tg;
		return(array('ok' => true));
	}
	
  function createdb()
  {
  	$db=explode('.',$this->database_table_portale);
  	$database=$db[0];
  	$table=$db[1];
  	$sql="USE $database";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo())));
		}
  	$sql="
CREATE TABLE IF NOT EXISTS `".$table."` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`chat_id` BIGINT(20) NOT NULL DEFAULT '0',
	`agent` VARCHAR(255) NOT NULL DEFAULT '0' COLLATE 'utf8mb4_bin',
	`row` TINYINT(4) NOT NULL DEFAULT '0',
	`col` CHAR(3) NOT NULL DEFAULT 'A' COLLATE 'utf8mb4_bin',
	`lat` CHAR(12) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`lng` CHAR(12) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`name` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`message_id` BIGINT(20) NOT NULL DEFAULT '0',
	`erstellt` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `chat_id` (`chat_id`) USING BTREE
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(array('ok' => true));
  }
  
	function loadchatdata($chat_id=0)
	{
		global $sheetsites, $sheetsitesanz;
		$this->chat_id=$chat_id;
		if($chat_id <> 0) { // Bei privaten Bot Messages wird die chat_botconf nicht automatisch geladen
			$this->tg->chat_readbotconf($chat_id);
		}
		if(IsSet($this->tg->chat_botconf['deleteoriginalmessage'])) $this->deleteoriginalmessage = $this->tg->chat_botconf['deleteoriginalmessage'];
		if(IsSet($this->tg->chat_botconf['portalhuntsheet'])) $this->portalhuntsheet=$this->tg->chat_botconf['portalhuntsheet'];
		if(IsSet($this->tg->chat_botconf['sheetnames'])) $this->sheetnames=$this->tg->chat_botconf['sheetnames'];
//		if(IsSet($this->tg->chat_botconf['sheetcount'])) $this->sheetcount=$this->tg->chat_botconf['sheetcount'];
		if(IsSet($this->tg->chat_botconf['portaldatarange'])) $this->portaldatarange=$this->tg->chat_botconf['portaldatarange'];
		if(IsSet($this->tg->chat_botconf['starttime'])) $this->starttime=$this->tg->chat_botconf['starttime'];
		if(IsSet($this->tg->chat_botconf['passcodesheetdata'])) $this->passcodesheetdata=$this->tg->chat_botconf['passcodesheetdata'];
		if(IsSet($this->tg->chat_botconf['autoinsertifempty'])) $this->autoinsertifempty=$this->tg->chat_botconf['autoinsertifempty'];
		if(IsSet($this->tg->chat_botconf['fstitle'])) $this->fstitle=$this->tg->chat_botconf['fstitle'];
		if(IsSet($this->tg->chat_botconf['fsdate'])) $this->fsdate=$this->tg->chat_botconf['fsdate'];
		if(IsSet($this->tg->chat_botconf['fslocation'])) $this->fslocation=$this->tg->chat_botconf['fslocation'];
		if(IsSet($this->tg->chat_botconf['fevgamesid'])) $this->fevgamesid=$this->tg->chat_botconf['fevgamesid'];
		if(IsSet($this->tg->chat_botconf['nextoldtime'])) $this->nextoldtime=$this->tg->chat_botconf['nextoldtime'];
		
		for($i=1; $i <= SHEETSITESANZ; $i++) {
			$letter=$this->dezColumn($i);
			switch($this->sheetnames) {
				case 'number':
					$name=$i;
				break;
				case 'letter':
					$name=$letter;
				break;
				case 'letterandnumber':
					$name=$letter.'_'.$i;
				break;
				default:
					$name='COL_'.$i;
				break;
			}
			$sheetsites[$letter]=$name;
		}
	}
	
	function dezColumn($count)
	{
		if($count <= 0) return('');
		$column='';
		if($count>26) {
			// Mehrstellige Spaltenbezeichnung
			$column=$this->dezColumn(intval($count/26));
			$count=$count-intval(intval($count/26)*26);
		}
		$column.=chr($count+64);
		return($column);
	}

	
	function savechatdata($chat_id=0)
	{
		if($chat_id == 0) $chat_id=$this->tg->chat_id;
		if($chat_id >= 0) { // Fehlerhafte Chat-ID
			error_log(__FILE__.', '.__LINE__.' Fehlerhafte Chat-ID - exit');
			exit;
		}
		$this->tg->chat_botconf['deleteoriginalmessage']=$this->deleteoriginalmessage;
		$this->tg->chat_botconf['portalhuntsheet']=$this->portalhuntsheet;
		$this->tg->chat_botconf['sheetnames']=$this->sheetnames;
//		$this->tg->chat_botconf['sheetcount']=$this->sheetcount;
		$this->tg->chat_botconf['portaldatarange']=$this->portaldatarange;
		$this->tg->chat_botconf['starttime']=$this->starttime;
		$this->tg->chat_botconf['passcodesheetdata']=$this->passcodesheetdata;
		$this->tg->chat_botconf['autoinsertifempty']=$this->autoinsertifempty;
		$this->tg->chat_botconf['fstitle']=$this->fstitle;
		$this->tg->chat_botconf['fsdate']=$this->fsdate;
		$this->tg->chat_botconf['fslocation']=$this->fslocation;
		$this->tg->chat_botconf['fevgamesid']=$this->fevgamesid;
		$this->tg->chat_botconf['nextoldtime']=$this->nextoldtime;
		$this->tg->chat_writebotconf($chat_id);
	}
	
	function addportal($agent, $col, $row, $lat, $lng, $name, $chat_id, $message_id)
	{
		$erstellt=time();
		$sql="INSERT INTO ".$this->database_table_portale." SET chat_id=:chat_id, agent=:agent, row=:row, col=:col, lat=:lat, lng=:lng, name=:name, message_id=:message_id, erstellt=:erstellt";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->bindParam(':agent',$agent, PDO::PARAM_STR);
		$stm_all->bindParam(':row',$row, PDO::PARAM_INT);
		$stm_all->bindParam(':col',$col, PDO::PARAM_STR);
		$stm_all->bindParam(':lat',$lat, PDO::PARAM_STR);
		$stm_all->bindParam(':lng',$lng, PDO::PARAM_STR);
		$stm_all->bindParam(':name',$name, PDO::PARAM_STR);
		$stm_all->bindParam(':message_id',$message_id, PDO::PARAM_INT);
		$stm_all->bindParam(':erstellt',$erstellt, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
	}

	function getanzportal($chat_id=0)
	// Liefert die Anzahl Portale in der Queue
	{
		if($chat_id == 0) $chat_id=$this->tg->chat_id;
		if($chat_id >= 0) { // Fehlerhafte Chat-ID
			error_log(__FILE__.', '.__LINE__.' Fehlerhafte Chat-ID - exit');
			exit;
		}
		$sql="SELECT count(chat_id) FROM ".$this->database_table_portale." WHERE chat_id=:chat_id GROUP BY chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		if($stm_all->rowCount() == 1) {
			$result=$stm_all->fetch();
			return($result[0]);
		}
		return(0);
	}
	
	function getoldesttimeportal($chat_id=0)
	// Liefert 0, wenn kein Portal in Queue, sonst den Zeitstempel des letzten Portals
	{
		if($chat_id == 0) $chat_id=$this->tg->chat_id;
		if($chat_id >= 0) { // Fehlerhafte Chat-ID
			error_log(__FILE__.', '.__LINE__.' Fehlerhafte Chat-ID - exit');
			exit;
		}
		$sql="SELECT erstellt FROM ".$this->database_table_portale." WHERE chat_id=:chat_id ORDER BY erstellt LIMIT 1";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		if($stm_all->rowCount() == 1) {
			$result=$stm_all->fetch();
			return($result[0]);
		}
		return(0);
	}
	
	function getnextportal($chat_id=0)
	{
		if($chat_id == 0) $chat_id=$this->tg->chat_id;
		if($chat_id >= 0) { // Fehlerhafte Chat-ID
			error_log(__FILE__.', '.__LINE__.' Fehlerhafte Chat-ID - exit');
			exit;
		}
		$sql="SELECT * FROM ".$this->database_table_portale." WHERE chat_id=:chat_id ORDER BY id LIMIT 1";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		if($stm_all->rowCount() == 1) {
			$result=$stm_all->fetch();
			return($result);
		}
		return(false);
	}
	
	function deletequeue($chat_id=0)
	{
		$sql="DELETE FROM ".$this->database_table_portale." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		return(true);
	}
	
	function isInQueue($chat_id, $col, $row, $lat, $lng)
	{
		$sql="SELECT * FROM ".$this->database_table_portale." WHERE chat_id=:chat_id and col=:col and row=:row and lat=:lat and lng=:lng";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->bindParam(':col',$col, PDO::PARAM_STR);
		$stm_all->bindParam(':row',$row, PDO::PARAM_INT);
		$stm_all->bindParam(':lat',$lat, PDO::PARAM_STR);
		$stm_all->bindParam(':lng',$lng, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return($stm_all->rowCount());
	}
	
	function getportalbyid($id)
	 // Portaldaten bei DB-Id
	{
		$sql="SELECT * FROM ".$this->database_table_portale." WHERE id=:id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':id',$id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount() == 1) {
			$result=$stm_all->fetch();
			return($result);
		}
		return(false);
	}
	
	function deleteportalbyid($id)
	 // Portaldaten bei DB-Id
	{
		$sql="DELETE FROM ".$this->database_table_portale." WHERE id=:id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':id',$id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(true);
	}
	
	function deleteChat($chat_id)
	// Löscht alle Portale des Chats
	{
		$sql="DELETE FROM ".$this->database_table_portale." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(true);
	}
	
	function testOlderQueue($chat_id,$zeit)
	// Prüft, ob Portale vor der übergebenen $zeit in der Queue sind, gibt die Anzahl zurück
	{
		$sql="SELECT count(chat_id) FROM ".$this->database_table_portale." WHERE chat_id=:chat_id and erstellt < :zeit GROUP BY chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->bindParam(':zeit',$zeit, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount() == 0) return(0);
		$result=$stm_all->fetch();
		return($result[0]);
	}

}
	
?>