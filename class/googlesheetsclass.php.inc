<?php

/**
* Klasse für den Zugriff auf GoogleSheets
*
*	@author Jan Clauß
* @version 1.02 - 2021-04-16
* 
* Nach dem Beispiel: https://developers.google.com/sheets/api/quickstart/php
*
* Zum Erzeugen der token.json das Skript quickstart.php aus dem Beispiel nutzen, liegt auch noch mal
* unter /bsp/googlesheets. Pfade ggfls anpassen
* Die erzeugte Datei token.json muss für den Webserver rw sein
*/

class googlesheets
{
	public $authConfig='';
	public $tokenPath='';
	
	public $client=false; // Interne Speicherung des Clients
	public $service=false; // Interne Speicherung des Service
	public $lasterr='';
	
	public $logcachetime=5; // 5 Sekunden Caching für logausgaben
	public $logcachelastwrite=0; // Zeitpunkt des letzten schreibens
	public $logcache=array(); // Der Cache des Logs
	public $logrange='';
	public $logparams=array();
	public $logspreadsheetid='';


	/**
	* Constructor
	*
	* @param string $authConfig Pfad zur GooglAuthConfig
	* @param string $tokenPath Pfad zur Token.json
	*/
	function __construct($authConfig,$tokenPath)
	{
  	$this->authConfig=$authConfig;
  	$this->tokenPath=$tokenPath;
  	
  	$this->getClient();
  }

  function __destruct()
  {
  	$this->appendlogflush();
  }
  
  /**
	* Speichert die letzte Exception-Message in $this->lasterr oder false, wenn false übergeben wird
	* 
	* @param $ExceptionMessage Message des Exceptionhandlers, oder false wenn false übergeben wird
  */
  function setLastErr($ExceptionMessage)
  {
  	if($ExceptionMessage) {
  		$this->lasterr=json_decode($ExceptionMessage);
  		error_log(__FILE__.' '.__LINE__.': '.$this->lasterr->error->code.' - '.$this->lasterr->error->message);
  	}else{
  		$this->lasterr=false;
  	}
  }
  

  /**
  * Verbindung zur Google-API herstellen mit den Authentifizierungs informationen
  *
  */
  function getClient()
  {
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP FSNew-Bot');
	//    $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
    $client->setAuthConfig($this->authConfig);
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
	    // Load previously authorized token from a file, if it exists.
	    // The file token.json stores the user's access and refresh tokens, and is
	    // created automatically when the authorization flow completes for the first
	    // time.
    $tokenPath = $this->tokenPath;
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    // If there is no previous token or it's expired.
		if ($client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			} else {
				error_log(__FILE__.' '.__LINE__.': '.'Can not Refresh Token');
				return(false);
			}
			// Save the token to a file.
			if (!file_exists(dirname($tokenPath))) {
				mkdir(dirname($tokenPath), 0700, true);
			}
			file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		}
		$this->setLastErr(false);
		return $client;
	}
	

	/**
	* Daten in ein Sheet schreiben
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu schreibender Bereich z.B. 'B2:C4'
	* @param array $values Zweidimensionales Array mit den zu schreibenden Werten
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/
	function update($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$body = new Google_Service_Sheets_ValueRange([
    		'values' => $values
			]);
			$this->setLastErr(false);
			return($this->service->spreadsheets_values->update($spreadsheetId, $range, $body, $params));
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	

	/**
	* Daten an ein Sheet anfügen, Die Funktion richtet sich nach den vorherigen Daten, rückt also ggfls auch ein
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Der Bereich, in dem sich Daten befinden, an die die neuen Daten angefügt werden sollen
	* @param array $values Zweidimensionales Array mit den zu schreibenden Werten
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/
	function append($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$body = new Google_Service_Sheets_ValueRange([
    		'values' => $values
			]);
			$this->setLastErr(false);
			return($this->service->spreadsheets_values->append($spreadsheetId, $range, $body, $params));
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	
	/**
	* Daten an ein Sheet anfügen, Beginnt immer an der ersten Spalte aus $range und der ersten freien Zeile
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Der Bereich, in dem sich Daten befinden, an die die neuen Daten angefügt werden sollen
	* @param array $values Zweidimensionales Array mit den zu schreibenden Werten
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/	function appendlines($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		$this->setLastErr(false);
		// Als erstes die $range auslesen
		$rangearr=$this->decoderange($range);
		// Zeilen ermitteln
		$lines=$this->getRows($spreadsheetId, $range);
		// Prüfen, ob neue Zeile im Range-Bereich
		if($rangearr['row']['count'] <> -1 and $lines >= $rangearr['row']['count']) return(false);
		// Schreibbereich ermitteln
		$writerange=$rangearr['sheetstr'].$rangearr['column'][0].($rangearr['row'][0]+$lines).':'.$rangearr['column'][1].($rangearr['row'][0]+$lines+count($values));
		$values[]=[""]; // Leerzeile, um das Sheet bei Bedarf zu verlängern
		// Schreiben
		$ret=$this->update($spreadsheetId, $writerange, $values);
		return($ret);
	}
	
	/**
	* Daten an ein Sheet anfügen, Die Funktion richtet sich nach den vorherigen Daten, rückt also ggfls auch ein
	* Die Funktion Cached die Daten und schreibt sie nur alle $logcachetime Sekunden
	* Die letzte Ausgabe muss mit appendlogflush geschrieben werden.
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Der Bereich, in dem sich Daten befinden, an die die neuen Daten angefügt werden sollen
	* @param array $values Zweidimensionales Array mit den zu schreibenden Werten
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/	
	function appendlog($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		$this->logrange=$range;
		$this->logparams=$params;
		$this->logspreadsheetid=$spreadsheetId;
		foreach($values AS $valueline) {
			$this->logcache[]=$valueline;
		}
		if($this->logcachelastwrite < time()-$this->logcachetime) $this->appendlogflush();
		return(true);
	}
	
	/**
	* Schreibt die Daten von appendlog() in das sheet
	*/
	
	function appendlogflush()
	{
		if($this->logspreadsheetid<>'' and count($this->logcache) > 0) {
			$this->appendlines($this->logspreadsheetid, $this->logrange, $this->logcache, $this->logparams);
		}
		$this->logcache=array();
		$this->logcachelastwrite=time();
	}
	
	
	/**
	* Daten an ein Sheet anfügen
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Der Bereich, in dem sich Daten befinden, an die die neuen Daten angefügt werden sollen
	* @param array $values Array mit Name/Wert zuordnungen
	*    ist ein Wert 'idx' vergeben, wird dies als relative Spalte für den Wert benutzt
	*    ist ein Wert 'name' vergeben, wird dieser mit der ersten Zeile verglichen, und die entsprechende Spalte benutzt, oder eine neue Spalte erstellt
	*      ist zusätzlich 'idx' vergeben, wird der Name aktualisiert bzw. ergänzt
	*      'value' enthält den Wert, der am Ende der Tabelle angehängt wird, entsprechend zur Position 'idx'/'name'
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/
	function appendByNames($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		$this->setLastErr(false);
		// Als erstes die $range auslesen
		$rangearr=$this->decoderange($range);
		// Range für die Kopfzeilen
		$isrange=$rangearr['sheetstr'].$rangearr['column'][0].$rangearr['row'][0].':'.$rangearr['column'][1].$rangearr['row'][0];
		$ishead=$this->getRange($spreadsheetId,$isrange);
		if($ishead === false) return(false);
		$retarr['getRange']=$ishead;
		if(IsSet($ishead->values[0])) { // Sind keine Daten im Sheet gibt es kein values[0]
			$ishead=$ishead->values[0];
		}else{
			$ishead=array();
		}
		// Die übergebenen Werte prüfen, ob vorhanden, ggfls anlegen und eigentliche Position ermitteln
		$maxpos=0;
		foreach($values AS $value) {
			if(!isSet($value['value'])) $value['value']='';
			
			if(IsSet($value['idx'])) { // Zuordnung per Index
				if($value['idx'] > $maxpos) $maxpos=$value['idx'];
				if(isSet($value['name'])) { // Namen vergleichen, ergänzen / aktualisieren
					if(!IsSet($ishead[$value['idx']]) or $ishead[$value['idx']] <> $value['name']) {
						// Neuer oder geändeter Titel
						$ishead[intval($value['idx'])] = $value['name'];
						$newhead=true;
					}
				}
				$newvalues[$value['idx']] = $value['value'];
				
			}else{ // Zuordnung per Name
				if(isSet($value['name'])) {
					$pos=array_search($value['name'],$ishead);
					if($pos === false) { // Name noch nicht vergeben
						if(count($ishead) > $maxpos) $maxpos=count($ishead);
						$newhead=true;
						$newvalues[count($ishead)]=$value['value'];
						$ishead[count($ishead)]=$value['name'];
					}else{ // Name gefunden
						if($pos > $maxpos) $maxpos=$pos;
						$newvalues[$pos]=$value['value'];
					}
				}
			}
		}
		if(IsSet($newhead)) {
			// Namen ergänzen
			for($i=0; $i <= $maxpos; $i++) {
				if(!IsSet($ishead[$i])) $ishead[$i]='';
			}
			ksort($ishead);
			// Tabellenkopf aktualisieren
			$ret=$this->update($spreadsheetId, $isrange,[$ishead]);
			if($ret === false) return(false);
			$retarr['updateHead']=$ret;
		}
		if(IsSet($newvalues)) {
			// Werte ergänzen
			for($i=0; $i <= $maxpos; $i++) {
				if(!IsSet($newvalues[$i])) $newvalues[$i]='';
			}
			// Array sortieren
			ksort($newvalues);
			// Werte an Tabelle anfügen, plus eine Leerzeile, dadurch wird das Dokument beim erreichen der letzten Zeile automatisch erweitert
			$lines=$this->getRows($spreadsheetId, $range);
			if($rangearr['row']['count'] <> -1 and $lines >= $rangearr['row']['count']) return(false);
			$writerange=$rangearr['sheetstr'].$rangearr['column'][0].($rangearr['row'][0]+$lines).':'.$rangearr['column'][1].($rangearr['row'][0]+$lines+1);
			$ret=$this->update($spreadsheetId, $writerange,[$newvalues,['']]);
			if($ret === false) return(false);
			$retarr['updateValues']=$ret;
		}
		return($retarr);
	}
	
	/**
	* Zerlegt die $range und erzeugt ein Array, mit diversen indexen für die Range
	*
	* @param string $range A1:Z, C4:G7....
	*
	* @return array Rangeinfos
	* [
	*  'sheet' => Name des Sheets,
	*  'sheetstr => Name des Sheets mit !
	*  'row' => [
	*    0 => Beginn Zeile
	*    1 => Ende Zeile
	*    'start' => Beginn Zeilen
	*    'count' => Anzahl Zeilen, -1 wenn unbegrenzt
	*  ]
	*  'column' => [
	*    0 => Beginn Spalte (Buchstabe(n))
	*    1 => Ende Spalte (Buchstabe(n))
	*    'start' => Beginn Spalte (Dezimal)
	*    'count' => Anzahl Spalten, -1 wenn unbegrenzt
	*  ]
	* ]
	*/
	function decoderange($range)
	{
		if(!preg_match('/(?>([a-z_0-9 ]+)!)?([a-z]+)([0-9]+):([a-z]*)([0-9]*)/i',$range,$arr)) {
			return(false);
		}
		$retrange['sheet']=$arr[1];
		$retrange['sheetstr']=$arr[1].'!';
		$retrange['row'][0]=$arr[3];		
		$retrange['row'][1]=$arr[5];
		$retrange['column'][0]=strtoupper($arr[2]);
		$retrange['column'][1]=strtoupper($arr[4]);
		$retrange['row']['start']=$arr[3];
		if($arr[5] == '') {
			$retrange['row']['count'] = -1;
		}else{
			$retrange['row']['count'] = $arr[5]-$arr[3]+1;
		}
		$retrange['column']['start'] = $this->columnDez($arr[2]);
		if($arr[4] == '') {
			$retrange['column']['count'] = -1;
		}else{
			$retrange['column']['count'] = $this->columnDez($arr[4])-$this->columnDez($arr[2]);
		}
		return($retrange);
	}
	
	/**
	* Berechnet aus der Spaltenbezeichnung den Integerwert der Position
	*
	* @param string $column Spaltenname (A, AA, AZ)
	*
	* @return integer Nummer der Spalte
	*/
	function columnDez($column)
	{
		$column=strtoupper(trim($column));
		if($column == '') return(-1);
		$count=0;
		if(strlen($column) > 1) {
			// Mehr als ein Zeichen (z.B. 'AC')
			$count=$this->columnDez(substr($column,0,strlen($column) -1))*26;
		}
		$count=ord(substr($column,strlen($column)-1))-64+$count;
		return($count);
	}
	
	/**
	* Berechnet aus dem Integerwert der Position die Spaltenbezeichnung
	*
	* @param integer $count Spaltennummer
	*
	* @return string Spaltenbezeichnung
	*/
	function dezColumn($count)
	{
		if($count <= 0) return('');
		$column='';
		if($count>26) {
			// Mehrstellige Spaltenbezeichnung
			$column=$this->dezColumn(intval($count/26));
			$count=$count-intval(intval($count/26)*26);
		}
		$column.=chr($count+64);
		return($column);
	}
	
	/**
	* Daten in ein Sheet schreiben, Bitte update-Funktion nutzen
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu schreibender Bereich z.B. 'B2:C4'
	* @param array $values Zweidimensionales Array mit den zu schreibenden Werten
	* @param array $params Parameter
	*
	* @return array Updateinfos
	*/
	function writesheet($spreadsheetId, $range, $values, $params = array('valueInputOption' => 'USER_ENTERED'))
	{
		error_log(__FILE__.' '.__LINE__.': Ersetze writesheet durch update');
		return($this->update($spreadsheetId, $range, $values, $params));
	}
	

	/** 
	* Daten aus einem Sheet lesen
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu schreibender Bereich z.B. 'B2:C4'
	*
	* @return Google_Service_Sheets_ValueRange-Object (Daten in ->values)
	*/
	function getRange($spreadsheetId,$range)
	{
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$result = $this->service->spreadsheets_values->get($spreadsheetId, $range);
			$this->setLastErr(false);
			return($result);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	

	/** 
	* Daten aus einem Sheet in ein Array lesen
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu schreibender Bereich z.B. 'B2:C4'
	*
	* @return Zweidimensionales Array
	*/
	function getRangeArr($spreadsheetId,$range)
	{
		$ret=$this->getRange($spreadsheetId,$range);
		if(is_object($ret)) {
			return($ret->values);
		}
		return($ret);
	}


	/**
	* Anzahl der belegten Zeilen eines Bereiches ermitteln
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu schreibender Bereich z.B. 'B2:C4'
	*
	* @return integer Anzahl Zeilen
	*/
	function getRows($spreadsheetId,$range) {
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$result = $this->service->spreadsheets_values->get($spreadsheetId, $range);
			$numRows = $result->getValues() != null ? count($result->getValues()) : 0;
			$this->setLastErr(false);
			return($numRows);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	

	/**
	* Löscht einen Bereich eines Sheets
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $range Zu löschender Bereich z.B. 'B2:C4'
	*
	* @return string json Propertys
	*/
	function batchClear($spreadsheetId,$range) {
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$requestBody = new Google_Service_Sheets_BatchClearValuesRequest();
			$requestBody->setRanges($range);
			$response = $this->service->spreadsheets_values->batchClear($spreadsheetId, $requestBody);
			$this->setLastErr(false);
			return($response);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	

	/**
	* Titel eines Sheets ermitteln
	*
	* @param string $spreadsheetId ID des Sheets
	*
	* @return string Titel des Sheets
	*/
	function getTitle($spreadsheetId) {
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$sheet_metadata = $this->service->spreadsheets->get($spreadsheetId)->properties->title;
			$this->setLastErr(false);
			return($sheet_metadata);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	

	/**
	* Alle Properties eines Sheets ermitteln
	*
	* @param string $spreadsheetId ID des Sheets
	*
	* @return string json Propertys
	*/
	function getAll($spreadsheetId) {
		try {
			if(!is_object($this->client)) $this->client=$this->getClient();
			if(!is_object($this->service)) $this->service=new Google_Service_Sheets($this->client);
			$sheet_metadata = $this->service->spreadsheets->get($spreadsheetId);
			$this->setLastErr(false);
			return($sheet_metadata);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	
	/**
	* Alle Sheets eines Spreadsheet ermitteln 
	*
	* @param string $spreadsheetId ID des Sheets
	*
	* @return Array(id,title)
	*/
	function getSheetsTitles($spreadsheetId) {
		if(!($alldata=$this->getAll($spreadsheetId))) return(false);
		$sheetlist=$alldata->getSheets();
		foreach($sheetlist AS $sheetRecord) {
			$sheetarr[]=array('title' => $sheetRecord['properties']['title'], 'id' => $sheetRecord['properties']['sheetId']);
		}
		return($sheetarr);
	}
	
	/**
	* die ID eines einzelnen Sheets ermitteln 
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $title Name/Titel des Sheets
	*
	* @return integer sheetid
	*/
	function getSheetId($spreadsheetId,$title) {
		if(!($sheetarr=getSheetsTitles($spreadsheetId))) return(false);
		foreach($sheetarr AS $sheetRecord) {
			if($sheetRecord['title'] == $title) return($sheetRecord['id']);
		}
		return(false);
	}
	
	/**
	* ein Sheet umbenennen 
	*
	* @param string $spreadsheetId ID des Sheets
	* @param string $sheet alter Titel des Sheets, oder die ID, wenn $id=true
	* @param string $newtitle ID des Sheets
	* @param integer $id if thrue the $sheet is the SheetId not the SheetTitle
	*
	* @return integer sheetid
	*
	* Beispiel auf https://gist.github.com/oskarijarvelin/ba39ed1687237ef85aa0548865ce4760
	*/
	function renameSheet($spreadsheetId, $sheet, $newtitle, $id=false) {
		if($id == false) $sheet=getSheetId($spreadsheetId,$sheet);
		if($sheet == false) return(false);
//		error_log("$sheet $newtitle");
		try {
			$requests = [
				new Google_Service_Sheets_Request([
					'updateSheetProperties' => [
						'properties' => [
							'sheetId' => $sheet,
							'title' => $newtitle
						],
						'fields' => 'title'
					]
				])
			];
			$sheetRenameTab = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(['requests' => $requests]);
			$sheetResult = $this->service->spreadsheets->batchUpdate($spreadsheetId,$sheetRenameTab);
			return(true);
		} catch (Exception $e) {
			$this->setLastErr($e->getMessage());
			return(false);
		}
	}
	
	function getSpreadSheetShareLink($spreadsheetId)
	{
		return 'https://docs.google.com/spreadsheets/d/'.$spreadsheetId.'/edit?usp=sharing';
		// https://docs.google.com/spreadsheets/d/sheetid/edit?usp=sharing
	}
}
?>