<?php

/**** Klasse für die Abfrage der Fevgames-Nutzer
*
*	@Author: Jan Clauß
* @Version: 1.01 2021-02-21
* 
*/

class fevgames
{
	public $fevgamespath='https://fevgames.net/ifs/event/?e=';
	public $fevgamesid=0;
	public $minimumagents=0; // Bei weniger gefunden Agents wird die Abfrage als Fehler (false) zurück gegeben
	public $enlanz=0;
	public $resanz=0;
	public $agents=array();
	public $cachingpath='';
	public $caching=true;
	public $cachingtime=300; // 5 Minuten; Ist $cachingtime=0 wird immer der Cache genutz, benötigt ein Manuelles Laden mit getdata(true)
	public $lastread=0; // Timestamp
	
	public $fsdata=array();
	
	public $curl_info=array();

	function __construct($fevgamesid) {
		$this->fevgamesid=intval($fevgamesid);
		if(defined('FEVGAMESPATH')) $this->fevgamespath=FEVGAMESPATH;
	}
	
  /**
	* Ruft die FS-Daten von Fevgames ab
	* 
  */
	function getfsdata()
	{
		if($this->fevgamesid == 0) return(false); // keine ID eingestellt
		$filename=$this->fevgamespath.$this->fevgamesid;
		$ch = curl_init();     
    curl_setopt($ch, CURLOPT_URL, $filename);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
	  $result = curl_exec($ch); 
	  $this->curl_info=curl_getinfo($ch);
	  if($result === false or (IsSet($this->curl_info['http_code']) and $this->curl_info['http_code'] <> 200 )) {
		  error_log(__FILE__.', Line:'.__LINE__.' - '.json_encode($this->curl_info));
		  return(false);
		}
		curl_close($ch); 
	// Titel: <h2>#IngressFS - Dessau-Roßlau, Germany - January 2021</h2>
		if(preg_match('/<h2>#IngressFS - (.*?) - [a-z]+ \d{4}<\/h2>/i',$result,$arr)) {
			$this->fsdata['title']=$arr[1];
		}
		
	// Zeitpunkt: Date/Time: <span class="info">01-Aug-2020 04:00 pm</span>
	// Date/Time: <span class='info'>03-Dec-2022 03:00 pm UTC +1</span> ab 2022-12-02
		if(preg_match('/Date\/Time:\s+<span\s+class=.info.>(.*?)<\/span>/',$result,$arr)) {
			if(trim($arr[1]) == '') return(false);
			$arr[1]=substr($arr[1],0,strpos($arr[1],'UTC'));
			$this->fsdata['time_str']=$arr[1];
			$this->fsdata['timestamp']=strtotime($arr[1]);
			$this->fsdata['date']=date('Y-m-d',$this->fsdata['timestamp']);
			$this->fsdata['time']=date('H:i:s',$this->fsdata['timestamp']);
		}
		
	// Registration Portal: <span class="info">LOVE</span> 
                // <a target='_blank' href='https://intel.ingress.com/?pll=49.496577,8.47831'>Intel Link</a>
		if(preg_match('/Registration Portal: <span class=\"info\">(.*?)<\/span>.*?ll=([-0-9.]+),([-0-9.]+)/si',$result,$arr)) {
			$this->fsdata['regport']['name']=$arr[1];
			$this->fsdata['regport']['lat']=$arr[2];
			$this->fsdata['regport']['lng']=$arr[3];
		}
		// Meeting Portal
		
		// Restocking Portal
		
		// Event-Channel
		if(preg_match('/Event\s+Channel:\s+<a\s+href=\'(https:\/\/t\.me\/[a-z0-9]+)\'>t\.me\/[a-z0-9]+<\/a>/i',$result,$arr)) {
			$this->fsdata['eventchannel']=$arr[1];
		}
		
		// Return False, wenn keine Daten
		if(count($this->fsdata) == 0) return(false);
		// Ansonsten FS-ID und Zeitpunkt mit einfügen
		$this->fsdata['import']['time']=time();
		$this->fsdata['import']['id']=$this->fevgamesid;
		return(true);
	}
	
	function extractFevAgents($list, $faction)
	{
		preg_match_all('/([a-z0-9_#+@]+)\s?(<br\/>)?/i',$list,$arr);
		foreach($arr[1] AS $agent)
		{
			if(preg_match('/^[a-z0-9_#+@]+$/i', $agent)) {
				$this->agents[$agent]=array('name' => $agent, 'faction' => $faction);
			}
		}
	}
	
  /**
	* Ruft die Agents von Fevgames ab
	* 
	* @param boolean $force Ruft die Daten von Fevgames ab, auch wenn der Cache aktiv ist
  */
	function getdata($force=false)
	{
		if($this->fevgamesid == 0) return(false); // keine ID eingestellt
		$cachfile=$this->cachingpath."fevgamescache_".$this->fevgamesid.".json";
		if($this->caching == true and $this->cachingpath <> '' and $force == false) { // Caching Aktiv -> Cache lesen
			if(count($this->agents) == 0) { // Cache der Klasse leer
//			if($this->lastread < time()-$this->cachingtime) { // In dieser Session noch nicht geladen oder älter als $cachingtime
				$this->agents=array();
				if(file_exists($cachfile)) { // Cache-File vorhanden
					$cachejson=file_get_contents($cachfile);
					$cachearr=json_decode($cachejson,true);
//					error_log(__FILE__.', '.__LINE__.', '.(time()-$cachearr['lastread']));
					if(IsSet($cachearr['lastread']) and IsSet($cachearr['agents']) and ($cachearr['lastread'] >= time()-$this->cachingtime or $this->cachingtime == 0)) { // Array OK und Alter hat Cachingtime noch nicht überschritten
						$this->agents = $cachearr['agents'];
						$this->resanz=$cachearr['resanz'];
						$this->enlanz=$cachearr['enlanz'];
					}
				}
			}
		}
		if(count($this->agents) == 0) {
			$filename=$this->fevgamespath.$this->fevgamesid;
			$ch = curl_init();     
	    curl_setopt($ch, CURLOPT_URL, $filename);  
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
	    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
	    $result = curl_exec($ch);  
		  $this->curl_info=curl_getinfo($ch);
	  	if($result === false or (IsSet($this->curl_info['http_code']) and $this->curl_info['http_code'] <> 200 )) {
			  error_log(__FILE__.', Line:'.__LINE__.' - '.json_encode($this->curl_info));
		  	return(false);
			}
	    curl_close($ch); 

			// ENL-Leader
			//<a class="enl" href="https://t.me/enlname">enlname</a>
			if(preg_match('/class=\"enl\" href=\"(.*?)\">([a-z0-9]+)\s*<\/a>/i',$result,$arr))
			{
				$this->agents[$arr[2]]=array('name' => $arr[2], 'typ' => 'enllead', 'faction' => 'enl', 'link' => $arr[1]);
			}
			// RES-Leader
			//<a class="res" href="https://t.me/resname">resname</a>
			if(preg_match('/class=\"res\" href=\"(.*?)\">([a-z0-9]+)\s*<\/a>/i',$result,$arr))
			{
				$this->agents[$arr[2]]=array('name' => $arr[2], 'typ' => 'reslead', 'faction' => 'res', 'link' => $arr[1]);
			}
			//ENL Agents
			if(preg_match('/<h4>Enl Attendees \((\d+)+\)<\/h4>\s+(([a-z0-9_#+@]+\s?(<br\/>)?)+)/im',$result,$arr)) // _#+@ alles Zeichen, die schon mal mit eingegeben wurden, und nicht zum Abbruch führen sollen
			{
				$this->enlanz = $arr[1];
				$this->extractFevAgents($arr[2],'enl');
			}
			//RES Agents
			if(preg_match('/<h4>Res Attendees \((\d+)+\)<\/h4>\s+(([a-z0-9_#+@]+\s?(<br\/>)?)+)/im',$result,$arr))
			{
				$this->resanz = $arr[1];
				$this->extractFevAgents($arr[2],'res');
			}
			if($this->cachingpath <> '') {
				error_log(date('Y-m-d H:i:s ').$this->fevgamesid." Lese von Fevgames RES: ".$this->resanz.", ENL: ".$this->enlanz.", Count: ".count($this->agents)."\n",3,$this->cachingpath.'fevgames.log');
			}
			if(count($this->agents) < $this->minimumagents) return(false); // Zu wenig Agents gefunden, fehlerhafte Abfrage
			if($this->cachingpath <> '') {
				error_log("Cache geschrieben\n",3,$this->cachingpath.'fevgames.log');
			}
			$this->lastread=time();
			if($this->caching == true and $this->cachingpath <> '') { // Caching Aktiv -> Cache schreiben
				$cachejson=json_encode(array('lastread' => time(), 'agents' => $this->agents, 'resanz' => $this->resanz, 'enlanz' => $this->enlanz));
				file_put_contents($cachfile,$cachejson);
			}
		}
	  return(true);
  }
}

?>