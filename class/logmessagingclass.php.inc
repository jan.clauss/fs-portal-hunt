<?php

/**** Klasse zur Ausgabe von Logs, Benachrichtigung von OPs, MODs, Admins, vie Telegram(gruppen)
*
* @Author: Jan Clauß
* @Version: 1.0 (2021-04-14)
* 
*/

define('DEBUG_FILE_ROOT', (empty($DOCUMENT_ROOT) ? dirname(dirname(__FILE__)) : str_replace("/", DIRECTORY_SEPARATOR,
        dirname($DOCUMENT_ROOT))) . DIRECTORY_SEPARATOR);

class logmessaging
{
	
	public $tg=null; // Telegramklasse für Alle Telegramgebundenen Ausgaben und zum Ermitteln der Telegram MODs, der Benutzer... Ist keine Klasse übergeben, wird nur in File geschrieben, so gesetzt.
	public $bot_owner='';
	public $normalchat=0; // Chat an den die Meldungen gesendet werden. Ist er gesetzt, werden keine Nachrichten an die MODs gesendet
	public $globalchat=0; // Globaler Chat, an den immer gesendet wird, unabhängig von $normalchat
	public $replacestrings=array(); // Array mit Strings, die in der endgültigen Ausgabe ersetzt werden sollen array(array('search' => Searchstring, 'replace' => Replacestring))
	public $logdir=''; // Pfad zu einem Log-Dir. Ist er gesetzt, werden die Meldungen in Logdateien unter diesem Pfad geschrieben
	public $loglevel='error'; // Was soll ins Logdir geschrieben werden. Ein Eintrag aus $main_array oder $error_array
	public $fh=null;  // Interne Speicherung des Filehandlers 'main'
	public $fe=null;  // Interne Speicherung des Filehandlers 'error'
	public $externalid=''; // Eine Zeichenkette, die in die Dateinamen aufgenommen wird
	public $sendtgusername=false; // Ist true und IsSet($tg->username) wird der Username mit in die Nachricht aufgenommen
	
	public $modlist=null; // Enthält eine Liste mit TG-IDs als Empfänger. Ist sie NULL, wird $tg->getmods() verwendet ist sie =array(), werden keine privaten Nachrichten versandt
	
	public $main_array = array(
			'notice',
			'info',
			'warning',
			'debug',
	);

	public $error_array = array(
			'error',
			'sql-error',
			'sheets-error',
			'critical',
	);
	
	public $messagedepth=0; // Speichert die Aufrufe von $tg->sendmessage/$tg->editreplymarkup um recursive Aufrufe zu vermeiden
	public $maxmessagedepth=3; // Maximale Tiefe der Aufrufe
	
	private $htmlescaped=array('&lt;code&gt;','&lt;/code&gt;','&lt;a&gt;','&lt;/a&gt;','&lt;b&gt;','&lt;/b&gt;');
	private $htmlnormal =array( '<code>', '</code>', '<a>', '</a>', '<b>', '</b>');
	
	private static $instance = null;  // Instance auf sich selbst

	/**
	* getInstance
	*
	* Lässt die Klasse sich selbst erstellen, wenn sie noch nicht existiert, sonst liefert sie die Instance zurück
	*
	* Aufruf $obj = logmessaging::getInstance(...)
	*
	* @param object $tg - Telegramclass-Objekt
	* @param integer $normalchat - Eine Chat-ID, die die Meldungen an die MODs ersetzt 0 = aus
	* @param integer $globalchat - Eine Chat-ID, an die die Meldungen immer gehen (Als Sammelgruppe für mehrere Bots) 0 = aus
	* @param string $logdir - Ein Pfad, in den Logfiles erstellt werden sollen '' = aus
	*/
	public static function getInstance()
	{
		if (!isset(self::$instance))
		{
			$object=__CLASS__;
			self::$instance = new $object;
		}
		return self::$instance;
	}
	
	/**
	* SetLogDir
	*
	* Setzt das Logginverzeichnis
	*
	* @param string $logdir
	*/
	public function setlogdir($logdir)
	{
		if($logdir <> '') {
			if(substr($logdir,-1) <> '/') $logdir.='/'; // Slash am Ende ergänzen
			$this->logdir=$logdir;
		}
	}

	/**
	* output
	*
	* Ausgabe einer Meldung in Kurzform
	*
	*/
	public function output($token, $text)
	{
		$this-> codeoutput('','','notice',$token,$text);
	}
	
	/**
	* codeoutput
	*
	* Ausgabe einer Meldung mit Codedetails / Übergabe von __FILE__, __LINE__ ...
	*
	*/
	public function codeoutput($file, $line, $level, $token, $text, $logfile='main')
	{
		if(!is_string($text)) $text=print_r($text,true);
		if($this->sendtgusername and IsSet($this->tg->username)) $text='@'.$this->tg->username.': '.$text;
		$this->codeoutputtg($file, $line, $level, $token, $text, $logfile);
		$this->codeoutputfile($file, $line, $level, $token, $text, $logfile);
	}
	
	/**
	* codeoutputtg
	*
	* Routine für TG- und Logfile Benachrichtigungen
	*
	*/
	public function codeoutputtg($file, $line, $level, $token, $text, $logfile)
	{
		// Prüfen, ob Telegramclass initialisiert ist, wenn nicht keine Telegram-Benachrichtigungen
		if(!IsSet($this->tg)) return(false);
		
		// Ziele für Nachricht ermitteln
		if($this->normalchat < 0) { // Nachrichten in Gruppe, dann gehen keine an die MODs
			$send[]=$this->normalchat;
		}else{ // Alle MODs erfassen
			if(IsSet($this->modlist)) { // Interne MOD-Liste verwenden
				$send=$this->modlist;
			}else{
				$mods=$this->tg->getmods();
				if($mods['ok'] <> false and IsSet($mods['mods'])) { // Werden keine Mods gefunden, können wir das hier beenden.
					$send=$mods['mods'];
				}
			}
		}
		if($this->globalchat <> 0) { // Eine Extra Gruppe oder extra User, an den die Nachrichten gehen
			$send[]=$this->globalchat;
		}
		// $file, $line, $level ergänzen
		$codetext='';
		$file = str_replace(DEBUG_FILE_ROOT, '', $file);
		if($file <> '') $codetext.=$file;
		if($file <> '' and $line <> '') $codetext.=':';
		if($line <> '') $codetext.=$line;
		if($file <> '' and $line <> '') $codetext.="\n";
		if($level <> '') $codetext.="$level\n";
		
		// Zu ersetzende Text suchen
		foreach($this->replacestrings AS $replacestr) {
			$text=str_replace($replacestr['search'], $replacestr['replace'], $text);
			$codetext=str_replace($replacestr['search'], $replacestr['replace'], $codetext);
		}
		
		// Nachricht an Empfänger senden
		foreach($send AS $stgid) {
			if($stgid < 0) $atgid='g'.abs($stgid); else $atgid='u'.$stgid;
			$logmessaging=$this->tg->getsqlbotconf('logmessaging', $atgid, false);
			if(!IsSet($logmessaging[$token]['time']) or $logmessaging[$token]['time'] < time()) {
				if(IsSet($logmessaging[$token]['lastmessageid'])) {
					$this->editMessageReplyMarkup($stgid, $logmessaging[$token]['lastmessageid'],json_encode(array('inline_keyboard'=>array())));
					unset($logmessaging[$token]['lastmessageid']);
					$this->tg->setsqlbotconf('logmessaging', $atgid, $logmessaging, false);
				}
				$configarray=array();  // Zurücksetzen von der vorherigen TG-ID
				$configarray[]=array(array('text' => '0.5h', 'callback_data' => "logmessage $token $atgid 1800"),
														 array('text' => '1h', 'callback_data' => "logmessage $token $atgid 3600"),
												 		array('text' => '2h', 'callback_data' => "logmessage $token $atgid 7200"),
												 		array('text' => '4h', 'callback_data' => "logmessage $token $atgid 14400"));
				$configarray=array('inline_keyboard'=>$configarray);
				
				$escapetext=$this->tg->escapestr($text);
				$escapetext=str_replace($this->htmlescaped, $this->htmlnormal, $escapetext); // Geschützte HTML-Codes wieder umwandeln
				$outputtext="[$token] - $escapetext";
				if($stgid == $this->bot_owner or $stgid == $this->globalchat) $outputtext=$codetext."[$token] - $escapetext"; // Nur der Botadmin und die Globalgruppe bekommen file-Infos
				$answer=array(
					'text' => $outputtext,
					'reply_markup' => json_encode($configarray)
				);
				$result=$this->sendmessage($stgid,$answer);
		  	if(IsSet($result['ok']) and $result['ok'] == true) {
					$logmessaging[$token]['lastmessageid']=$result['result']['message_id'];
					$this->tg->setsqlbotconf('logmessaging', $atgid, $logmessaging, false);
				}else{
//					$this->sendmessage($this->bot_owner, print_r($result,true));
// Das hat zu reichlich Problemem geführt, sobald zu viele Nachrichten gesendet wurden.
				}
			}
		}
//		$this->dbwrite();
		
	}
	
	/**
	* codeoutputtg
	*
	* Routine für Logfiles
	*
	*/
	public function codeoutputfile($file, $line, $level, $token, $msg, $logfile)
	{
		if($this->logdir == '') return(true);
		$logfile = $logfile . "-" . date("Y-m-d") . "_".$this->externalid."_".getmypid().".log";

		if (!IsSet($this->fh[$logfile]) and in_array($level, $this->main_array) and in_array($this->loglevel,$this->main_array)) {
			$this->fh[$logfile] = fopen($this->logdir.$logfile, "a");
		}
		if (!IsSet($this->fe)) {
			$this->fe = fopen($this->logdir.'error.log', "a");
		}

		$shortfile = str_replace(DEBUG_FILE_ROOT, '', $file);

		//Main.log
		if (in_array($level, $this->main_array) and in_array($this->loglevel,$this->main_array)) {
			$logline=date("d.m.y H:i:s") . "  [" . $level . "] /* FILE: $shortfile, $line */  " . $this->formatlogmsg($msg) . "\n";
			fwrite($this->fh[$logfile],$logline);
		}

		//error.log
		if (in_array($level, $this->error_array)) {
			$logline = date("d.m.y H:i:s") . "  [" . $level . "] /* FILE: $shortfile, $line */   " . $this->formatlogmsg($msg) . "\n";
			fwrite($this->fe,$logline);
		}
	}

	public function formatlogmsg($msg)
	{
		if (is_string($msg)) {
			return $msg;
		} elseif (is_array($msg)) {
			return print_r($msg,true);
		} elseif (is_object($msg)) {
			return print_r($msg,true);
		} else {
			return $msg;
		}
	}
	
	public function outputpause($tgres)
	{
		// Einbinden in case 'callback_query':
		//		case 'logmessage':
		//			$logmess->outputpause($tgres);
		//		break;
		$token=$tgres['args'][0];
		$atgid=$tgres['args'][1];
		if($atgid[0] == 'g') $stgid=intval('-'.substr($atgid,1)); else $stgid=intval(substr($atgid,1));
		$logmessaging=$this->tg->getsqlbotconf('logmessaging', $atgid, false);
		$logmessaging[$token]['time']=time()+intval($tgres['args'][2]);
//			$this->sendtomod('debug',date('H:i:s',time()).'  '.date('H:i:s',$this->loggingarr[$atgid][$token]['time']));
		$this->editMessageReplyMarkup($stgid, $tgres['message_id'],json_encode(array('inline_keyboard'=>array())));
		unset($logmessaging[$token]['lastmessageid']);
		$this->tg->setsqlbotconf('logmessaging', $atgid, $logmessaging, false);
	}
	
	/**
	* editMessageReplyMarkup
	* 
	* Eigene Funktion für die selbe Funktion in telegramclass aufruft, aber einen Recursionszähler/-begrenzer implementiert
	*/
	public function editMessageReplyMarkup($user_id, $message_id, $reply_markup='')
	{
		if($this->messagedepth < $this->maxmessagedepth) { // Recursion vermeiden
			$this->messagedepth++;
			$result=$this->tg->editMessageReplyMarkup($user_id, $message_id, $reply_markup);
			$this->messagedepth--;
			return($result);
		}
		return false;
	}
	
	/**
	* sendmessage
	* 
	* Eigene Funktion für die selbe Funktion in telegramclass aufruft, aber einen Recursionszähler/-begrenzer implementiert
	*/
	public function sendmessage($user, $message, $othermessagelimit = 0)
	{
		if($this->messagedepth < $this->maxmessagedepth) { // Recursion vermeiden
			$this->messagedepth++;
			$result=$this->tg->sendmessage($user, $message, $othermessagelimit);
			$this->messagedepth--;
			return $result;
		}
		return false;
	}
}


?>
