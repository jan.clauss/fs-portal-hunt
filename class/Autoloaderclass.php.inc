<?php

class Autoloader {
	static public function loader($className) {
		global $bot_root;
		$mainclassName=preg_replace('/(_[a-z0-9]+)$/i','',$className);
		$filename = $bot_root.'class/' . str_replace('\\', '/', $mainclassName) . 'class.php.inc';
		if (file_exists($filename)) {
			include_once($filename);
			if (class_exists($className)) {
				return TRUE;
			}
		}
		return FALSE;
	}
}
spl_autoload_register('Autoloader::loader');

?>