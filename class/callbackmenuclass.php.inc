<?php

/**** Klasse für Callback-Menüs
*
*	@Author: Jan Clauß
* @Version: 1.041 (2021-04-05)
*
* 1.041 Funktionsdoku und Fehlerkorrektur Stunden
* 
*/

class callbackmenu
{

public $tg=null;
public $tl=null;
public $monate="Jan\nFeb\nMär\nApr\nMai\nJun\nJul\nAug\nSep\nOkt\nNov\nDez"; // Token: cbm_monate

	function __construct($tg, $tl=null)
	{
		$this->tg=$tg; // Telegramclass
		$this->tl=$tl; // Translateclass
	}
	
	/**
	* function getstr()
	*
	* Diese Funktion nutzt die translate-class, wenn vorhanden und wenn das Token existiert, sonst default
	*
	* @param string $token - Der Token für den Text
	* @param array $args - Argumente, die übergeben werden sollen ([#1#]...)
	* @param string $default - Der default- String wenn die Klasse nicht eingebunden oder das Token nicht vorhanden ist.
	*/
	function getstr($token,$args,$default)
	
/* Array für die languageger.php.inc
$languageger['cbm_monate']=array('date' => '2021-01-02', 'desc'=>'callbackmenuclass: Monate Aufzählung kurz Zeilenweise', 'text'=>"Jan\nFeb\nMär\nApr\nMai\nJun\nJul\nAug\nSep\nOkt\nNov\nDez");
$languageger['cbm_jetzt']=array('date' => '2021-01-02', 'desc'=>'callbackmenuclass: Jetzt', 'text'=>"Jetzt:");
$languageger['cbm_waehle']=array('date' => '2021-01-02', 'desc'=>'callbackmenuclass: Wähle', 'text'=>"Wähle:");
*/

	{
		if(!is_object($this->tl)) { // Object nicht initialisiert
			return($default);
		}
		if(!$this->tl->isstr($token)) { // Token existiert nicht
			return($default);
		}
		return($this->tl->getstr($token,$args,$default));
	}

	/**
	* function zeit()
	*
	* Stellt ein Callback Menü zur Eingabe der Zeit bereit
	* 
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param integer $zeit - Ausgangszeitstempel (Timestamp)
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	function zeit($callbackstr, $tgres, $zeit=0, $kommandidx=1)
	// Zeitauswahl
	{

		$monate=explode("\n", $this->getstr('cbm_monate',[],$this->monate));
		
		if(IsSet($tgres['args'][$kommandidx+1])) { // von sich selber aufgerufen, zeit als args
			$zeit=intval($tgres['args'][$kommandidx+1]);
			if(IsSet($tgres['args'][$kommandidx+2]) and $tgres['args'][$kommandidx+2] == 'ret') { // Beenden, DatumZeit übergeben
				return($zeit);
			}
		}
		$answer=$this->getstr('cbm_waehle',[],'Wähle: ');

		if($zeit == 0) $zeit=time();
		$datetab='date';
		if(IsSet($tgres['args'][$kommandidx]) and in_array($tgres['args'][$kommandidx],['month', 'hour', 'min'])) $datetab=$tgres['args'][$kommandidx];

		$configarray[]=array(array('text' => $this->getstr('cbm_jetzt',[],'Jetzt: ').date($this->getstr('datetime_time',[],'Y-m-d'),time()), 'callback_data' => "$callbackstr hour ".time()));
		$hourmarker='';
		$minmarker='';
		switch($datetab) {
			case 'min':
				$minmarker='_';
			break;
			default:
				$hourmarker='_';
			break;
		}
		$configarray[]=array(
											array('text' => $hourmarker.date('H:', $zeit).$hourmarker, 'callback_data' => "$callbackstr hour $zeit"),
											array('text' => $minmarker.date('i', $zeit).$minmarker, 'callback_data' => "$callbackstr min $zeit")
										);
		// Aktuelle Werte zerlegen
		$year=intval(date('Y', $zeit));
		$month=intval(date('m', $zeit));
		$day=intval(date('d', $zeit));
		$hour=intval(date('H', $zeit));
		$min=intval(date('i', $zeit));
		
		switch($datetab) {
			case 'min':
				for($i=0; $i < 10; $i++) {
					$configarray1=array();
					for($j=1; $j < 7; $j++) {
						$imin=$i*6+$j-1;
						if($imin == $min) $imin='_'.$min.'_';
						$configarray1[]=array('text' => $imin, 'callback_data' => "$callbackstr min ".strtotime("$year-$month-$day $hour:$imin"));
					}
					$configarray[]=$configarray1;
				}
			break;
			default: //case 'hour':
				for($i=0; $i < 4; $i++) {
					$configarray1=array();
					for($j=0; $j < 6; $j++) {
						$ihour=$i*6+$j;
						if($ihour == $hour) $ihour='_'.$hour.'_';
						$configarray1[]=array('text' => $ihour, 'callback_data' => "$callbackstr hour ".strtotime("$year-$month-$day $ihour:$min"));
					}
					$configarray[]=$configarray1;
				}
			break;
		}
		
		$selectedtime=str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($min,2,'0',STR_PAD_LEFT);
		$configarray[]=array(array('text' => $this->getstr('cbm_ok',[],'Übernehmen: ').$selectedtime, 'callback_data' => "$callbackstr date ".strtotime("$year-$month-$day $hour:$min").' ret'));
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}  //function zeit()

	/**
	* function datum()
	*
	* Stellt ein Callback Menü zur Eingabe des Datums bereit
	* 
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param integer $datum - Ausgangszeitstempel (Timestamp)
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	function datum($callbackstr, $tgres, $datum=0, $kommandidx=1)
	{
		/**
		* Datum
		* Jahr-1 | Jahr | Jahr+1
		* Monat-2    | Monat-1    | Monat    | Monat+1    |    Monat+2
		* Montag  | Dienstag | Mittwoch | Donnerstag | Freitag | Samstag | Sonntag
		*/
		
		$monate=explode("\n", $this->getstr('cbm_monate',[],$this->monate));
		
		if(IsSet($tgres['args'][$kommandidx+1])) { // von sich selber aufgerufen, zeit als args
			$datum=intval($tgres['args'][$kommandidx+1]);
			if(IsSet($tgres['args'][$kommandidx+2]) and $tgres['args'][$kommandidx+2] == 'ret') { // Beenden, DatumZeit übergeben
				return($datum);
			}
		}
		$answer=$this->getstr('cbm_waehle',[],'Wähle: ');

		if($datum == 0) $datum=time();
		// Aktuelle Werte zerlegen
		$year=intval(date('Y', $datum));
		$month=intval(date('m', $datum));
		$day=intval(date('d', $datum));
		
		$configarray[]=array(array('text' => $this->getstr('cbm_jetzt',[],'Jetzt: ').date($this->getstr('datetime_date',[],'Y-m-d'),time()), 'callback_data' => "$callbackstr date ".time()));
		$configarray[]=array( // Jahre
											array('text' => date('Y',$datum)-2, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year-2,$month,$day))), // date('Y-m-d H:i',$zeit).' - 2 year')),
											array('text' => date('Y',$datum)-1, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year-1,$month,$day))),
											array('text' => '_'.date('Y',$datum).'_', 'callback_data' => "$callbackstr date $datum"),
											array('text' => date('Y',$datum)+1, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year+1,$month,$day))),
											array('text' => date('Y',$datum)+2, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year+2,$month,$day)))
									);
		$configarray[]=array( // Monate
											array('text' => $monate[substr($this->TestAndCorrDate($year,$month-2,$day),5,2)-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month-2,$day))),
											array('text' => $monate[substr($this->TestAndCorrDate($year,$month-1,$day),5,2)-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month-1,$day))),
											array('text' => '_'.$monate[date('m',$datum)-1].'_', 'callback_data' => "$callbackstr date $datum"),
											array('text' => $monate[substr($this->TestAndCorrDate($year,$month+1,$day),5,2)-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month+1,$day))),
											array('text' => $monate[substr($this->TestAndCorrDate($year,$month+2,$day),5,2)-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month+2,$day)))
									);
//		$year=date('Y',$datum);
//		$month=date('m',$datum);
		$weakday=date('N',strtotime("$year-$month-01 00:00:00"));
		$weakoffset=$weakday-1;
		for($i=0;$i < 7;$i++) {
			$configarray1=array();
			if($i == 0 or checkdate(intval($month), $i*7+1-$weakoffset, intval($year))) {
				// Erste Zeile ($i == 0) oder erster Wochentag gültig
				for($j=1;$j < 8; $j++) {
					$iday=$i*7+$j-$weakoffset;
					if(checkdate(intval($month), $iday, intval($year))) {
						if($iday == $day) $iday='_'.$day.'_';
						$configarray1[]=array('text' => $iday, 'callback_data' => "$callbackstr date ".strtotime("$year-$month-$iday"));
					}else{
						$configarray1[]=array('text' => '-', 'callback_data' => "$callbackstr date $datum");
					}
				}
			}
			$configarray[]=$configarray1;
		}
		$selectedtime=$this->TestAndCorrDate($year,$month,$day);
		$configarray[]=array(array('text' => $this->getstr('cbm_ok',[],'Übernehmen: ').substr($selectedtime,0,10), 'callback_data' => "$callbackstr date ".strtotime($selectedtime).' ret'));
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}  //function datum()
	
	/**
	* function datumzeit()
	*
	* Stellt ein Callback Menü zur Eingabe von Datum und Zeit bereit
	* 
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param integer $zeit - Ausgangszeitstempel (Timestamp)
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	
	function datumzeit($callbackstr, $tgres, $zeit=0, $kommandidx=1)
	{
		/**
		*
		* Datum
		* Jahr-1 | Jahr | Jahr+1
		* Monat-2    | Monat-1    | Monat    | Monat+1    |    Monat+2
		* Montag  | Dienstag | Mittwoch | Donnerstag | Freitag | Samstag | Sonntag
		*/
		
		$monate=explode("\n", $this->getstr('cbm_monate',[],$this->monate));
		
		if(IsSet($tgres['args'][$kommandidx+1])) { // von sich selber aufgerufen, zeit als args
			$zeit=intval($tgres['args'][$kommandidx+1]);
			if(IsSet($tgres['args'][$kommandidx+2]) and $tgres['args'][$kommandidx+2] == 'ret') { // Beenden, DatumZeit übergeben
				return($zeit);
			}
		}
		$answer=$this->getstr('cbm_waehle',[],'Wähle: ');

		if($zeit == 0) $zeit=time();
		$datetab='date';
		if(IsSet($tgres['args'][$kommandidx]) and in_array($tgres['args'][$kommandidx],['month', 'hour', 'min'])) $datetab=$tgres['args'][$kommandidx];

		$configarray[]=array(array('text' => $this->getstr('cbm_jetzt',[],'Jetzt: ').date($this->getstr('datetime_datetime',[],'Y-m-d'),time()), 'callback_data' => "$callbackstr date ".time()));
		$datemarker='';
		$hourmarker='';
		$minmarker='';
		switch($datetab) {
			case 'min':
				$minmarker='_';
			break;
			case 'hour':
				$hourmarker='_';
			break;
			default:
				$datemarker='_';
			break;
		}
		$configarray[]=array(
											array('text' => $datemarker.date($this->getstr('datetime_datet',[],'Y-m-d'), $zeit).$datemarker, 'callback_data' => "$callbackstr date $zeit"),
											array('text' => $hourmarker.date('H:', $zeit).$hourmarker, 'callback_data' => "$callbackstr hour $zeit"),
											array('text' => $minmarker.date('i', $zeit).$minmarker, 'callback_data' => "$callbackstr min $zeit")
										);
		// Aktuelle Werte zerlegen
		$year=intval(date('Y', $zeit));
		$month=intval(date('m', $zeit));
		$day=intval(date('d', $zeit));
		$hour=intval(date('H', $zeit));
		$min=intval(date('i', $zeit));
		
		switch($datetab) {
			case 'date':
				$configarray[]=array( // Jahre
													array('text' => date('Y',$zeit)-2, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year-2,$month,$day,$hour,$min))), // date('Y-m-d H:i',$zeit).' - 2 year')),
													array('text' => date('Y',$zeit)-1, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year-1,$month,$day,$hour,$min))),
													array('text' => '_'.date('Y',$zeit).'_', 'callback_data' => "$callbackstr date $zeit"),
													array('text' => date('Y',$zeit)+1, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year+1,$month,$day,$hour,$min))),
													array('text' => date('Y',$zeit)+2, 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year+2,$month,$day,$hour,$min)))
											);
				$configarray[]=array( // Monate
													array('text' => $monate[intval(substr($this->TestAndCorrDate($year,$month-2,$day,$hour,$min),5,2))-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month-2,$day,$hour,$min))),
													array('text' => $monate[intval(substr($this->TestAndCorrDate($year,$month-1,$day,$hour,$min),5,2))-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month-1,$day,$hour,$min))),
													array('text' => '_'.$monate[date('m',$zeit)-1].'_', 'callback_data' => "$callbackstr date $zeit"),
													array('text' => $monate[intval(substr($this->TestAndCorrDate($year,$month+1,$day,$hour,$min),5,2))-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month+1,$day,$hour,$min))),
													array('text' => $monate[intval(substr($this->TestAndCorrDate($year,$month+2,$day,$hour,$min),5,2))-1], 'callback_data' => "$callbackstr date ".strtotime($this->TestAndCorrDate($year,$month+2,$day,$hour,$min)))
											);
//				$year=date('Y',$zeit);
//				$month=date('m',$zeit);
				$weakday=date('N',strtotime("$year-$month-01 00:00:00"));
				$weakoffset=$weakday-1;
				for($i=0;$i < 7;$i++) {
					$configarray1=array();
					if($i == 0 or checkdate(intval($month), $i*7+1-$weakoffset, intval($year))) {
						// Erste Zeile ($i == 0) oder erster Wochentag gültig
						for($j=1;$j < 8; $j++) {
							$iday=$i*7+$j-$weakoffset;
							if(checkdate(intval($month), $iday, intval($year))) {
								if($iday == $day) $iday='_'.$day.'_';
								$configarray1[]=array('text' => $iday, 'callback_data' => "$callbackstr date ".strtotime("$year-$month-$iday $hour:$min"));
							}else{
								$configarray1[]=array('text' => '-', 'callback_data' => "$callbackstr date $zeit");
							}
						}
					}
					$configarray[]=$configarray1;
				}
			break;
			case 'hour':
				for($i=0; $i < 4; $i++) {
					$configarray1=array();
					for($j=0; $j < 6; $j++) {
						$ihour=$i*6+$j;
						if($ihour == $hour) $ihour='_'.$hour.'_';
						$configarray1[]=array('text' => $ihour, 'callback_data' => "$callbackstr hour ".strtotime("$year-$month-$day $ihour:$min"));
					}
					$configarray[]=$configarray1;
				}
			break;
			case 'min':
				for($i=0; $i < 10; $i++) {
					$configarray1=array();
					for($j=1; $j < 7; $j++) {
						$imin=$i*6+$j-1;
						if($imin == $min) $imin='_'.$min.'_';
						$configarray1[]=array('text' => $imin, 'callback_data' => "$callbackstr min ".strtotime("$year-$month-$day $hour:$imin"));
					}
					$configarray[]=$configarray1;
				}
			break;
		}
		$selectedtime=$this->TestAndCorrDate($year,$month,$day,$hour,$min);
		$configarray[]=array(array('text' => $this->getstr('cbm_ok',[],'Übernehmen: ').$selectedtime, 'callback_data' => "$callbackstr date ".strtotime($selectedtime).' ret'));
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}  //function datumzeit()
	
	/**
	* function TestAndCorrDate()
	*
	* Prüft die Übergebenen Datums/Zeitwerte auf Gültigkeit, Korrigiert / Rechnet sie gültig
	*
	* @param integer $year - Jahr
	* @param integer $month - Monat
	* @param integer $day - Tag
	* @param integer $hour=0 - Stunde
	* @param integer $min=0 - Minute
	* @param integer $sec=0 - Sekunde
	* 
	* @param string return - Zeitstring (Y-m-d H:i:s)
	*/
	function TestAndCorrDate($year, $month, $day, $hour=0, $min=0, $sec=0)
	// Testet, korrigiert wenn möglich das Datum und gibt es als Datetimestring zurück, bei Fehlern das übergebene Datum
	{
		$count=25; // Muss über alle Schleifendurchläufe > 0 sein, sonst Fehler
		$iday=intval($day);
		$imonth=intval($month);
		$iyear=intval($year);
		while($imonth>12 and $count>0) {
			$imonth=$imonth-12;
			$iyear++;
			$count--;
		}
		while($imonth<1 and $count>0) {
			$imonth=$imonth+12;
			$iyear--;
			$count--;
		}
		while(!checkdate($imonth, $iday, $iyear) and $count>0) {
			$iday--;
			$count--;
		}
		if($count = 0) {
			error_log(__FILE__.' '.__LINE__.' to many month');
			return(str_pad($year,4,'0',STR_PAD_LEFT).'-'.str_pad($month,2,'0',STR_PAD_LEFT).'-'.str_pad($day,2,'0',STR_PAD_LEFT).' '.str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($min,2,'0',STR_PAD_LEFT).':'.str_pad($sec,2,'0',STR_PAD_LEFT));
		}
		return(str_pad($iyear,4,'0',STR_PAD_LEFT).'-'.str_pad($imonth,2,'0',STR_PAD_LEFT).'-'.str_pad($iday,2,'0',STR_PAD_LEFT).' '.str_pad($hour,2,'0',STR_PAD_LEFT).':'.str_pad($min,2,'0',STR_PAD_LEFT).':'.str_pad($sec,2,'0',STR_PAD_LEFT));
	}

	/**
	* function WertRoller
	*
	* Gibt eine Zahleneingabe in Form eines Zahlen(Rollen)schlosses als Callbackmenü zurück
	*
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param string $titel - Ein Titel für das Callback-Menü
	* @param integer $defwert - Vorgabewert
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	function WertRoller($callbackstr, $tgres, $titel, $defwert, $kommandidx=1)
	{
		if(IsSet($tgres['args'][$kommandidx])) { // von sich selber aufgerufen, Auswahl als args
			$auswahl=$tgres['args'][$kommandidx];
			if(IsSet($tgres['args'][$kommandidx+1]) and $tgres['args'][$kommandidx+1] == 'ret') { // Beenden, DatumZeit übergeben
				return($auswahl);
			}
		}
		$answer=$titel;
		
		$ausgabe=str_pad($tgres['args'][$kommandidx], 6, 0, STR_PAD_LEFT);
		for ($i = -5; $i < 5; $i++) {
			$configarray1=array();
			for($j=0; $j < 6; $j++) {
				$digit=intval($ausgabe[$j])+110+$i;
				$digit='_'.$digit;
				$digit=$digit[3];
				$ausgabe1=$ausgabe;
				$ausgabe1[$j]=$digit;
				if($i == 0) $digit='_'.$digit.'_';
				$configarray1[]=array('text' => $digit, 'callback_data' => "$callbackstr $ausgabe1");
			}
			$configarray[]=$configarray1;
		}
		$configarray[]=array(array('text' => $this->getstr('cbm_ok',[],'Übernehmen: ').intval($ausgabe), 'callback_data' => "$callbackstr $ausgabe ret"));
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}
	
	/**
	* function WertFeld
	*
	* Gibt eine Zahleneingabe in Form von Zahlenreihen für jede Ziffer als Callbackmenü zurück
	*
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param string $titel - Ein Titel für das Callback-Menü
	* @param integer $defwert - Vorgabewert
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	function WertFeld($callbackstr, $tgres, $titel, $defwert, $kommandidx=1)
	{
		if(IsSet($tgres['args'][$kommandidx])) { // von sich selber aufgerufen, Auswahl als args
			$auswahl=$tgres['args'][$kommandidx];
			if(IsSet($tgres['args'][$kommandidx+1]) and $tgres['args'][$kommandidx+1] == 'ret') { // Beenden, DatumZeit übergeben
				return($auswahl);
			}
		}
		$answer=$titel;
		
		$ausgabe=str_pad($tgres['args'][$kommandidx], 6, 0, STR_PAD_LEFT);
		for ($i = 0; $i < 10; $i++) {
			$configarray1=array();
			for($j=0; $j < 6; $j++) {
				$digit=intval($ausgabe[$j]);
				$ausgabe1=$ausgabe;
				$ausgabe1[$j]=$i;
				$idigit=$i;
				if($i == $digit) $idigit='_'.$idigit.'_';
				$configarray1[]=array('text' => $idigit, 'callback_data' => "$callbackstr $ausgabe1");
			}
			$configarray[]=$configarray1;
		}
		$configarray[]=array(array('text' => $this->getstr('cbm_ok',[],'Übernehmen: ').intval($ausgabe), 'callback_data' => "$callbackstr $ausgabe ret"));
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}
	
	/**
	* function Auswahlliste
	*
	* Erstellt eine Liste der übergebenen Aktionen als Callback-Menü
	*
	* @param string $callbackstr - String mit dem Beginn der Übergabeparameter am callback_data
	* @param array $tgres - Von telegramclass->decodemessage() erzeugtes Array der Empfangenen Telegram-Daten
	* @param string $titel - Ein Titel für das Callback-Menü
	* @param array $options - Vorgabewert
	*                         Als einfaches Array von Strings, Text und Kommando sind gleich
	*                         Als Array mit optionen als Array('text' => Beschreibung, 'command' => Kommando)
	*                         Als Array von (array von strings oder array von optionen) für Mehrspaltige Ausgabe
	* @param integer $kommandidx - Der Index für $tgres['args'] ab dem für Funktionsinterne Parameter zur Verfügung steht 
	*/
	function AuswahlListe($callbackstr, $tgres, $titel, $options, $kommandidx=1)
	{
		if(IsSet($tgres['args'][$kommandidx])) { // von sich selber aufgerufen, Auswahl als args
			$auswahl=$tgres['args'][$kommandidx];
			if(IsSet($tgres['args'][$kommandidx+1]) and $tgres['args'][$kommandidx+1] == 'ret') { // Beenden, DatumZeit übergeben
				return($auswahl);
			}
		}
		$answer=$titel;
		
		foreach($options AS $option) {
			if(is_array($option) and IsSet($option['text'])) { // Option ist ein Array('text' => Beschreibung, 'command' => Kommando)
				$command=$option['command'];
				$configarray[]=array(array('text' => $option['text'], 'callback_data' => "$callbackstr $command ret"));
			}elseif(is_array($option)) { // Mehrspaltig
				$configarray1=array();
				foreach($option AS $subopt) {
					if(is_array($subopt) and IsSet($subopt['text'])) {
						$command=$subopt['command'];
						$configarray1[]=array('text' => $subopt['text'], 'callback_data' => "$callbackstr $command ret");
					}elseif(is_array($subopt)) {
						error_log(__FILE__.', '.__LINE__." callbackquery->AuswahlListe - Falsche Übergabe options\n".json_encode($options));
					}else{
						$configarray1[]=array('text' => $subopt, 'callback_data' => "$callbackstr $subopt ret");
					}
				}
				$configarray[]=$configarray1;
			}else{ // Option ist ein String für Beschreibung = Commando
				$configarray[]=array(array('text' => $option, 'callback_data' => "$callbackstr $option ret"));
			}
			
		}
		if(IsSet($configarray)) {
			$configarray=array('inline_keyboard'=>$configarray);

			$answer=array(
				'text' => $answer,
				'reply_markup' => json_encode($configarray)
			);
			if($tgres['message_typ']=='callback_query') {
				if(IsSet($tgres['message_id'])) {
					$answer['message_id']=$tgres['message_id'];
				}
			}
		}
		$this->tg->sendmessage($this->tg->user_id,$answer);
		
		return(false);
	}
	
}
?>