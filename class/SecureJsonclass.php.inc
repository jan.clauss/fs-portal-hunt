<?php

// von Crytix

// 2021-02-12

class SecureJSON
{
    public $array;
    public $key;
    
    public function __construct(array $array)
    {
        $this->array = $array;
    }
    
    public function jsonSerialize()
    {
        $original = $this->array;
//        ksort($data);
				ksort($original);
        $original['signature'] = hash_hmac('sha256', json_encode($original), $this->key);
        
        return $original;
    }
    
    public static function fromJson(string $data, string $key)
    {
        $data = json_decode($data, true);
        $signature = $data['signature'];
        unset($data['signature']);
        
        ksort($data);
        
        if (hash_equals(hash_hmac('sha256', json_encode($data), $key), $signature)) {
            return new static($data);
        }
        
        throw new \Exception('Invalid signature');
    }
}
