﻿<?php

/**** Klasse für Telegram
*
*	@Author: Jan Clauß
* @Version: 1.1 (2021-12-24)
*
* 1.5 Keyboardbuttons
* 1.1 reply_message_id in Messagearray
* 
*/

class telegram
{
	private $bottoken='';
	private $botid=0;

	public $maxmessagesize = 4096; // Telegram kann nur 4096 Byte große Nachrichten
	public $message_limit = 4; // Maximal anzahl Nachrichten, wenn eine Nachricht gesplittet wird
	public $parsemode = 'HTML'; // Telegram wertet die Übergebenen Daten als html aus, nur ein paar der html Formatierungen
	public $disablewebpagepreview = false; // Schaltet die Link-Vorschau aus
	public $delete_not_editmessage = false; // Nachricht wird nicht geändert, sondern gelöscht und neu geschrieben
	
	public $migrate_chat_info_fkt = ''; // Für eine Funktion, die beim Wechsel der Chat-ID aufgerufen wird (group->supergroup)
	// fname($old, $new);
	
	private $lastpollmessageid=0; // Nur für das Polling, die letzte Message ID
	
	public $noiseerrors = array(
		400 => array(
			'Bad Request: message is not modified',
			"Bad Request: message can't be deleted for everyone",
		),
		403 => '', // Forbidden: bot was blocked by the user
		429 => '', // Too Many Requests
	);
	
	function __construct($bot_token)
	{
  	$this->bottoken=$bot_token;
  	$this->botid=intval(substr($bot_token,0,strpos($bot_token,':')));
  }
  
  public function getbottoken() {
  	return($this->bottoken);
  }

  public function getbotid() {
  	return($this->botid);
  }
  
  public function botstatusmeldung($msg)
  // Wird aufgerufen, wenn der Bot eine Meldung von "sich selber" bekommt
  // - z.B. Er hat sich aus einer Gruppe entfernt - Wird er entfernt kommt nix zumindest, wenn er keinen Zugriff auf Nachrichten hat
  // $msg enthält das unbearbeitete Message Array
  // Diese Funktion in abgeleiteter Klasse ersetzen, wenn benötigt
  //
  // ['message']['left_chat_member']=array('id'=id, 'is_bot' = true... // Chat verlassen
  {
  	exit;
  }
  
  public function checkNoiseError($error_code, $error_text)
  {
  	if(!IsSet($this->noiseerrors[$error_code])) return false; // Fehlercode nicht in Noise-Liste
  	if(!is_array($this->noiseerrors[$error_code]) === '') return true; // Nur auf Error Code, Error-Code vorhanden
  	foreach ($this->noiseerrors[$error_code] AS $needle) {
  	 	if(strpos($error_text, $needle) !== false) return true; // Substring vorhanden
  	}
  	return false;
  }
  
  public function errorhandler($result, $data, $function) {
  	// Prüft die Rückgabe auf Fehler und gibt sie ggfls. aus
  	if(!is_array($result)) $result=json_decode($result,true);
  	if(IsSet($result['ok']) and $result['ok'] == true) return(true);
  	$errormsg="$function";
  	if(IsSet($result['error_code'])) $errormsg.=", ErrCode: ".$result['error_code'].' ';
  	if(IsSet($result['description'])) $errormsg.=", ErrDesc: ".$result['description'].' ';
  	if(IsSet($data['chat_id'])) $errormsg.=", ID: ".$data['chat_id'].' ';
  	if(IsSet($data['text'])) $errormsg.=", Text: ".$this->escapestr($data['text']);
  	if(!IsSet($result['error_code']) or $this->checkNoiseError($result['error_code'], $result['description']) == false) {
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'error',$errormsg,'main');
		}
  	error_log(__FILE__.', '.__LINE__." $errormsg");
  }
  
  public function installwebhook($script = '', $maxconnections = -1, $allowed_updates = '', $certificate = '')
  // Webhook installieren
  {
		$ch = curl_init();
		if($script == '') { // Kein Script übergeben, nehme laufendes Skript als Webhook
			$script=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
		}
  	echo $this->bottoken." - ".$script;
		$data['url'] = $script; // Der Skriptname, der von Telegram aufgerufen wird
		if($certificate <> '') { // !!Ungetestet
			$data['certificate'] = ''; // Das Zertifikat, bei nichtauflösbaren Zertifikaten
		}
		if($maxconnections <> -1) { // !!Ungetestet
			$data['max_connections']=$maxconnections; // Maximal gleichzeitige Verbinungen
		}
		if($allowed_updates <> '') { // !!Ungetestet
			$data['allowed_updates']=''; // Kommandos die erlaubt sind
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/setWebhook");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'installwebhook');
		return($res);
	}

	public function uninstallwebhook()
		// Webhook stoppen
	{
		$ch = curl_init();
		$data['url'] = "";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/setWebhook");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'uninstallwebhook');
		return($res);
	}
	
	public function pollmessage()
		// Ruft neue Nachrichten ab, wenn kein Webhook installiert ist
	{
		return(false); // Noch nicht implementiert
	}
	
	public function decodemessage($message)
		// Dekodiert eine empfangene Nachricht in ein einheitliches Array (Kommando, Callback, Antwort)

		// $message enthält die Nachtricht von Telegram
		
		//* ['date'] => Zeitpunkt der Nachricht ( timestamp )
		//* ['message_typ'] => query, callback_query, location, document, photo
		//* ['user_id'] => Telegram ID
		//* ['first_name'] => Vorname
		//* ['last_name'] => Nachname
		//* ['user_name'] => Telegram Username
		//* ['language_code']
		//* ['message_id'] => ID der Nachricht
		//* ['text'] => Der Text aus der Eingabe
		//* ['chat']['id'] => Chat-ID bei privat == user_id
		//* ['chat']['title'] => Chat-Name ist type <> private
		//* ['chat']['first_name'] => Vorname ist type == private
		//* ['chat']['last_name'] => Nachname ist type == private
		//* ['chat']['username'] => Telegram Username ist type == private
		//* ['chat']['type'] => private, group, supergroup, channel
		//* ['new_chat_participant']['id'] => Wenn jemand zur Gruppe hinzukommt, die ID
		//* ['new_chat_participant']['is_bot'] => 1, wenn Bot
		//* ['new_chat_participant']['first_name'] => Vorname / Name
		//* ['new_chat_participant']['last_name'] => Nachname
		//* ['new_chat_participant']['user_name'] => Username
		//* ['command'] => Extrahiertes Kommando, erstes Wort ([a-z0-9]+)
		//* ['args'] => Array aller Argumente
		//* ['lat'] => location - latitude
		//* ['lng'] => location - longitude
		//* ['filename'] => document
		//* ['filesize'] => document, photo
		//* ['filemime'] => document, photo
		//* ['fileid'] => document, photo
		//* ['fileuid'] => document, photo
		//* ['width'] => photo
		//* ['height'] => photo
		//* ['thumb']['filesize'] => document, photo
		//* ['thumb']['fileid'] => document, photo
		//* ['thumb']['fileuid'] => document, photo
		//* ['thumb']['width'] => document, photo
		//* ['thumb']['height'] => document, photo
		//* ['thumb1']['filesize'] => photo
		//* ['thumb1']['fileid'] => photo
		//* ['thumb1']['fileuid'] => photo
		//* ['thumb1']['width'] => photo
		//* ['thumb1']['height'] => photo
	{
		$ret=array();
		$msg=json_decode($message,true);
		if(IsSet($msg['message']['from']['is_bot']) and $msg['message']['from']['is_bot'] == true and $msg['message']['from']['id'] == $this->botid) {
			// Eine Meldung vom Bot selber, z.b verlassen eines Chat
			$this->botstatusmeldung($msg);
		}
		// ########################################
		// User ermitteln und prüfen
		// ########################################
		if(IsSet($msg['message']['date'])) $ret['date']=$msg['message']['date'];
		if(IsSet($msg['message']) and IsSet($msg['message']['from']['id'])) {
//		error_log(__FILE__.' '. __LINE__.' '.$message);
			// Normale Message an Bot Prüfen, das ID Vorhanden für Identifikation
			$ret['message_typ']='query';
			$ret['user_id']=$msg['message']['from']['id'];
			$ret['first_name']='';
			if(IsSet($msg['message']['from']['first_name'])) $ret['first_name']=$msg['message']['from']['first_name'];
			$ret['last_name']='';
			if(IsSet($msg['message']['from']['last_name'])) $ret['last_name']=$msg['message']['from']['last_name'];
			$ret['username']='';
			if(IsSet($msg['message']['from']['username'])) $ret['username']=$msg['message']['from']['username'];
			$ret['language_code']='';
			if(IsSet($msg['message']['from']['language_code'])) $ret['language_code']=$msg['message']['from']['language_code'];
			$ret['text']='';
			if(IsSet($msg['message']['text'])) $ret['text']=$msg['message']['text'];
			if(IsSet($msg['message']['chat'])) {
				$ret['chat']=$msg['message']['chat'];
			}
			if(IsSet($msg['message']['new_chat_participant'])) $ret['new_chat_participant']=$msg['message']['new_chat_participant'];
			if(IsSet($msg['message']['migrate_from_chat_id'])) {
				$ret['migrate_from_chat_id']=$msg['message']['migrate_from_chat_id']; // Chat von Gruppe nach Supergruppe gewechselt
				$this->MigrateFromChatId($msg['message']['migrate_from_chat_id'], $msg['message']['chat']['id']);
			}
			$ret['message_id']=$msg['message']['message_id'];
			if(IsSet($msg['message']['reply_to_message']['message_id'])) $ret['replytomessage_id']=$msg['message']['reply_to_message']['message_id'];
			if(IsSet($msg['message']['location'])) {
				// Position
				$ret['message_typ']='location';
				$ret['lat']=$msg['message']['location']['latitude'];
				$ret['lng']=$msg['message']['location']['longitude'];
			}
			if(IsSet($msg['message']['document'])) {
				// Dokument - Liefert z.T. ein Thumbnail mit
				$ret['message_typ']='document';
				$ret['filename']=$msg['message']['document']['file_name'];
				$ret['filesize']=$msg['message']['document']['file_size'];
				$ret['filemime']=$msg['message']['document']['mime_type'];
				$ret['fileid']=$msg['message']['document']['file_id'];
				$ret['fileuid']=$msg['message']['document']['file_unique_id'];
				if(IsSet($msg['message']['document']['thumb'])) {
					$ret['thumb']['filesize']=$msg['message']['document']['thumb']['file_size'];
					$ret['thumb']['fileid']=$msg['message']['document']['thumb']['file_id'];
					$ret['thumb']['fileuid']=$msg['message']['document']['thumb']['file_unique_id'];
					$ret['thumb']['width']=$msg['message']['document']['thumb']['width'];
					$ret['thumb']['height']=$msg['message']['document']['thumb']['height'];
				}
			}
			if(IsSet($msg['message']['photo'])) {
				// Photo, Begrenzte Qualität und bis zu zwei Thumbnails
				$ret['message_typ']='photo';
				$photoidx=count($msg['message']['photo'])-1; // Letztes Bild im Array ist das größte
				$ret['filesize'] = $msg['message']['photo'][$photoidx]['file_size'];
				$ret['fileid'] = $msg['message']['photo'][$photoidx]['file_id'];
				$ret['fileuid'] = $msg['message']['photo'][$photoidx]['file_unique_id'];
				$ret['width'] = $msg['message']['photo'][$photoidx]['width'];
				$ret['height'] = $msg['message']['photo'][$photoidx]['height'];
				if($photoidx > 0) { // Es gibt ein Thumbnail
					$photoidx--;
					$ret['thumb']['filesize']=$msg['message']['photo'][$photoidx]['file_size'];
					$ret['thumb']['fileid']=$msg['message']['photo'][$photoidx]['file_id'];
					$ret['thumb']['fileuid']=$msg['message']['photo'][$photoidx]['file_unique_id'];
					$ret['thumb']['width']=$msg['message']['photo'][$photoidx]['width'];
					$ret['thumb']['height']=$msg['message']['photo'][$photoidx]['height'];
					if($photoidx > 0) { // und noch ein kleineres Thumbnail
						$photoidx--;
						$ret['thumb1']['filesize']=$msg['message']['photo'][$photoidx]['file_size'];
						$ret['thumb1']['fileid']=$msg['message']['photo'][$photoidx]['file_id'];
						$ret['thumb1']['fileuid']=$msg['message']['photo'][$photoidx]['file_unique_id'];
						$ret['thumb1']['width']=$msg['message']['photo'][$photoidx]['width'];
						$ret['thumb1']['height']=$msg['message']['photo'][$photoidx]['height'];
					}
				}
			}
		}elseif(IsSet($msg['callback_query']) and IsSet($msg['callback_query']['from']['id'])) {
			// Callback Query, ['data'] wird zu ['text']
			$ret['message_typ']='callback_query';
			$ret['user_id']=$msg['callback_query']['from']['id'];
			$ret['first_name']='';
			if(IsSet($msg['callback_query']['from']['first_name'])) $ret['first_name']=$msg['callback_query']['from']['first_name'];
			$ret['last_name']='';
			if(IsSet($msg['callback_query']['from']['last_name'])) $ret['last_name']=$msg['callback_query']['from']['last_name'];
			$ret['username']='';
			if(IsSet($msg['callback_query']['from']['username'])) $ret['username']=$msg['callback_query']['from']['username'];
			$ret['language_code']='';
			if(IsSet($msg['callback_query']['from']['language_code'])) $ret['language_code']=$msg['callback_query']['from']['language_code'];
			$ret['text']='';
			if(IsSet($msg['callback_query']['data'])) $ret['text']=$msg['callback_query']['data'];
			$ret['message_id']=$msg['callback_query']['message']['message_id'];
			if(IsSet($msg['callback_query']['message']['chat'])) {
				$ret['chat']=$msg['callback_query']['message']['chat'];
			}
		}
		if(IsSet($ret['message_typ'])) {
			$ret['command']=''; // Damit kein fehlender Index kommand, wenn der preg nicht passt
			if(preg_match('/^(\/?[a-z0-9]+)(.*)/i',$ret['text'],$arr)) {
				$ret['command']=$arr[1];
				$ret['args']=array();
				if(trim($arr[2]) <> '') {
					preg_match_all('/([^ ]+)/',$arr[2], $args);
					$ret['args']=$args[0];
				}
			}
		}
		$ret['originalmessage']=$msg;
		return($ret);
	}
	
	public function MigrateFromChatId($old, $new)
	{
		if($this->migrate_chat_info_fkt <> '') { // Es ist eine Funktion hinterlegt, über die Informiert wird
			$MigrateChatInfoFkt=$this->migrate_chat_info_fkt;
			if(function_exists($MigrateChatInfoFkt)) {
				$MigrateChatInfoFkt($old, $new);
			}
		}
	}
	
	public function escapestr($str)
	{
		if(!is_String($str)) {
			return('telegram->escapestr() - Fail in string');
		}
		if($this->parsemode == 'HTML') { // Parsemode HTML
			return(htmlspecialchars($str,ENT_QUOTES));
		}else{ // Parsemode Markdown // nicht getestet
			$search=array('_', '*', '[', '`');	// html_entity_decode
			$replace=array('\_', '\*', '\[', '\`');
			return(str_replace($search, $replace, $str));
		}
	}
	
	public function escapearr(&$data)
	{
		if(is_array($data)) {
			if(count($data) > 0) {
				foreach($data AS &$subdata) {
					$this->escapearr($subdata);
				}
			}
		}else{
			if(is_string($data)) {
				$data=$this->escapestr($data);
			}
		}
	}
	
	public function sendmessage($user, $message, $othermessagelimit = 0)
		// Sendet eine Nachricht an den User oder Gruppe
		
		// $message -> Message als Text, als Array für Telegram oder 
		//		irgendein anderes Array, das dann in String gewandelt wird, wenn kein ['text'] vorhanden ist
		// $user -> Der Empfänger (Telegram ID)
		// $othermessagelimit -> Maximale Anzahl Nachrichten, die gesendet werden dürfen
		
	{
//		error_log(__FILE__.' '. __LINE__.' '." $user $message");
	  $res='';
	  $resarr['ok']=true; // Alles in Ornung
		if(is_array($message) and IsSet($message['text'])) { // Array für Telegram
			$messagearray=$message;
			$messagetext=$messagearray['text'];
		}elseif(is_array($message)) { // Irgendein anderes Array, Konvertieren nach String
			$messagetext=print_r($message,true);
		}else{ // Einfacher Text
			$messagetext=$message;
		}
		$messagecount=0;
		if(strlen($messagetext) > $this->maxmessagesize) {
			while(strlen($messagetext) > 0 and ($messagecount < $this->message_limit or $messagecount < $othermessagelimit) and $resarr['ok'] == true) {
				// Teilnachrichten, solange nicht Messagelimit und senden einer Nachricht fehlgeschlagen
				$messagecount++;
				$pos=strrpos(substr($messagetext,0,$this->maxmessagesize),"\n"); // Versuch einen Zeilenumbruch vor dem Message Limit Ende zu finden
				if($pos === false) { // nicht gefunden
					$pos=$this->maxmessagesize;
				}
				$msgtext=substr($messagetext,0,$pos);
				$resarr=$this->sendmessage($user,$msgtext);
				$messagetext=trim(substr($messagetext,$pos)); // Gesendeten Teil entfernen
			}
			if(IsSet($messagearray)) {
				$messagearray['text']=substr($msgtext,0,10); // Nur noch Teilstring vom letzten gesendeten Text
				$resarr=$this->sendmessage($user,$messagearray);
//				error_log('2: '.print_r($resarr,true));
			}
		}else{
			$ch = curl_init();
			if(IsSet($messagearray)) {
				$data=$messagearray;
				$data['text']=$messagetext;
			}else{
				$data['text']=$messagetext;
			}
			if(trim($data['text']) == '') {
				$this->errorhandler(array('ok' => false, 'error_code' => '400', 'description' => 'Bad Request: message text is empty'),array('chat_id' => $this->user_id, 'text' => '*empty*'),'sendmessage');
				return(json_encode(array('ok' => false, 'error_code' => '400', 'description' => 'Bad Request: message text is empty')));
			}
			$data['chat_id'] = intval($user);
			$data['parse_mode'] = $this->parsemode;
			$data['disable_web_page_preview'] = $this->disablewebpagepreview;
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	 		if(IsSet($data['message_id'])) { // Ändern einer Nachricht
	 			if($this->delete_not_editmessage) { // Noch ungetestet
	 				$this->deletemessage($user,$data['message_id']);
	 				curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
	 			}else{
		  		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/editMessageText");
		  	}
	  	}else{
		  	curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
	  	}
			$res = curl_exec($ch);
			$this->errorhandler($res,$data,'sendmessage');
			$resarr=json_decode($res,true);
			unset($ch);
		}
//		error_log('3: '.print_r($resarr,true));
		return($resarr);
	}
	
	function sendMsgBigDoc($user_id, $text, $maxsize=2048, $filename='text.txt')
	{
		// Sendet eine Text-Nachricht bis $maxsize über sendmessage, dann über sendDocument
		if(strlen($text) > $maxsize) {
			$tempfile=sys_get_temp_dir().'/sendMsgBigDoc'.time().'.tmp';
			$tempstr=file_put_contents($tempfile, $text);
			$this->sendDocument($this->user_id, $tempfile, $filename, $filename);
			unlink($tempfile);
		}else{
			$this->sendmessage($this->user_id, $text);
		}
	}
	
	function editMessageText($user_id, $message_id, $text, $reply_markup='')
	{
		//Ändert den Text einer schon gesendeten Nachricht
		if(is_array($message_id) and IsSet($message_id['result']['message_id'])) $message_id=$message_id['result']['message_id'];
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup);
		$ch = curl_init();
		$data['chat_id']=intval($user_id);
		$data['text']=$text;
		$data['message_id']=$message_id;
		$data['reply_markup']=$reply_markup;
		$data['parse_mode'] = $this->parsemode;
		$data['disable_web_page_preview'] = $this->disablewebpagepreview;
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/editMessageText");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'editMessageText');
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	function editMessageCaption($user_id, $message_id, $text, $reply_markup='')
	{
		//Ändert den Text einer schon gesendeten Nachricht
		if(is_array($message_id) and IsSet($message_id['result']['message_id'])) $message_id=$message_id['result']['message_id'];
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup);
		$ch = curl_init();
		$data['chat_id']=intval($user_id);
		$data['caption']=$text;
		$data['message_id']=$message_id;
		$data['reply_markup']=$reply_markup;
		$data['parse_mode'] = $this->parsemode;
		$data['disable_web_page_preview'] = $this->disablewebpagepreview;
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/editMessageCaption");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'editMessageCaption');
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	function editMessageReplyMarkup($user_id, $message_id, $reply_markup='')
	{
		//Ändert den Text einer schon gesendeten Nachricht
		if(is_array($message_id) and IsSet($message_id['result']['message_id'])) $message_id=$message_id['result']['message_id'];
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup);
		$ch = curl_init();
		$data['chat_id']=intval($user_id);
		$data['message_id']=$message_id;
		$data['reply_markup']=$reply_markup;
		$data['parse_mode'] = $this->parsemode;
		$data['disable_web_page_preview'] = $this->disablewebpagepreview;
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/editMessageReplyMarkup");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'editMessageReplyMarkup');
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	/**************************
	* ReplyKeyboardSet
	* 
	* Setzt ein Keyboard
	*
	* @param integer $user_id Id des Users, der das Keyboard bekommt
	* @param string $text Auszugebender Text beim Setzen des Keyboards
	* @param array $keyboardarray array of Buttonstrings array('btn1', 'btn2'...)
	* @param boolean $ontimekeyboard Ontimekeyboard, default = false
	* @param boolean $resizekeyboards Buttons an die Texte anpassen, default = true
	*/

	function ReplyKeyboardSet($user_id,$text,$keyboardarray,$ontimekeyboard=false, $resizekeyboard=true)
	{
		$reply_markup['keyboard']=array($keyboardarray);
		if($ontimekeyboard == true) $reply_markup['one_time_keyboard'] = true; 
		if($resizekeyboard == true) $reply_markup['resize_keyboard'] = true;
		$reply_markup=json_encode($reply_markup);
		$ch = curl_init();
		$data['chat_id']=intval($user_id);
		$data['text']=$text;
		$data['reply_markup']=$reply_markup;
		$data['parse_mode'] = $this->parsemode;
		$data['disable_web_page_preview'] = $this->disablewebpagepreview;
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'ReplyKeyboardSet');
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	/***************************
	* ReplyKeyboardRemove
	*
	* @param integer $user_id Id des Users, der das Keyboard bekommt
	* @param string $text Auszugebender Text beim Rücksetzen des Keyboards
	*/
	
	function ReplyKeyboardRemove($user_id, $text)
	{
		$ch = curl_init();
		$data['chat_id']=intval($user_id);
		$data['text']=$text;
		$data['reply_markup']=json_encode(array('hide_keyboard'=>true));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'ReplyKeyboardRemove');
		$resarr=json_decode($res,true);
		return($resarr);
	}

	function testsendstr($user_id,$text) 
	{
		
		//* Sendet einen String Probeweise, um zu testen, ob Fehler enthalten sind
		
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'parse_mode' => 'HTML',
			'disable_web_page_preview'=>true
		); // Schaltet die Link-Vorschau aus
		$data['text']=$text;
		//	error_log(print_r($data,true));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/sendMessage");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'testsendstr');
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	
	function deletemessage($user_id,$message_id) {
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'message_id'=>$message_id
		);
//	error_log(__FILE__.' '. __LINE__.' '.print_r($data,true));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
	 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  	curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/deleteMessage");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'deletemessage');
		$resarr=json_decode($res,true);
		unset($ch);
		return($resarr);
	}
	
	public function sendDocument($user_id, $filename, $text, $sendfilename='', $reply_markup='') 
	// user_id - ID des Users (chat_id)
	// filename - Dateiname mit Pfad der zu sendenden Datei
	// text - Ein mit zu sendender Text
	// $sendfilename - Dateiname, als der die Datei gesendet werden soll
	{
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup); // Falls Reply Markup als Array überegeben wurde
		if(!file_exists($filename)) {
			// Datei nicht vorhanden
			return(array('ok' => 'false', 'description' => 'Datei existiert nicht'));
		}
		if($sendfilename == '') { // Ausgabefilename für Telegram aus $filename ermitteln
			$pos=strrpos($filename,'/');
			if($pos === false) { //Dateiname ohne Pfad
				$sendfilename=$filename;
			}else{
				$sendfilename=substr($filename,$pos+1);
			}
		}
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'caption' => $text,
			'document' => new CURLFile($filename,'text/plain',$sendfilename),
			'parse_mode' => $this->parsemode,
			'disable_web_page_preview' => $this->disablewebpagepreview
		);
		if($reply_markup <> '') $data['reply_markup']=$reply_markup;
		$url="https://api.telegram.org/bot".$this->bottoken."/sendDocument";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit wird die Antwort als String der Funktion curl_exec zurück gegeben
		curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-Type: multipart/form-data"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'sendDocument');
		return(json_decode($res,true));
	}
	
	public function sendDocumentById($user_id, $file_id, $text, $reply_markup='')
	{
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup); // Falls Reply Markup als Array überegeben wurde
		$ch = curl_init();
		$data=array(
			'chat_id' => intval($user_id),
			'caption' => $text,
			'document' => $file_id,
			'parse_mode' => $this->parsemode,
			'disable_web_page_preview' => $this->disablewebpagepreview
		);
		if($reply_markup <> '') $data['reply_markup']=$reply_markup;
		$url="https://api.telegram.org/bot".$this->bottoken."/sendDocument";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit wird die Antwort als String der Funktion curl_exec zurück gegeben
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'sendDocumentById');
		return(json_decode($res,true));
	}

	public function getFileName($file_id)
	{
		$ch = curl_init();
		$data=array(
			'file_id'=>$file_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/getFile");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'getFileName');
		return(json_decode($res,true));
	}

	public function getDocument($file_id)
	{
		$filearray=$this->getFileName($file_id);
		if($filearray['ok'] == false) {
			return($filearray);
		}
		$filename="https://api.telegram.org/file/bot".$this->bottoken."/".$filearray['result']['file_path'];
		$ch = curl_init();     
    curl_setopt($ch, CURLOPT_URL, $filename);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
    $res = curl_exec($ch);  
		$this->errorhandler($res,$data,'getDocument');
    curl_close($ch); 
    return(json_decode($res,true));
	}
	
	public function copyDocument($file_id,$localfile)
	{
		$local=fopen($localfile,'w');
		if(!$local) {
			return(array('ok' => false, 'error_code' => '2', 'description' => 'File-Error','errorInfo' => "Can't create file"));
		}
		$filearray=$this->getFileName($file_id);
		if($filearray['ok'] == false) {
			return($filearray);
		}
		$filename="https://api.telegram.org/file/bot".$this->bottoken."/".$filearray['result']['file_path'];
		$ch = curl_init();     
    curl_setopt($ch, CURLOPT_URL, $filename);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($ch, CURLOPT_UPLOAD, 0);  
    curl_setopt($ch, CURLOPT_FILE, $local);
    $res=curl_exec($ch);  
//		$this->errorhandler($res,null,'copyDocument');
    $result=curl_getinfo($ch); // curl_exec liefert auch bei http_code 404 ein true, deswegen über getinfo
    if($result['http_code'] == 200) { // Download erfolgreich
    	$result['ok']=true;
    }else{
    	$result['ok']=false;
    }
    curl_close($ch); 
    return($result);
	}
	
	public function sendPhoto($user_id, $filename, $text, $sendfilename='', $reply_markup='') // Untestet, text/plain ersetzen
	{
	// user_id - ID des Users (chat_id)
	// filename - Dateiname mit Pfad zum Bild
	// text - Ein mit zu sendender Text
	// $sendfilename - Dateiname, als der die Datei gesendet werden soll
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup); // Falls Reply Markup als Array überegeben wurde
		if(!file_exists($filename)) {
			// Datei nicht vorhanden
			return(array('ok' => 'false', 'description' => 'Datei existiert nicht'));
		}
		if($sendfilename == '') { // Ausgabefilename für Telegram aus $filename ermitteln
			$pos=strrpos($filename,'/');
			if($pos === false) { //Dateiname ohne Pfad
				$sendfilename=$filename;
			}else{
				$sendfilename=substr($filename,$pos+1);
			}
		}
		$filesize=getimagesize($filename);
		$ch = curl_init();
		$data=array(
			'chat_id'=>intval($user_id),
			'caption' => $text,
			'photo' => new CURLFile($filename,$filesize['mime'],$sendfilename),
			'parse_mode' => $this->parsemode,
			'disable_web_page_preview' => $this->disablewebpagepreview
		);
		if($reply_markup <> '') $data['reply_markup']=$reply_markup;
		$url="https://api.telegram.org/bot".$this->bottoken."/sendPhoto";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit wird die Antwort als String der Funktion curl_exec zurück gegeben
		curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-Type: multipart/form-data"));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'sendPhoto');
		return(json_decode($res,true));
	}
	
	public function sendPhotoById($user_id, $img_id, $text, $reply_markup='')
	{
		if(is_array($reply_markup)) $reply_markup=json_encode($reply_markup); // Falls Reply Markup als Array überegeben wurde
		$ch = curl_init();
		$data=array(
			'chat_id' => intval($user_id),
			'caption' => $text,
			'photo' => $img_id,
			'parse_mode' => $this->parsemode,
			'disable_web_page_preview' => $this->disablewebpagepreview
		);
		if($reply_markup <> '') $data['reply_markup']=$reply_markup;
		$url="https://api.telegram.org/bot".$this->bottoken."/sendPhoto";
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit wird die Antwort als String der Funktion curl_exec zurück gegeben
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'sendPhotoById');
		return(json_decode($res,true));
	}

//****************************************
//* Chat
//****************************************

	public function getChat($chat_id) // Liefert ein Chat-Objekt
	/*******
    [ok] => 1
    [result]
       [id] => -599485364
       [title] => ENLinkBotTestGruppe
       [type] => group
       [permissions] => Array
          [can_send_messages] => 1
          [can_send_media_messages] => 1
          [can_send_polls] => 1
          [can_send_other_messages] => 1
          [can_add_web_page_previews] => 1
          [can_change_info] => 1
          [can_invite_users] => 1
          [can_pin_messages] => 1
       [all_members_are_administrators] => 1
	*******/
	{
		$ch = curl_init();
		$data=array(
			'chat_id'=>$chat_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/getChat");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'getChat');
		return(json_decode($res,true));
	}
	
	public function getChatAdministrators($chat_id,$erroroutput=true) // Liefert eine Liste der Administratoren mit kompletten Userinfos, Status und Rechten
	{
		$ch = curl_init();
		$data=array(
			'chat_id'=>$chat_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/getChatAdministrators");
		$res = curl_exec($ch);
		if($erroroutput) $this->errorhandler($res,$data,'getChatAdministrators'); // Kann abgeschalten werden, da diese Funktion auch zum Testen genutzt wird
		return(json_decode($res,true));
	}
	
	public function getChatMember($chat_id, $user_id) // Informationen zum Übergebenen User abrufen
	{
		$ch = curl_init();
		$data=array(
			'chat_id' => $chat_id,
			'user_id' => $user_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/getChatMember");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'getChatMember');
		return(json_decode($res,true));
	}
	
	public function leaveChat($chat_id) // einen Chat verlassen
	{
		$ch = curl_init();
		$data=array(
			'chat_id'=>$chat_id
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	  curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot".$this->bottoken."/leaveChat");
		$res = curl_exec($ch);
		$this->errorhandler($res,$data,'leaveChat');
		return(json_decode($res,true));
	}
	
	public function testechat($chat_id)
	// Prüft, ob der Chat noch verbunden ist
	{
		if($chat_id > 0) return(true); // Nutzer-Datensatz selbige sind immer verbunden zu betrachten
		$admins=$this->getChatAdministrators($chat_id,false);
		if($admins['ok'] == false) return(false);
			//  {"ok":false,"error_code":403,"description":"Forbidden: bot was kicked from the supergroup chat"}
			//  {"ok":false,"error_code":403,"description":"Forbidden: bot is not a member of the supergroup chat"}
		return(true);
	}
	

	
}

?>

