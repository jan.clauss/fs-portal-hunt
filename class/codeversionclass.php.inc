<?php

/**** Klasse zum einlesen der Prime-Stats aus Telegram und Tab-Separiert
*
*	@Author: Jan Clauß
* @Version: 1.0 - 2021-04-23
*
* 
*/

class codeversion
{
	
	/**
	* gitlog
	*
	* Liefert die Ausgabe des Kommandos 'git log' als Array of Strings
	*
	* @return array of string
	*/
	
	function git_log()
	{
		exec('git log',$output);
		return($output);
	}
	
	/**
	* gitlogarray
	*
	* Liefert die Ausgabe des KOmmandos 'git log'
	*
	* @param integer $anzahl - Anzahl, die ausgewertet werden soll
	* @return array - Commits
	* Array=(
	*   guid string -> Die Commit-GUID
	*   author string -> Author des Commits
	*   mail string -> Mail des Authors
	*   time timestamp -> Zeitpunkt des Commits
	* )
	*/
	function git_logarray($anzahl=-1)
	{
		$gitlogstrings=$this->git_log();
		$commitarray=array(); // Liste der Commits
		$commit=array(); // Einzelnes Commit
		foreach($gitlogstrings AS $line) {
			if(preg_match('/commit ([a-f0-9]+)/',$line,$arr)) { // Commit
				if(count($commit) > 0) { // Letzten Eintrag speichern
					$commitarray[]=$commit;
					if($anzahl > 0 and count($commitarray) >= $anzahl) { // Vorzeitiges Ende, da $anzahl erreicht
						return($commitarray);
					}
					$commit=array();
				}
				$commit['guid'] = $arr[1];
				$commit['desc'] = ''; // Descriptionstring initialisieren
			}elseif(preg_match('/Author:(.*?)\s+<(.*?)>/',$line,$arr)) { // Author und Mail
				$commit['author'] = $arr[1];
				$commit['mail'] = $arr[2];
			}elseif(preg_match('/Date:\s+([a-z0-9 :+-]+)/i',$line,$arr)) { // Zeitpunkt
				$commit['time'] = strtotime($arr[1]);
			}else{
				if(trim($line) <> '') {
					$commit['desc'].=trim($line);
				}
			}
		}
		if(count($commit) > 0) { // Letzten Eintrag speichern
			$commitarray[]=$commit;
		}
		return($commitarray);
	}
	
	function git_codeversion()
	{
		$commitarray=$this->git_logarray(1);
		$retarr['commit']=substr($commitarray[0]['guid'],0,8);
		$retarr['timestr']=date('Y-m-d H:i',$commitarray[0]['time']);
		return($retarr);
	}
	
	function git_branches()
	{
		exec('git branch',$output);
		$branches=array();
		foreach($output AS $line) {
			if(preg_match('/(\**)\s+(.*)/',$line,$arr)) {
				$branches[]=array('active' => ($arr[1] == '*'), 'name' => $arr[2]);
			}
		}
		return $branches;
	}
	
}

?>