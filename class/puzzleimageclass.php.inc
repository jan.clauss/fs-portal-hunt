<?php
/**** Klasse für die Übermittlung der Daten an Crytix ImageWebsite
*
*	@Author: Jan Clauß
* @Version: 1.0
* 
* Required Class telegram
*/



class puzzleimage
{
	
	public $apitoken = ''; // Der Token zum senden an API
	public $url = ''; // Die eigentliche User bzw. Chat-DB
	public $imageurl = ''; // Der Link zum Image-Upload
	public $ok=false;


	function __construct($apitoken, $url, $imageurl,$webportalurl)
	{
		if($url == '' or $apitoken == '') return;
		$this->ok=true;
		$this->apitoken=$apitoken;
		$this->url=$url;
		$this->imageurl=$imageurl;
		$this->webportalurl=$webportalurl;
  }

	public function sendportalstatus($col, $row, $status, $fevgamesid)
	{
		global $tg, $bot_admins; // für Debugging
		if($this->ok == false) return(false);
		$SJ=new SecureJson(array('FevgamesID' => $fevgamesid, 'ok' => true, 'portals' => array("$col$row" => $status), 'type' => 'update'));
		$SJ->key=$this->apitoken;
		$ch = curl_init();
		$portalresult=$SJ->jsonSerialize();
//		if(IsSet($bot_admins[$tg->user_id])) $tg->sendmessage($tg->user_id, json_encode($portalresult)); // Debugging
		$data=json_encode($portalresult);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$res = curl_exec($ch);
//		if(IsSet($bot_admins[$tg->user_id])) $tg->sendmessage($tg->user_id, $res); // Debugging
		$resarr=json_decode($res,true);
		return($resarr);
	}
	
	public function sendfoundportals($readsheetresult, $titel, $datum, $ort, $fevgamesid, $huntstart)
	//
	// $readsheetresult - Ergebnis von portalhuntsheet->readsheet
	// $title - Titel des FS (Importiert aus Fevgames)
	// $datum - Datum des FS
	// $ort - Ort des FS 
	// $fevgamesid - Die Fevgames-ID
	// $huntstart - Der Startzeitpunkt des Portalhunt (integer/ timestamp)
	{
		global $tg, $bot_admins; // für Debugging
		if($this->ok == false) return(false);
		$readsheetresult['FSTitle']=$titel;
		$readsheetresult['FSDate']=$datum;
		$readsheetresult['FSLocation']=$ort;
		$readsheetresult['FevgamesID']=$fevgamesid;
		$readsheetresult['HuntStart']=$huntstart;
		
		$readsheetresult['type']='initial';
		$SJ=new SecureJson($readsheetresult);
		$SJ->key=$this->apitoken;
		$readsheetresult=$SJ->jsonSerialize();
//		if(IsSet($bot_admins[$tg->user_id])) $tg->sendmessage($tg->user_id, json_encode($readsheetresult)); // Debugging
		$ch = curl_init();
		$data=json_encode($readsheetresult);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Damit die Antwort nicht direkt ausgegeben wird
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$res = curl_exec($ch);
//		if(IsSet($bot_admins[$tg->user_id])) $tg->sendmessage($tg->user_id, $tg->escapestr($res)); // Debugging
//		if(IsSet($bot_admins[$tg->user_id])) $tg->sendmessage($tg->user_id, json_encode(curl_getinfo($ch))); // Debugging
		$resarr=json_decode($res,true);
		return $resarr;
	}
	
	public function genconfigsitelink($fevgamesid, $duration=10*60)
	{
		if($this->ok == false) return(false);
		$sigarr=['fevid' => intval($fevgamesid), 'validto' => time()+$duration];
		$SJ=new SecureJson($sigarr);
		$SJ->key=$this->apitoken;
		$sigarr=$SJ->jsonSerialize();
		$link=$this->imageurl.'?fevid='.$fevgamesid.'&validto='.$sigarr['validto'].'&signature='.$sigarr['signature'];
		return($link);
	}
	
	public function WebportalLink($fevgamesid)
	{
		if($this->ok == false) return(false);
		$link=$this->webportalurl."?e=$fevgamesid";
		return($link);
	}
}
?>