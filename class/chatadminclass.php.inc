<?php
/**** Klasse für Telegram in Verbindung mit einer DB
*
*	@Author: Jan Clauß
* @Version: 1.0
* 
* Required Class telegram
*/



class chatadmin
{
	
	public $database_table = ''; // Die Zurodnungstabelle User - Chat
	public $database_table_userchat =''; // Die eigentliche User bzw. Chat-DB
	public $pdo=null;


	function __construct($pdoobj, $database_table)
	{
		if(!is_object($pdoobj))
			return(array('ok' => false, 'error_code' => '1', 'description' => '$pdo must be an Database Object'));
		$this->pdo=$pdoobj;
		$this->database_table_telegram=$database_table;
		$this->database_table_chatadm=$database_table.'_chatadm';
		return(array('ok' => true));
  }
  
  function createdb()
  {
  	$db=explode('.',$this->database_table_chatadm);
  	$database=$db[0];
  	$table=$db[1];
  	$sql="USE $database";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo())));
		}
  	$sql="
CREATE TABLE IF NOT EXISTS `".$table."` (
	`chat_id` BIGINT(20) NOT NULL,
	`user_id` BIGINT(20) NOT NULL,
	`status` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_bin',
	`lastchange` DATETIME NOT NULL,
	INDEX `chat_id` (`chat_id`),
	INDEX `user_id` (`user_id`),
	UNIQUE INDEX `chat_id_user_id` (`chat_id`, `user_id`)
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
;
";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(array('ok' => true));
  }
  
  function setadmins($chat_id, $admins) // Erwartet das Array aus telegramclass->getChatAdministrators
  {
		// User-IDs aus dem Array ermitteln
		if(!IsSet($admins['ok']) or $admins['ok'] <> true) {
			return(array('ok' => false));
		}
		// Alte Admin-Info löschen
		$sql="DELETE FROM ".$this->database_table_chatadm." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		$lastchange=date('Y-m-d H:i:s');
		$sql="INSERT INTO ".$this->database_table_chatadm." SET chat_id=:chat_id, user_id=:user_id, status=:status, lastchange=:lastchange";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->bindParam(':lastchange',$lastchange, PDO::PARAM_STR);
		foreach($admins['result'] AS $userarr) {
			$stm_all->bindParam(':user_id',$userarr['user']['id'], PDO::PARAM_INT);
			$stm_all->bindParam(':status',$userarr['status'], PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
			}
		}
		return(array('ok' => true));
	}
	
	function getadmins($chat_id) // Liefert die Admins des Chats aus der DB
	{
		$sql="
SELECT t.user_id 
FROM ".$this->database_table_telegram." AS t 
LEFT JOIN ".$this->database_table_chatadm." AS a ON a.user_id=t.user_id 
LEFT JOIN ".$this->database_table_telegram." AS c ON c.user_id=a.chat_id
WHERE a.chat_id=:chat_id AND c.rights IN ('user', 'mod')";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		$ret=array();
		while($result=$stm_all->fetch()) {
//			error_log("getadmins $chat_id ".$result['user_id']);
			$ret[]=$result['user_id'];
		}
		return($ret);
	}
	
	function getchat($chat_id) // Liefert Daten zu dem einzelnen chat
	{
		$sql="SELECT * FROM ".$this->database_table_telegram." WHERE user_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		if($stm_all->rowCount() <> 1) {
			return(false);
		}
		$result=$stm_all->fetch();
		return($result);
	}
	
	function getallchats()
	// Liefert alle Chats
	{
		$sql="SELECT * FROM ".$this->database_table_telegram." WHERE user_id < 0 ORDER BY lastkontakt DESC";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
			return(array('ok' => false));
		}
		$ret=array();
		while($result = $stm_all->fetch()) {
			$ret[]=$result;
		}
		return($ret);
	}
  
	function getchats($user_id) // Liefert ein Array mit chat_id und chat_name
	{
		$sql="
SELECT c.user_id, c.first_name FROM ".$this->database_table_telegram." AS c 
LEFT JOIN ".$this->database_table_chatadm." AS a ON c.user_id=a.chat_id 
WHERE a.user_id=:user_id AND c.rights IN ('user', 'mod')";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
			return(array('ok' => false));
		}
		$ret=array();
		while($result = $stm_all->fetch()) {
			$ret[]=array('chat_id' => $result['user_id'], 'chat_title' => $result['first_name']);
		}
		return(array('ok' => true, 'values' => $ret));
	}
	
	function isadmin($chat_id, $user_id) 
	// Testet, ob der User Admin des Chats ist
	// Testet in der Datenbank
	// Rückgabewert false oder Status
	{
		$sql="
SELECT a.status FROM ".$this->database_table_chatadm." AS a
LEFT JOIN ".$this->database_table_telegram." AS c ON c.user_id=a.chat_id
WHERE chat_id=:chat_id AND a.user_id=:user_id AND c.rights IN ('user', 'mod')";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		if($stm_all->rowCount() == 0) {
			return(false);
		}
		$result=$stm_all->fetch();
//		error_log("isadmin $chat_id $user_id ".print_r($result,true));
		return($result['status']);
	}
	
	function deletechat($chat_id)
	{
		$sql="DELETE FROM ".$this->database_table_chatadm." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(true);
	}
}
