<?php

/**** Klasse für Daten von PortalhuntSheet
*
*	@Author: Jan Clauß
* @Version: 1.0
* 
*/


class portalhuntsheet
{

	public $pdo = null; // Das PDO-object für den Bot (Constructor)
	public $database_table_telegram = ''; // Die Datenbank für den Bot (Constructor)
	public $database_table_sheet = ''; // Die Datenbank für den Sheet-Status (Constructor)
	public $chat_id = 0; // ID-des Chats
	public $ph = null;
	public $passrange=SHEETRANGE_PASSCODE;
	public $colrange=SHEETRRANGE_COL;
	
	function __construct($pdoobj, $database_table, $ph, $chat_id, $colrange=SHEETRRANGE_COL, $passrange=SHEETRANGE_PASSCODE)
	{
		if(!is_object($pdoobj))
			return(array('ok' => false, 'error_code' => '1', 'description' => '$pdo must be an Database Object'));
		$this->pdo=$pdoobj;
		$this->database_table_telegram=$database_table;
		$this->database_table_sheet=$database_table.'_Sheet';
		$this->chat_id=$chat_id;
		$this->ph=$ph;
		$this->colrange=$colrange;
		$this->passrange=$passrange;
		return(array('ok' => true));
	}
	
  function createdb()
  {
  	$db=explode('.',$this->database_table_sheet);
  	$database=$db[0];
  	$table=$db[1];
  	$sql="USE $database";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo())));
		}
  	$sql="
CREATE TABLE IF NOT EXISTS `".$table."` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`chat_id` BIGINT(20) NOT NULL,
	`row` TINYINT(4) NOT NULL,
	`col` CHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`lat` CHAR(12) NOT NULL COLLATE 'utf8mb4_bin',
	`lng` CHAR(12) NOT NULL COLLATE 'utf8mb4_bin',
	`agent` CHAR(50) NOT NULL COLLATE 'utf8mb4_bin',
	UNIQUE INDEX `id` (`id`) USING BTREE,
	INDEX `chat_id` (`chat_id`) USING BTREE,
	INDEX `row` (`row`) USING BTREE,
	INDEX `col` (`col`) USING BTREE
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(array('ok' => true));
  }

	function readsheet($tg = null, $tg_id = 0) // Soll in Zukunft selber ausgeben
	{
		global
			$sheets_authConfig,$sheets_tokenPath,
			$sheetsites;
			
		if($this->ph->portalhuntsheet == '') {
			return(array('ok' => false, 'code' => '001', 'description' => 'No Passcode-Sheet'));
		}
		$sheets=new googlesheets($sheets_authConfig,$sheets_tokenPath);
		
		// *** Passcode Sheet lesen ***
		// Daten auslesen
		$range=SHEETNAME_PASSCODE.'!'.$this->passrange;
		$rows=$sheets->getRange($this->ph->portalhuntsheet,$range);
		if(!IsSet($rows['values']) or $rows['values'] == null) {
			return(array('ok' => false, 'code' => '002', 'description' => 'ReadError Passcode-Sheet'));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sheets-error','ReadError Passcode-Sheet','main');

		}
		// Die gefundenen Zeilen ensprechen der Anzahl der eingetragenen Puzzle-Spalten, es wird die Anzahl der Portale ermittelt und falls gefunden der Code
		foreach($rows['values'] AS $value) {
			$pcs[$value[0]]=['portals' =>intval($value[3]), 'character' => $value[5]];
		}
		$this->ph->passcodesheetdata=$pcs; // Speichern in Portalhuntclass
		$this->ph->savechatdata($this->chat_id); // Und in die DB(botconf) des Chats
		$this->deleteportale(); // Alte Portaldaten aus der DB entfernen
		$output='';
		// Die einzelnen Sheets auslesen
		foreach($this->ph->passcodesheetdata AS $column => $data) {
			$columnchar=$this->ph->dezColumn($column);
			$output.=$columnchar;
			$output.=' ('.$data['character'].'): <code>';
			$range=$sheetsites[$columnchar].'!'.$this->colrange;
			$rows=$sheets->getRange($this->ph->portalhuntsheet,$range);
//			error_log(json_encode($rows));
			if(IsSet($rows['values']) and $data['portals'] > 0) { // Sollte immer was liefern, da die Portalanzahl im Passcode-Sheet steht und hier die Numern erzeugt werden.
				for($i=0; $i < $data['portals']; $i++) {
					$portals[$columnchar.($i+1)]=IsSet($rows['values'][$i][3]);
					if(IsSet($rows['values'][$i][3])) { // Link steht im Sheet, da das das letzte Feld ist, existieren auch alle anderen Felder und müssen nicht geprüft werden
						$this->writeportal($columnchar, $i+1, $rows['values'][$i][3], $rows['values'][$i][2]);
						$output.=$columnchar.($i+1).' ';
					}
				}
			}else{
				$output.="❗";
//				if($sheetsites[$columnchar]=='P_16') {
//					unset($rows['values']);
//			error_log(json_encode($rows));
//				}
				if($rows === false) {
					$output.=' '.$sheets->lasterr->error->code.' - '.$sheets->lasterr->error->message;
				}elseif(is_null($rows['values'])) {
					$output.=' No Data in Range: '.$range;
				}elseif($data['portals'] == 0) {
					$output.=' No Rows Set: '.$range;
				}else{
					$output.=" unknown error";
				}
				$portals[$columnchar."0"]='error';
			}
			$output.="</code>\n";
		}
		if($output == '') {
			// Nichts gefunden
			return(array('ok' => false, 'code' => '003', 'description' => 'No Data'));
		}
		return(array('ok' => true, 'data' => $output, 'cols' => $this->ph->passcodesheetdata, 'portals' => $portals));
	}
	
	function deleteportale()
	// Löscht die Portalinformationen des aktuellen Chats
	{
		$sql="DELETE FROM ".$this->database_table_sheet." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		return(true);
	}
	
	function writeportal($col, $row, $link, $agent)
	// Schreibt ein Portal
	{
		if(preg_match('/pll=([-0-9.]+),([-0-9.]+)/', $link, $arr)) {
			$sql="INSERT INTO ".$this->database_table_sheet." SET chat_id=:chat_id, row=:row, col=:col, lat=:lat, lng=:lng, agent=:agent";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
			$stm_all->bindParam(':row',$row, PDO::PARAM_INT);
			$stm_all->bindParam(':col',$col, PDO::PARAM_STR);
			$stm_all->bindParam(':lat',$arr[1], PDO::PARAM_STR);
			$stm_all->bindParam(':lng',$arr[2], PDO::PARAM_STR);
			$stm_all->bindParam(':agent',$agent, PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
			}
		}else{
			error_log(__FILE__.' '.__LINE__.' Error decoding Portallink');
		}
	}
	
	function getcol($col)
	// Ließt eine Spalte der Portaldaten aus der Datenbank // col =Numerischer Wert 1-
	{
		if(!IsSet($this->ph->passcodesheetdata[$col])) {
			return(false);
		}
		$result=$this->ph->passcodesheetdata[$col]; // portals und character
		$colchar=$this->ph->dezColumn($col);
		$result['col']=$colchar;
		$sql="SELECT * FROM ".$this->database_table_sheet." WHERE chat_id=:chat_id and col=:col ORDER BY row";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
		$stm_all->bindParam(':col', $colchar, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		$result['exist']=array();
		while($resarr=$stm_all->fetch()) {
			$result['exist'][]=$resarr['col'].$resarr['row'];
		}
		return($result);
	}
	
	function getall()
	// Ließt alle Portaldaten des aktuellen Chats aus der DB Sortiert nach col, row
	{
		$sql="SELECT * FROM ".$this->database_table_sheet." WHERE chat_id=:chat_id ORDER BY col, row";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		$portalarray=array();
		while($result=$stm_all->fetch(PDO::FETCH_ASSOC)) {
			$portalarray[]=$result;
		}
		return($portalarray);
	}
	
	function getaktiveagents()
	// Ermittelt aus $this->getall die Beteiligten Agents
	{
		$portalarray=$this->getall();
		foreach($portalarray AS $portal) {
			if(trim($portal['agent']) <> '') {
				$agentarray[$portal['agent']]=$portal['agent'];
			}
		}
		return($agentarray);
	}
	
	function deleteChat($chat_id) 
	// Löscht alle Portale des übergebenen Chats
	{
		$sql="DELETE FROM ".$this->database_table_sheet." WHERE chat_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}
		return(true);
	}
}
?>