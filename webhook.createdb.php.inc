<?php
		$resarr=$tg->createdb('user');
		if($resarr['ok'] == false) {
			echo "Fehler beim Anlegen der Telegram User-DB<br>\n";
			echo json_encode($resarr);
			exit;
		} // Telegram DB erfolgreich angelegt
		echo "Telegram User-DB angelegt<br>\n";
		echo json_encode($resarr)."<br>\n";
		// Chatadmin-DB anlegen
		$resarr=$chatadm->createdb();
		if($resarr['ok'] == false) {
			echo json_encode($resarr);
			exit();
		}
		echo "Chat-Admin-DB angelegt<br>\n";
		echo json_encode($resarr)."<br>\n";
		// Portal-DB anlegen
		$resarr=$ph->createdb();
		if($resarr['ok'] == false) {
			echo json_encode($resarr);
			exit();
		}
		echo "Portal-DB angelegt<br>\n";
		echo json_encode($resarr)."<br>\n";
		// Sheet-DB anlegen
		$phsheet=new portalhuntsheet($pdo, $database_table_main,0,0); // ID 0, da nur die DB angelegt wird
		$resarr=$phsheet->createdb();
		if($resarr['ok'] == false) {
			echo json_encode($resarr);
			exit();
		}
		unset($phsheet);
		echo "Portal-DB angelegt<br>\n";
		echo json_encode($resarr)."<br>\n";
		echo "Datenbanken erfolgreich angelegt<br>\n";
?>